<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		log_message('debug', "Model Class Initialized");
	}

	/**
	 * __get
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

    /**
    * Executa a query sql para consulta no banco de dados
    *
    * @param string $sql O comando SQL que será executado
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function _executaSql($sql)
    {
        $dados = array();

        try {
            $query = $this->db->query(utf8_decode($sql));

            if ($query->num_rows() <> 0) {
                $dados = $query->result_array();
            }

        } catch (Exception $e) {
            log_message('error', __method__ . print_r($e, true));
            return ($e);
        }
        
        return $dados;
    }	
}
// END Model Class

/* End of file Model.php */
/* Location: ./system/core/Model.php */