﻿<?php
/**
* Mercado File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Mercado_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Mercado extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/liquidez/index
    *
    * @access  public
    * @return  void
    */
    public function index()
    {
        $this->load->view('header');
        $this->load->view('footer');
    }

    /**
    * Consulta ao banco de dados para monitoramento de risco de mercado.
    *
    * @access  private
    * @return  void $query->result();
    */
    private function _getMonitMercProdutos()
    {
        $query = $this->db->query(
            "SELECT 
                P.NO_PRD, 
                P.CO_PRD, 
                PD.DT_REF, 
                PD.VR_MAPS_PL_INFO, 
                PD.VR_MAPS_MTM, 
                PD.NO_MAPS_METODO_VAR, 
                PD.NO_MAPS_METODO_VAR, 
                PD.NU_MAPS_HORIZ, 
                PD.VR_MAPS_NIV_CONF,
                PD.VR_MAPS_LAMBDA, 
                PD.VR_MAPS_VAR_POL, 
                PD.VR_MAPS_VAR_POL / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_POL, 
                PA.CO_RISCO, 
                N.FT_LIM, 
                (PD.VR_MAPS_VAR_POL / PD.VR_MAPS_PL_INFO) / N.FT_LIM PC_ALOC_S_LIM,
                PD.VR_MAPS_VAR_1D_95, 
                PD.VR_MAPS_VAR_1D_95 / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_1D_95,
                PD.VR_MAPS_VAR_1D_95_STRESS, 
                PD.VR_MAPS_VAR_1D_95_STRESS / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_1D_95_STRESS,
                PD.VR_MAPS_VAR_21D_95, 
                PD.VR_MAPS_VAR_21D_95 / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_21D_95,
                PD.VR_MAPS_VAR_1D_99, 
                PD.VR_MAPS_VAR_1D_99 / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_1D_99,
                PD.VR_MAPS_VAR_15D_99, 
                PD.VR_MAPS_VAR_15D_99 / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_15D_99,
                PD.VR_MAPS_VAR_21D_99, 
                PD.VR_MAPS_VAR_21D_99 / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_21D_99,
                F.NO_FATOR NO_MAX_FATOR, 
                A.NO_ATIVO NO_MAX_ATIVO, 
                PD.VR_MAX_ATIVO, 
                PD.VR_MAX_ATIVO / PD.VR_MAPS_PL_INFO PC_ALOC_MAX_ATIVO,
            FROM 
                PRODUTO_DIA PD
            LEFT JOIN 
                Produto_Dia_Expo_Ativo_x_Fator_Fator F ON PD.CO_MAX_FATOR = F.CO_FATOR
            LEFT JOIN 
                Produto_Dia_Expo_Ativo_x_Fator_Ativo A ON PD.CO_MAX_ATIVO = A.CO_ATIVO,
                PRODUTO P
            LEFT JOIN 
                SIRAT_PARAM PA ON P.CO_PRD = PA.CO_FDO
            LEFT JOIN 
                SIRAT_RM_NIVEL_RISCO_LIM N ON PA.CO_RISCO = N.CO_NIV_RISCO
            INNER JOIN 
                PRODUTO_SEGMENTO PS
                  ON PS.CO_SEG = P.CO_SEG
            WHERE 
                P.CO_PRD = PD.CO_PRD 
                AND PD.DT_ATU = (select max(x.dt_atu) from produto_dia x where x.co_prd = p.co_prd and x.vr_maps_mtm is not null)
                AND PS.CO_POL = 1
                AND P.IC_DESAT = 0
            ORDER BY P.NO_PRD"
        );
        return $query->result();
    }

    /**
    * Consulta ao banco de dados para detalhe do monitoramento de risco de mercado.
    *
    * @param string $co_prd O CNPJ do fundo/carteira
    *    
    * @return  void $query->row();
    */
    private function _getMonitMercProdutoDet($co_prd)
    {
        $query = $this->db->query(
            "SELECT 
                P.NO_PRD, 
                PD.NO_MAPS_METODO_VAR, 
                PD.NU_MAPS_HORIZ, 
                PD.VR_MAPS_NIV_CONF, 
                PD.VR_MAPS_MTM, 
                PD.VR_MAPS_PL_INFO, 
                PD.VR_MAPS_VAR_POL,
                PD.VR_MAPS_VAR_POL / PD.VR_MAPS_PL_INFO PC_ALOC_VAR_POL, 
                N.FT_LIM, 
                (PD.VR_MAPS_VAR_POL / PD.VR_MAPS_PL_INFO) / N.FT_LIM PC_ALOC_S_LIM
            FROM 
                PRODUTO_DIA PD, 
                PRODUTO P
            LEFT JOIN 
                SIRAT_PARAM PA ON P.CO_PRD = PA.CO_FDO
            LEFT JOIN 
                SIRAT_RM_NIVEL_RISCO_LIM N ON PA.CO_RISCO = N.CO_NIV_RISCO
            WHERE 
                P.CO_PRD = PD.CO_PRD 
                AND PD.DT_ATU = (select max(x.dt_atu) from produto_dia x where x.co_prd = p.co_prd and x.vr_maps_mtm is not null)
                AND PD.CO_PRD='".$co_prd."'"
        );
        return $query->row();
    }
    
    /**
    * Lista dos fundos com o controle de limite de risco de mercado.
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/mercado/ControleLimite
    *
    * @access  public
    * @return  void
    */
    public function controleLimite()
    {
        $produtos = $this->_getMonitMercProdutos();
        $data['produtos'] = $produtos;

        $this->load->view('header');
        $this->load->view('list_controle_limite', $data);
        $this->load->view('footer');
    }

    /**
    * Detalhe do limite de risco de mercado para um único fundo.
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/mercado/ControleLimiteDetalhe
    *
    * @access  public
    * @return  void
    */
    public function controleLimiteDetalhe()
    {
        $co_prd = $this->input->get('co_prd', true);
        $this->load->helper("url");
        $this->load->database();
        $row = $this->getMonitMercProdutoDet($cnpj);
        $data['produto'] = $row;

        //$this->load->view('controle_limite_list');
        $this->load->view('controleLimiteRep1', $data);
    }
}