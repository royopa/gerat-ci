<?php 
/**
* Liquidez File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Liquidez_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Fundos extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');
        if ($this->agent->browser() == 'Internet Explorer') {
            redirect('/gerat/navegadorSuportado', 'refresh');
            return;
        }        
        $this->load->database();
    }
   
    /**
    * Lista de fundos/carteiras ativas.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/listaProdutosAtivos
    *
    * @access  public
    * @return  void
    */
    public function listaProdutosAtivos()
    {
        $produto_model = new Produto_Model();

        $data = array();

        $data['produtos'] = $produto_model->getProdutos(0);
 
        $this->twiggy->title('GERAT');
        $this->twiggy->title()->prepend('Lista de fundos');

        $this->twiggy->set($data, NULL);        
        $this->twiggy->display('lista_fundos');
    }
}
