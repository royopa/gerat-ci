<?php
/**
* Benchmark File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Benchmark_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class BenchmarkController extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Benchmark_Model');
    }

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/benchmark/index
    *
    * @access  public
    * @return  null
    */
    public function index()
    {
        $bench_model = new Benchmark_Model();

        $benchmarks = $bench_model->getBenchmarks();

        $dados = array();

        foreach ($benchmarks as $key => $value) {
            # code...
            $dados[] = array($value['CO_BENCH'] => utf8_encode($value['NO_BENCH']));
        }

        $data['benchmarks'] = $dados;

        $this->load->view('header');
        $this->load->view('form_benchmark_search', $data);
        $this->load->view('footer');
    }

    /**
    * Test Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/index2
    *
    * @access  public
    * @return  null
    */
    public function teste()
    {
        $this->load->model('Benchmark_Model');

        $bench_model = new Benchmark_Model();

        var_dump($composicao);
    }

    /**
    * Test Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/benchmark/listaBenchmarks
    *
    * @access  public
    * @return  null
    */
    public function listaBenchmarks()
    {
        //dt_ini;
        //dt_fim;
        //co_bench;
        $dt_ini   = formata_data($this->input->post('dt_ini'));
        $dt_fim   = formata_data($this->input->post('dt_fim'));
        $co_bench = $this->input->post('co_bench');

        $this->load->model('Benchmark_Model');
        $bench_model = new Benchmark_Model();

        $lista = $bench_model->getListaBenchmarks($co_bench, $dt_ini, $dt_fim);

        $data['lista'] = $lista;

        $this->load->view('header');
        $this->load->view('list_benchmark', $data);
        $this->load->view('footer');
    }
}
