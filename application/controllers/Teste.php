<?php
/**
* Teste File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
* Teste_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Teste extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
    * Teste
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/teste
    *
    * @access  public
    * @return  void
    */
    public function index()
    {
        /*
        $composicao   = new Composicao();
        $composicao2  = new Composicao();
        $listaComposicaoCarteira = new ListaComposicaoCarteira();

        $composicao->setNome('Item 1');
        $composicao->setValor(102456.55);
        $composicao->setPercentual(25.40);

        $listaComposicaoCarteira->append($composicao);

        $composicao2->setNome('Item 2');
        $composicao2->setValor(9966656.55);
        $composicao2->setPercentual(50.88);

        $listaComposicaoCarteira->append($composicao2);

        $serieDados = new SerieDados($listaComposicaoCarteira);

        //var_dump($serieDados->getSerieArray());
        //var_dump($listaComposicaoCarteira->getDadosSerie2());

        $dbal = new Dbal();
        $conn = $dbal->getConn();

        //$conn = new Dbal->connDbal;

        $sql = "SELECT * FROM sysusers";
        $stmt = $conn->query($sql); // Simple, but has several drawbacks

        while ($row = $stmt->fetch()) {
            echo $row['name'] . '</br>';
        }

        $sql = "SELECT * FROM sysusers WHERE name = ? OR uid = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(1, 'dbo');
        $stmt->bindValue(2, 16393);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            echo $row['name'] . '</br>';
        }

        $sql = "SELECT * FROM sysusers WHERE name = :name OR uid = :uid";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("name", 'dbo');
        $stmt->bindValue("uid", 16393);
        $stmt->execute();

        $co_prd = '01165780000192';
        $dt_atu = '2013-09-25';

        //instancia o objeto fundoDiario
        $fundoDiario = new FundoDiario(
            new Fundo($co_prd),
            new DateTime($dt_atu)
        );

        $stmt = new ItemRiscoMercadoGrupo($fundoDiario);
        */

        /*
        $ConnectionParamsSqlite = array(
            'driver'   => 'pdo_sqlite',
            'memory'   => false,
            'path'     => APPPATH . '/data/db.sqlite',
        );

        $dbal = new Dbal($ConnectionParamsSqlite);
        $conn = $dbal->getConn();
        */

        /*

        var_dump($conn);

        $sql = "SELECT * FROM relatorios WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("id", 1);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            var_dump($row) . '</br>';
        }

        $sql
            = "
            SELECT
                relatorio_id AS idRelatorio,
                relatorios.nome AS nomeRelatorio, 
                relatorios.descricao AS descricaoRelatorio,
                views_relatorios.posicao,
                views.nome AS nomeView,
                views.path AS pathView,
                views.descricao AS descricaoView
            FROM
                views_relatorios
            INNER JOIN 
                views
            ON
                views.id = views_relatorios.view_id
            INNER JOIN
                relatorios
            ON
                relatorios.id = views_relatorios.relatorio_id
            WHERE
                relatorios.id = :id
        ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue("id", 2);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            var_dump($row) . '</br>';
            //$this->load->view($row['path'], $data);
        }
        */

        /*
        $relatorio = new Relatorio($conn);
        //chama o relatório para mostrar as views configuradas
        $relatorio->get(1);

        foreach ($relatorio as $view) {
            //var_dump($view);
            //$this->load->view($view->getPath(), $data);
        }

        $co_prd = '01165780000192';
        $dt_atu = '2013-09-25';

        //instancia o objeto fundoDiario
        $fundoDiario = new FundoDiario(
            new Fundo($co_prd),
            new DateTime($dt_atu)
        );

        $desempenhoMensalGrupo = new DesempenhoMensalGrupo($fundoDiario);
        //var_dump($desempenho->fetchAll($fundoDiario, $desempenho->getUltimoDiaMes($fundoDiario)));

        foreach ($desempenhoMensalGrupo as $desempenhoMensal)
        {
            //var_dump($desempenhoMensal);
            //var_dump($maxDataReferenciaFinal);
            //var_dump($maxDataReferenciaInicial);
        }
        */
               
        // map the functions as a Twig functions


        $this->twiggy->title('GERAT');

        $this->twiggy->title()->prepend('Home');

        $data = array();
        // Presumably you'll want to get these from the database through the model
        $data['albums'] = array('Nothing Is Sound', 'Oh! Gravity.', 'Vice Verses');
        $data['tracks'] = array('Afterlife', 'The Original', 'The War Inside');
        $data['cnpj']   = '025222223111';

        $this->twiggy->set($data, NULL);        

        $albums = $this->twiggy->albums;

        $this->twiggy->register_function('phpversion');

        //$this->twiggy->display();
        $this->twiggy->display('welcome');
    }
}
