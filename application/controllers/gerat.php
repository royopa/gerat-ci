<?php
/**
* Gerat File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Gerat_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Gerat extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/index
    *
    * @access  public
    * @return  null
    */
    public function index()
    {
        $this->load->library('user_agent');
        if ($this->agent->browser() == 'Internet Explorer') {
            redirect('/gerat/navegadorSuportado', 'refresh');
            return;
        }

		//return redirect('http://10.4.45.208/gerat-sf/web/', 'refresh');
        
        $this->twiggy->title('GERAT');
        $this->twiggy->title()->prepend('Home');
        $this->twiggy->display('home');
    }

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/navegadorSuportado
    *
    * @access  public
    * @return  null
    */
    public function navegadorSuportado()
    {
        $this->twiggy->title('Navegador não suportado');
        $this->twiggy->title()->prepend('GERAT');
        $this->twiggy->display('navegador_suportado');
    }
}
