<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	* Index Page for this controller.
	*
	* Maps to the following URL
	* 		http://example.com/index.php/welcome
	*	- or -  
	* 		http://example.com/index.php/welcome/index
	*	- or -
	* Since this controller is set as the default controller in 
	* config/routes.php, it's displayed at http://example.com/
	*
	* So any other public methods not prefixed with an underscore will
	* map to /index.php/welcome/<method_name>
	* @see http://codeigniter.com/user_guide/general/urls.html
	*/
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function index()
    {
		//phpinfo();
        $this->load->view('header');
        $this->load->view('welcome_message');
		$this->load->view('footer');        
    }
    
    /**
     * A Série Histórica de Cotações traz todo o histórico de preços dos títulos  
     * negociados na Bolsa desde 1986.
     * 
     * http://www.bmfbovespa.com.br/pt-br/cotacoes-historicas/FormSeriesHistoricas.asp
    */
    public function cotacaoHistorica()
    {
        $this->load->helper('file');
                
        //Site para baixar a série histórica Ibovespa
        $site = 'http://www.bmfbovespa.com.br/InstDados/SerHist/';
        $dia_arquivo = '08082013';
        $arquivo_zip = 'COTAHIST_D'.$dia_arquivo.'.ZIP';
        $arquivo_txt = 'COTAHIST_D'.$dia_arquivo.'.TXT';
        $url = $site.$arquivo_zip;
        
        $dia = substr($dia_arquivo, 0, 2);
        $mes = substr($dia_arquivo, 2, 2);
        $ano = substr($dia_arquivo, 4, 4);

        $dia_semana = jddayofweek ( cal_to_jd(CAL_GREGORIAN, $mes, $dia, $ano) , 0 );
        
        $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . $arquivo_zip;

        //se não é fim de semana, baixa o arquivo
        if (($dia_semana != 0) && ($dia_semana != 6)) {

            $this->download($url, $path);
            
            //extrai o arquivo zip
            $this->descompacta($path);
            
            //apaga o arquivo zip
            unlink($path);            
            
            $this->abre_zip($path, $arquivo_txt);
    
        } else {
            echo 'Arquivo indisponivel ' . $dia . '/'  . $mes . '/' . $ano . ' = fim de semana.';
        }

    }

    public function abre_zip($path, $arquivo_txt) 
    {
        $zip = new ZipArchive();
                 
        if( $zip->open($path)  === true) {
            $conteudo_txt = $zip->getFromName($arquivo_txt);
            echo $conteudo_txt;
        }        
    }

    public function descompacta($path) 
    {
        // A variável $zip recebe da biblioteca do php.ini o comando ZipArquive
        $zip = new ZipArchive;
 
        // Verificando se o arquivo recebido na variável $nome_real existe e abrindo o arquivo
        if ($zip->open($path) === TRUE) {
 
            // Se o arquivo existir, ele será totalmente descompactado dentro da pasta arquivos
            $zip->extractTo(dirname(__FILE__).DIRECTORY_SEPARATOR);
            // Fechando o comando ZipArquive
            $zip->close();
                
            // Mostrando a mensagem de arquivo descompactado
            echo 'Arquivo descompactado';
 
        } else {
            // Caso haja alguma falha ao descompactar irá ser mostrado a mensagem de erro!
            echo 'Falha ao descompactar arquivo';
            }     
    }
    
    public function download($url,$path) 
    {    
        $fp = fopen($path, 'w');
 
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FILE, $fp);
               
        $data = curl_exec($ch);
               
        curl_close($ch);
        fclose($fp);
 
        $ch = curl_init();
 
        // informar URL e outras funções ao CURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // acessar URL
        $output = curl_exec($ch);
 
        // Pegar o código de resposta
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    }
    
    public function liquidez()
    {
        $this->load->view('header');
        
        $produto_model = new Produto_Model();
        
        //pega todos os produtos/fundos ativos
        $produtos_ativos = $produto_model->getProdutos(0);
        
		foreach ($produtos_ativos as $row)
		{
            echo '<p>' . $row->CO_PRD . ' ' . 
                $row->NO_PRD . 
                    '16/08/2013 &nbsp; ' . 
                        anchor('liquidez/detalhe/' . $row->CO_PRD, 'R$ 365.956.333,00', 'title="Detalhe do valor de liquidez"') .
                            '</p>';
		}

        
        //$this->load->view('liquidez_detalhe');
        $this->load->view('footer');
    }
    
    

    //produto_dia
    public function ultimas_importacoes()
    {
    /*
    -- pega as últimas importações
    USE SIRAT
    
    SELECT P.NO_PRD, * FROM PRODUTO_DIA D JOIN PRODUTO P ON D.CO_PRD = P.CO_PRD
    
    WHERE 
    
    DT_ATU = 
    --	'2013-08-08'
    	(SELECT MAX(DT_ATU) FROM PRODUTO_DIA)
    
    AND DT_REF <>
    --	'2013-08-07'
    	(SELECT MAX(DT_REF) FROM PRODUTO_DIA)
    
    ORDER BY DT_REF, P.NO_PRD
    */
    }

    public function info()
    {
        phpinfo();
    }
	
	public function diferencas_xml($data_ref)
	{
	
	/**
		SELECT 	PP.DT_REF DATA, P.CO_PRD CNPJ, P.NO_PRD FUNDO, SUM(PP.VR_MERC_A) ATIVO, SUM(PP.VR_MERC_P) PASSIVO, SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) TOTAL, PD.VR_ATI 'VALOR XML',
			SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) - PD.VR_ATI DIFERENÇA
		FROM 	PRODUTO_POSICAO PP JOIN PRODUTO_DIA PD ON PD.DT_REF = PP.DT_REF AND PD.CO_PRD = PP.CO_PRD JOIN PRODUTO P ON P.CO_PRD = PP.CO_PRD
		WHERE 	PD.DT_ATU = (SELECT MAX(DT_ATU) FROM PRODUTO_DIA) AND P.CO_PRD <> '00000000000007'
			AND P.CO_CUSTOD = '00360305000104' 
		GROUP BY PP.DT_REF, P.CO_PRD, P.NO_PRD, PD.VR_ATI
		HAVING ABS(SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) - PD.VR_ATI) > 1000 AND ABS(SUM(PP.VR_MERC_A) - PD.VR_ATI) > 1000
		ORDER BY DIFERENÇA	
	**/		
	}
    
    
}