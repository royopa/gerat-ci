<?php 
/**
* Rotina_Diaria File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Rotina_Diaria_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class RotinaDiaria extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');
        if ($this->agent->browser() == 'Internet Explorer') {
            redirect('/gerat/navegadorSuportado', 'refresh');
            return;
        }        
        $this->load->database();
    }
    
    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/index
    *
    * @access  public
    * @return  null
    */    
    public function index()
    {
        $this->load->view('header');
        //$this->load->view('form_xml_nao_processados');
        $this->load->view('footer');        
    }

    /**
    * Formulário para consulta dos XMLs não processados no XIMP
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/liquidez/consultaXmlNaoProcessados
    *
    * @access  public
    *
    * @return void
    */
    public function consultaXmlNaoProcessados()
    {
        $this->load->view('header');
        $this->load->view('form_xml_nao_processados');
        $this->load->view('footer');        
    }

    /**
    * Formulário para consulta das diferenças dos XMLs processados no XIMP
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/liquidez/consultaXmlNaoProcessados
    *
    * @access  public
    *
    * @return void
    */
    public function consultaDiferencasXml()
    {
        $this->load->view('header');
        $this->load->view('form_diferencas_xml');
        $this->load->view('footer');        
    }

    /**
    * Lista dos fundos dos XMLs não processados no XIMP
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/liquidez/xmlNaoProcessadosList
    *
    * @access  public
    *
    * @return void
    */
    function xmlNaoProcessadosList()
    {
        $dados = array();

        $produto_model = new Produto_Model();
        
        //pega a data de referência
        $dt_atu = $this->input->get_post('dt_atu');

        $data['title'] 
            = 'Lista de XML não processados - Data de atualização ' . $dt_atu;
        
        $data['produtos'] = $produto_model->getXmlNaoProcessados($dt_atu);
 
        $this->load->view('header');
        $this->load->view('list_xml_nao_processados', $data);
        $this->load->view('footer');
    }

    /**
    * Lista dos fundos dos XMLs não processados no XIMP
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/liquidez/xmlNaoProcessadosList
    *
    * @access  public
    *
    * @return void
    */    
    function diferencasXmlList()
    {
        $dados = array();

        $produto_model = new Produto_Model();
        
        //pega a data de atualizacao
        $dt_atu = $this->input->get_post('dt_atu');

        $data['title'] 
            = 'Lista de diferenças nos XMLs - Data de atualização ' . $dt_atu;
        
        $data['produtos'] = $produto_model->getDiferencasXml($dt_atu);
 
        $this->load->view('header');
        $this->load->view('list_diferencas_xml', $data);
        $this->load->view('footer');
    }    
}