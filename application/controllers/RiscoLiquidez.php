<?php 
/**
* Liquidez File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Liquidez_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class RiscoLiquidez extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    /**
    * Lista os registros da tabela 11.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/tabela11
    *
    * @param string $co_prd O CNPJ do fundo/carteira
    * @param string $dt_atu A data de atualização do SIRAT/XIMPP - D+1 do XML
    *
    * @return void
    */
    public function tabela11($co_prd = 0, $dt_atu = 0)
    {
        $data = array();
        return $data;
    }

    /**
    * Lista os registros da tabela 12.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/tabela12
    *
    * @param string $co_prd O CNPJ do fundo/carteira.
    * @param string $dt_atu A data de atualização do SIRAT/XIMPP - D+1 do XML.
    *
    * @return array Um com os registros da tabela 12
    */
    public function tabela12($co_prd = 0, $dt_atu = 0)
    {
        $data = array();
        return $data;
    }

    /**
    * Lista os fundos e valores de liquidez diários.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/listaProdutosLiquidezDiaria
    *
    * @access  public
    *
    * @return void
    */
    public function listaProdutosLiquidezDiaria()
    {
        $dt_atu = $this->input->post('dt_atu');
        $dt_atu = formata_data($dt_atu);
        
        $data['title'] = 'Fundos - Valor de liquidez';
        $data['dt_atu'] = $dt_atu;
        
        $produto_model = new Produto_Model();
        
        $dt_ref = $produto_model->getDataReferencia($dt_atu);

        $data['produtos']
            = $produto_model->getPerfilCvmLiquidez($dt_ref);

        $this->load->view('header');
        $this->load->view('list_produtos_liquidez_cvm', $data);
        $this->load->view('footer');
    }

    /**
    * Lista as últimas negociações de um ativo.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/listaNegociacaoAtivo
    *
    * @access  public
    *
    * @return void
    */
    public function listaNegociacaoAtivo()
    {
        $dt_ref  = $this->input->get_post('dt_ref');
        $dt_atu  = $this->input->get_post('dt_atu');
        $co_ati  = $this->input->get_post('co_ati');
        $qtd_neg = $this->input->get_post('qtd_neg');
        
        $dt_ref = formata_data($dt_ref);
        $dt_atu = formata_data($dt_atu);
        
        $produto_model = new Produto_Model(); 
        $negociacao = $produto_model->detalheNegocMercadoTitPub($dt_atu, $co_ati, $qtd_neg);
        
        $data['negociacao'] = $negociacao;
        
        $this->load->view('header');
        $this->load->view('list_negociacao_ativo', $data);
        $this->load->view('footer');
    }

    /**
    * Formulário para consulta de liquidez diária dos fundos/carteiras
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/index.php/liquidez/pesquisa_liquidez
    *
    * @access  public
    *
    * @return void
    */
    public function pesquisaLiquidezDiaria()
    {
        $this->load->view('header');

        $dt_atu = $this->input->post('dt_atu');
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if ($this->form_validation->run() == false) {
            $this->load->view('form_liquidez_search', array('error' => ' ' ));
        } else {
            //faz a consulta no banco de dados
            //$dossie = $dossie->getDossies();
                        
            //$this->load->view('form_dossie_new', array('error' => ' ' ));
            //$this->load->view('form_dossie_search', array('error' => ' ' ));
            //$this->load->view('dossies_list', array('dossies' => $dossie ));
        }
        
        $this->load->view('footer');
    }

    /**
    * Formulário para consulta das últimas negociações de um ativo
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/index.php/liquidez/pesquisaNegociacaoAtivo
    *
    * @access  public
    *
    * @return void
    */
    function pesquisaNegociacaoAtivo()
    {
        $this->load->view('header');
        $this->load->view('form_liquidez_negociacao_search', array('error' => ' ' ));
        $this->load->view('footer');
    }
    
    /**
    * Lista a composição dos ativos de um fundo e a liquidez de cada um deles
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/index.php/liquidez/pesquisaNegociacaoAtivo
    *
    * @access  public
    *
    * @return void
    */
    public function detalheLiquidezDiaria()
    {
        //pega a data de referência e co_prd e dt_atu
        $dt_ref = $this->input->get('dt_ref');
        $dt_atu = $this->input->get('dt_atu');
        $co_prd = $this->input->get('co_prd');        
        
        $data['title'] = 'Detalhe valor de liquidez ' . $dt_ref;
        
        $data['dt_ref'] = formata_data_brasil($dt_ref);
        
        $data['dt_atu'] = $dt_atu;
        
        $produto_model = new Produto_Model();
        
        $data['produto'] = $produto_model->getProduto($co_prd);
        
        $data['posicao'] = $produto_model->getPosicaoAgrupadaAtivo($co_prd, $dt_ref, $dt_atu);

        $valorCaixa = $produto_model->getPosicaoCaixaDia($co_prd, $dt_ref);
        $data['valorCaixa'] = (float) $valorCaixa['0']['VR_CAIXA'];

        $fundoDiario = new FundoDiario(new Fundo($co_prd), new \Datetime($dt_atu));
        $data['fundoDiario'] = $fundoDiario;
        
        //$this->load->view('header_print');
        $this->load->view('header');
        $this->load->view('liquidez_detalhe', $data);
        $this->load->view('footer');        
    }
}