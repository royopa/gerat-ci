<?php
/**
* Sirat File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Sirat_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Sirat extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Produto_Model');
        $this->load->model('Liquidez_Model');
    }
    
    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *     http://endereco.site/gerat/index.php/sirat/index
    *
    * @access  public
    * @return  void
    */
    public function index()
    {
        $this->load->view('header');
        //
        $this->load->view('footer');
    }

    public function riscoCredito()
    {
        //pega a dt_atu via get
        $dt_atu = $this->input->get('dt_atu');
        $dt_atu = formata_data($dt_atu);

        //instancia o model para fazer acesso aos métodos de banco de dados
        $produto_model = new Produto_Model();
        
        //se a dt_atu não está setada, pega a maior data atu dabase de dados
        if ($dt_atu == 0) {
            $dt_atu = $produto_model->getMaxDtAtu();
        }


        //$dt_atu = '2013-08-26';
        $data['dt_atu'] = $dt_atu;


        //var_dump($produto_model->getCarteiraCreditoGeral($dt_atu));
        //var_dump($produto_model->getLimitesNaoIfGeral($dt_atu));
        //var_dump($produto_model->getFidcsGeral($dt_atu));

        //$this->load->view('header');
        //
        //$this->load->view('footer');
    }

    public function riscoLiquidez()
    {
        //pega a dt_atu via get
        $dt_atu = $this->input->get('dt_atu');
        $dt_atu = formata_data($dt_atu);

        //instancia o model para fazer acesso aos métodos de banco de dados
        $produto_model  = new Produto_Model();
        $liquidez_model = new Liquidez_Model();
        
        //se a dt_atu não está setada, pega a maior data atu dabase de dados
        if ($dt_atu == 0) {
            $dt_atu = $produto_model->getMaxDtAtu();
        }

        //$dt_atu = '2013-08-26';
        $data['dt_atu'] = $dt_atu;

        var_dump($liquidez_model->getComposicaoCarteiraGeral($dt_atu));

        //var_dump($produto_model->getCarteiraCreditoGeral($dt_atu));
        //var_dump($produto_model->getLimitesNaoIfGeral($dt_atu));
        //var_dump($produto_model->getFidcsGeral($dt_atu));

        //$this->load->view('header');
        //
        //$this->load->view('footer');
    }    
}