<?php
/**
* Relatórios File Doc Comment
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
*
*/
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
* Relatorios_Controller
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
*/
class Relatorios extends CI_Controller
{
    /**
    * Construct method for this controller.
    */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('user_agent');
        if ($this->agent->browser() == 'Internet Explorer') {
            redirect('/gerat/navegadorSuportado', 'refresh');
            return;
        }

        $this->load->database();
        $ConnectionParamsSqlite = array(
            'driver'   => 'pdo_sqlite',
            'memory'   => false,
            'path'     => APPPATH . '/data/db.sqlite',
        );
        $dbal            = new Dbal($ConnectionParamsSqlite);
        $this->conn      = $dbal->getConn();
        $this->relatorio = new Relatorio($this->conn);


        /*
        $useragent = $_SERVER['HTTP_USER_AGENT'];
         
        if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
            $browser_version=$matched[1];
            $browser = 'IE';
        } elseif (preg_match( '|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched)) {
            $browser_version=$matched[1];
            $browser = 'Opera';
        } elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
            $browser_version=$matched[1];
            $browser = 'Firefox';
        } elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
            $browser_version=$matched[1];
            $browser = 'Chrome';
        } elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
            $browser_version=$matched[1];
            $browser = 'Safari';
        } else {
            // browser not recognized!
            $browser_version = 0;
            $browser= 'other';
        }
        print "browser: $browser $browser_version";
        */
       
    }

    /**
    * Relatório de risco, detalhe da composição do fundo e indicadores de risco.
    *
    * Maps to the following URL
    *     http://endereco.site/index.php/gerat/relatorioRisco
    *
    * @access  public
    * @return  void
    */
    public function relatorioRiscoOld()
    {
        //pega o indicador se é para mostrar a versão para impressão
        $print = $this->input->get('print');
        //pega o id do relatório que deverá ser exibido
        $idRelatorio = (int) $this->input->get('id_relatorio');
        $dataAtualizacao = $this->input->get('dt_atu');

        if ($dataAtualizacao == false) {
            $dbal = new Dbal();
            $conn = $dbal->getConn();
            $data
                =
                $conn->fetchAssoc(
                    'SELECT MAX(DT_ATU) DT_ATU FROM Produto_Dia'
                );

            $dataAtualizacao = $data['DT_ATU'];
        }

        $dataAtualizacao = formata_data($dataAtualizacao);

        //instancia o objeto fundoDiario
        $fundoDiario = new FundoDiario(
            new Fundo($this->input->get('co_prd')),
            new DateTime($dataAtualizacao)
        );

        if ($fundoDiario->getPatrimonioLiquido() == 0) {
            show_404('page');
        }

        $fundoDiario->processaDependencias();

        $data['fundoDiario'] = $fundoDiario;
        $data['idRelatorio'] = $idRelatorio;

        //carrega as views - Relatório padrão - id = 1
        $this->carregaViews($data, $print, $idRelatorio);
    }

    public function carregaViews($data, $print = null, $idRelatorio = 0)
    {
        $this->carregaHeaderRelatorio($data, $print);

        $this->load->view('relatorio_risco/topo', $data); //1

        //chama o relatório para mostrar as views configuradas
        $this->relatorio->get($idRelatorio);

        foreach ($this->relatorio as $view) {
            $this->load->view($view->getPath(), $data);
        }

        $this->carregaFooterRelatorio();
    }

    /**
    * Carrega o header para os relatórios
    *
    * @access  public
    * @return  void
    */
    public function carregaHeaderRelatorio($data = null, $print = null)
    {
        if ($print == 'yes') {
            $this->load->view('header_print'); //16
            $this->load->view('relatorio_risco/topo_print_relatorio'); //15
        } else {
            $this->load->view('header', $data); //17
            $this->load->view('relatorio_risco/navbar', $data); //14
        }
    }

    /**
    * Carrega o footer para os relatórios
    *
    * @access  public
    * @return  void
    */
    public function carregaFooterRelatorio()
    {
        $this->load->view('relatorio_risco/footer'); //12
        $this->load->view('footer'); //18
    }

    /**
    * Lista os fundos que já tem a atribuição de performance calculada
    *
    * @access  public
    * @return  void
    */
    public function atribuicaoPerformance()
    {
        
        return redirect('http://10.4.45.208/gerat-sf/web/app.php/relatorio_atribuicao_performance', 'refresh');

        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $sql
            = "
            SELECT DISTINCT
                o.co_prd
            FROM
                arr_produto_dia o 
            WHERE
                o.IC_CHK_RET_PRD_DIA = 1 and
                o.co_prd not in
                    (SELECT DISTINCT
                        x.co_prd 
                    FROM
                        arr_produto_dia x
                    WHERE
                        x.IC_CHK_RET_PRD_DIA <> 1 and
                        x.dt_ref between '2013-09-03' and '2013-10-01')
                and o.dt_ref between '2013-09-03' and '2013-10-01'
            ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $data    = array();
        $codigos = array();

        while ($row = $stmt->fetch()) {
            $codigos[] = $row['co_prd'];
        }

        $conn = $dbal->getConn();

        $stmt = $conn->executeQuery('SELECT CO_PRD, NO_PRD FROM produto WHERE CO_PRD IN (?)',
            array($codigos),
            array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
        );

        $data['produtos'] = $stmt->fetchAll();

        $this->twiggy->title('Relatório');
        $this->twiggy->title()->prepend('Atribuição de Performance');

        $this->twiggy->set($data, NULL);        
        $this->twiggy->display('lista_fundos_atribuicao_performance');

        //var_dump($data['produtos']);
        //var_dump($codigos);

    }
}
