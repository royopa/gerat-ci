<h1><?php echo $title; ?></h1>
<table id="table_export" >
  <thead>
    <tr>
      <th width="1%">#</th>
      <th>CNPJ</th>
      <th>Nome</th>
      <th>Data informe</th>
      <th>Valor liquidez fundo</th>
      <th>Valor calculado</th>
      <th>Diferença</th>
    </tr>
	</thead>
    <tbody>
        <?php $i = 1; ?>
        <?php foreach ($produtos as $row) : ?>
<?php
    //$fundoDiario = new FundoDiario(new Fundo($row['CO_PRD']), new \Datetime($dt_atu));
    //$fundoDiario->setValorLiquidezCalculado($valorLiquido);
    if ($i > 0) {
        $url = 'http://localhost/gerat/index.php/RiscoLiquidez/detalheLiquidezDiaria?co_prd=' . $row['CO_PRD'] . '&dt_ref=' . $row['DT_REF'] . '&dt_atu=' . $dt_atu;
        $homepage = file_get_contents($url);
    }
    ?>
    <tr>
			<td><?php echo $i; ?></td>
      <td><?php echo $row['CO_PRD']; ?></td>
			<td><?php echo $row['NO_PRD']; ?></td>
			<td><?php echo formata_data_brasil($row['DT_REF']); ?></td>
      <td>
        <?php echo anchor(
          'RiscoLiquidez/detalheLiquidezDiaria?co_prd=' . $row['CO_PRD'] . '&dt_ref=' . $row['DT_REF'] . '&dt_atu=' . $dt_atu, 
          'R$ ' . number_format($row['LIQ'],2,",","."), 
          'title="Detalhe do valor de liquidez"'); ?>
      </td>
      <td>
        R$ <?php echo number_format($row['LIQ_CONF'],2,",","."); ?>
      </td>
      <td>
        R$ <?php echo number_format($row['LIQ'] - $row['LIQ_CONF'],2,",","."); ?>
      </td>      
		</tr>
        <?php $i++; ?>
        <?php endforeach; ?>
    </tbody>
</table>