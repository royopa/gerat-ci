  <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div> <!-- /container -->
  <!-- jQuery  -->
  <!-- <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script> -->
  <!-- jQuery DataTables -->
  <script src="<?php echo base_url(); ?>assets/js/DataTables-1.9.4/media/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/DataTables-1.9.4/extras/TableTools/media/js/TableTools.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/dataTables.data-eu.js"></script>
  <!-- jQuery UI -->
  <script src="<?php echo base_url(); ?>assets/css/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>  
  <!-- jQuery Validation --> 
  <!-- <script src="<?php echo base_url(); ?>assets/js/jquery-validation/dist/jquery.validate.min.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>assets/js/jquery-validation/dist/additional-methods.min.js"></script> -->
  <!-- <script src="<?php echo base_url(); ?>assets/js/jquery-validation/localization/messages_PT_BR.js"></script> -->
  <!-- Funções da página -->
  <script src="<?php echo base_url(); ?>assets/js/gerat.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url(); ?>assets/css/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/css/bootstrap/assets/js/holder.js"></script>
  <!-- Nestable jQuery -->
  <script src="<?php echo base_url(); ?>assets/js/jquery.nestable.js"></script>  
  <!-- Chart sem integração, somente visualização -->
  <script src="<?php echo base_url(); ?>assets/js/Chart.js-master/Chart.js"></script>
  <!-- jQuery Highcharts -->
  <script src="<?php echo base_url(); ?>assets/js/highcharts/js/highcharts.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/highcharts/js/modules/exporting.js"></script>
  <!--[if IE]><script src="<?php echo base_url(); ?>assets/js/excanvas.js"></script><![endif]-->    
</body>
</html>