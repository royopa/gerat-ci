<?php $listaDistribuicaoCotistas = new ListaDistribuicaoCotistas($fundoDiario); ?>
<?php if (count($listaDistribuicaoCotistas) > 0) : ?>
<!-- Distribuição dos cotistas de um fundo -->
<div class="row">
  <div class="col-md-6">
    <!-- Perfil de distribuição dos cotistas -->
    <h2 class="panel_title" id="titulo_painel_2">
    Perfil de Distribuição - Cotistas
    </h2>    
    <div class="panel panel-info">
      <table class="table bordered-table">
        <thead>
          <tr>
            <th>FIC/Carteira</th>
            <th>Valor aplicado</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listaDistribuicaoCotistas as $distribuicaoCotistas) : ?>
          <tr>
            <td><?php echo $distribuicaoCotistas->getNome() ?></td>
            <td><?php echo formataMoeda($distribuicaoCotistas->getValor()) ?></td>
            <td><?php echo round($distribuicaoCotistas->getPercentual(), 2) ?>%</td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>    
    </div>
  </div><!-- /.col-md-6 -->
  
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_distribuicao_cotistas"></div>
        <script type="text/javascript"><?php echo $listaDistribuicaoCotistas->getGrafico()->render("chart1"); ?></script>
      </div>
    </div>
  </div><!-- /.col-md-6 -->  
</div><!-- Fim - distribuição dos cotistas de um fundo -->
<?php endif; ?>