<?php if (count($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteFidcGrupo()) > 0) : ?>
<div class="row">
<h2 class="panel_title" id="titulo_painel_2">
    FIDC 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    R$ <?php 
      echo formataMoeda($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteIfGrupo()
            ->getValorTotal()); ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
    <?php 
      echo round($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteIfGrupo()
            ->getPercentualTotal(), 2); ?>% PL
</h2>
<div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Contraparte</th>
            <th>Rating</th>
            <th>Alocado</th>
            <th>Limite % PL</th>            
            <th>Alocado % PL</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th>% Limite - Resgate Stress</th>
            <th width="2px">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </th>
          </tr>
        </thead>
        <tbody class="table table-bordered">
          <?php foreach ($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteFidcGrupo() as $limite) : ?>
          <tr>
            <td>
              <?php echo $limite->getEmissor()->getNome(); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getRating(); ?>
            </td>
            <td>
              R$ <?php echo formataMoeda($limite->getValorAlocado()); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getPercentualLimite(); ?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocado(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimite(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimiteStress(), 2)?>%
            </td>
            <td>
              <?php if (($limite->getPercentualAlocadoLimite() >= 70) && 
                ($limite->getPercentualAlocadoLimite() < 90)) : ?>
              <span class="label alerta-amarelo">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 90) && 
                ($limite->getPercentualAlocadoLimite() < 100)) : ?>
              <span class="label alerta-laranja">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 100)) : ?>
              <span class="label alerta-vermelho">Alerta</span>
              <?php endif; ?>
            </td>            
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
  </div>
</div>
<?php endif; ?>