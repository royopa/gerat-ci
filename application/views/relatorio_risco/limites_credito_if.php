
<?php if (is_object($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteIfGrupo())) : ?>
<?php if (count($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteIfGrupo()) > 0) : ?>
<div class="row">
<h2 class="panel_title" id="titulo_painel_2">
    Instituições Financeiras 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    R$ <?php 
      echo formataMoeda($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteIfGrupo()
            ->getValorTotal()); ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
    <?php 
      echo round($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteIfGrupo()
            ->getPercentualTotal(), 2); ?>% PL
</h2>
<div class="col-md-12">
  <div class="panel panel-info">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Contraparte</th>
            <th>Rating</th>
            <th>Alocado</th>
            <th>Limite % PL</th>            
            <th>Alocado % PL</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th>% Limite - Resgate Stress</th>
            <th width="2px">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </th>
          </tr>
        </thead>
        <tbody class="table table-bordered">
          <?php foreach ($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteIfGrupo() as $limite) : ?>
          <tr>
            <td>
              <?php echo $limite->getEmissor()->getNome(); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getRating(); ?>
            </td>
            <td>
              R$ <?php echo formataMoeda($limite->getValorAlocado()); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getPercentualLimite(); ?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocado(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimite(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimiteStress(), 2)?>%
            </td>
            <td>
              <?php if (($limite->getPercentualAlocadoLimite() >= 70) && 
                ($limite->getPercentualAlocadoLimite() < 90)) : ?>
              <span class="label alerta-amarelo">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 90) && 
                ($limite->getPercentualAlocadoLimite() < 100)) : ?>
              <span class="label alerta-laranja">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 100)) : ?>
              <span class="label alerta-vermelho">Alerta</span>
              <?php endif; ?>
            </td>            
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
  <!-- Gráfico Limites IF -->
    <div class="panel panel-info">
        <div class="panel-body">
            <div id="chart_limite_if"></div>
            <script type="text/javascript">
              <?php echo $fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteIfGrupo()->getGrafico()->render("chart1"); ?>
            </script>
        </div><!-- Fim - Quadro/Gráfico Captação/Rentabilidade -->
    </div>
  </div>
</div>
<?php endif; ?>
<?php endif; ?>