<?php $cronogramaVencimentoGrupo = new CronogramaVencimentoGrupo($fundoDiario); ?>
<?php if (count($cronogramaVencimentoGrupo) > 0) : ?>
<div class="row">
  <h2 class="panel_title" id="titulo_painel_2">Risco de Liquidez</h2>
  <!-- Composição da Carteira de Crédito -->
  <div class="col-md-4">
    <div class="panel panel-info">
      <table class="table table-bordered" id="table_carteira_credito">
        <thead>
          <tr class="text-center">
            <th>Data vencimento</th>
            <th>Volume</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php foreach ($cronogramaVencimentoGrupo as $cronograma) : ?>
          <tr class="text-center">
            <td><?php echo $cronograma->getDataVencimento()->format('m/Y'); ?></td>
            <td>R$ <?php echo formataMoeda(abs($cronograma->getValorVolume())); ?></td>
            <td><?php echo round($cronograma->getPercentual(), 2) . '%'; ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div><!-- /.col-md-5 -->
  
  <!-- Gráfico da composição da carteira de crédito -->
  <div class="col-md-8">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_cronograma_vencimento"></div>
        <script type="text/javascript">
          <?php echo $cronogramaVencimentoGrupo->getGrafico()->render("chart1"); ?>
        </script>        
      </div>
    </div>
  </div><!-- /.col-md-6 -->

</div>
<?php endif; ?>