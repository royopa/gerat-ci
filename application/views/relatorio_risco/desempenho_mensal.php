<?php 
  $desempenhoMensalGrupo = new DesempenhoMensalGrupo($fundoDiario);
  $grafico = $desempenhoMensalGrupo->getGrafico();
  $desempenhoMensalGrupo = array_reverse($desempenhoMensalGrupo->getArrayCopy());
?>
<?php if (count($desempenhoMensalGrupo) > 0) : ?>
<div class="row">
  <h2 class="panel_title" id="titulo_painel_2">Desempenho do Fundo - Mensal</h2>
  <!-- Composição da Carteira de Crédito -->
  <div class="col-md-5">
    <div class="panel panel-info">
      <table class="table table-bordered" id="table_carteira_credito">
        <thead>
          <tr>
            <th>Mês</th>
            <th>Rentabilidade Fundo (%)</th>
            <th>Rentabilidade Bench (%)</th>
            <th>Rentabilidade Fundo (% Bench)</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php foreach ($desempenhoMensalGrupo as $desempenho) : ?>
          <tr class="text-center">
            <td><?php echo $desempenho->getData()->format('m/Y'); ?></td>
            <td <?php if ($desempenho->getRentabilidadeFundo() < 0) { echo "class='valor-negativo'"; } ?>>
              <?php echo $desempenho->getRentabilidadeFundo(); ?>%
            </td>
            <td <?php if ($desempenho->getRentabilidadeBenchmark() < 0) { echo "class='valor-negativo'"; } ?>>
              <?php echo $desempenho->getRentabilidadeBenchmark(); ?>%
            </td>
            <td>
              <?php if (($desempenho->getRentabilidadeBenchmark() > 0) && ($desempenho->getRentabilidadeFundo() > 0)) : ?>
              <?php echo $desempenho->getPercentual(); ?>%
              <?php endif; ?>              
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div><!-- /.col-md-5 -->

  <!-- Quadro/Gráfico Captação/Rentabilidade -->
  <div class="col-md-7">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_desempenho_mensal">
        </div>
          <script type="text/javascript">
            <?php echo $grafico->render("chart1"); ?>
          </script>
      </div>
    </div><!-- Fim - Quadro/Gráfico Captação/Rentabilidade -->
  </div>

</div>
<?php endif; ?>