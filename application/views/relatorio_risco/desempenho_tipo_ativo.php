<?php $desempenhoTipoAtivoGrupo = new DesempenhoTipoAtivoGrupo($fundoDiario); ?>
<?php if (count($desempenhoTipoAtivoGrupo) > 0) : ?>
<div class="row">
  <h2 class="panel_title" id="titulo_painel_2">Desempenho por Tipo de Ativo</h2>
  <!-- Composição da Carteira de Crédito -->
  <div class="col-md-5">
    <div class="panel panel-info">
      <table class="table table-hover table-striped" id="table_carteira_credito">
        <thead>
          <tr class="text-center">
            <th>Tipo</th>
            <th>Ativos</th>
            <th>Resultado Financeiro (R$)</th>
            <th>Participação Rentabilidade</th>
          </tr>
        </thead>
<!--
          <?php 
              $arrayTipos  = array();
              $arrayPassou = array();
              $rowSpan = '';
              foreach ($desempenhoTipoAtivoGrupo as $desempenho) {
                  //if ()
                  if (array_key_exists($desempenho->getTipo(), $arrayTipos) && $desempenho->getTipo() != '') {
                      //adiciona um no contador
                      $qtd = $arrayTipos[$desempenho->getTipo()];
                      $arrayTipos[$desempenho->getTipo()] = $qtd + 1;
                  } else {
                      $arrayTipos[$desempenho->getTipo()] = 1;
                  }
              }
            ?>
          <?php 
              //var_dump($arrayTipos[$desempenho->getTipo()]);
              if ($arrayTipos[$desempenho->getTipo()] > 1) {
                  $rowSpan = 'rowspan="' . $arrayTipos[$desempenho->getTipo()] . '"';
              }
          ?>            
            <?php if ( ! in_array($desempenho->getTipo(), $arrayPassou)) : ?>
            <td <?php echo $rowSpan; ?>>
              <?php //if ($desempenho->getTipo() != 'Despesas') : ; ?>
              <?php echo $desempenho->getTipo(); ?>
              <?php //endif; ?>
            </td>
            <?php endif; ?>
          <?php $arrayPassou[] = $desempenho->getTipo(); ?>
          -->
        <tbody>
          <?php foreach ($desempenhoTipoAtivoGrupo as $desempenho) : ?>
          <tr class="text-center">
            <td><?php echo $desempenho->getTipo(); ?></td>
            <td><?php echo $desempenho->getNome(); ?></td>
            <td <?php if ($desempenho->getValor() < 0) { echo "class='valor-negativo'"; } ?>>
              R$ <?php echo formataMoeda($desempenho->getValor()); ?>
            </td>
            <td <?php if ($desempenho->getPercentual() < 0) { echo "class='valor-negativo'"; } ?>>
              <?php echo round($desempenho->getPercentual(), 2) . '%'; ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div><!-- /.col-md-5 -->

  <!-- Gráfico -->
  <div class="col-md-7">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_desempenho_tipo_ativo"></div>
        <script type="text/javascript">
          <?php echo $desempenhoTipoAtivoGrupo->getGrafico()->render("chart1"); ?>
        </script>        
      </div>
    </div>
  </div><!-- /.col-md-7 -->
</div>
<?php endif; ?>
