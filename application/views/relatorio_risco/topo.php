<!--
  H1 com o nome do fundo
-->
<div class="page-header">
  <h1 id="nome_fundo_h1"><?php echo $fundoDiario->getFundo()->getNome(); ?></h1>
</div>
<!--
  Fim do H1 com o nome do fundo
-->

<!--
  PL e data de atualização
-->
<div class="row" id="info_fundo">
  <strong id="posicao_dt_atu">Data de Referência: <?php echo $fundoDiario->getDataReferencia()->format('d/m/Y'); ?></strong>
  <p class="text-left">
    <strong id="patrimonio_liquido_span">Patrimônio Líquido R$ <?php echo formataMoeda($fundoDiario->getPatrimonioLiquido()); ?></strong>
  </p>
</div>

<div class="row">
    <dl class="dl-horizontal-description">
      <dt>CNPJ:</dt>
      <dd><?php echo formatarCPF_CNPJ($fundoDiario->getFundo()->getCnpj(), true); ?></dd>
      <dt>Início do Fundo:</dt>
      <dd><?php echo $fundoDiario->getFundo()->getDataInicio()->format('d/m/Y'); ?></dd>
      <?php if (strlen($fundoDiario->getFundo()->getBenchmark()->getNome()) > 0) : ?>
      <dt>Benchmark:</dt>
      <dd><?php echo $fundoDiario->getFundo()->getBenchmark()->getNome(); ?></dd>
      <?php endif; ?>
    </dl>
</div>
<!--
  Fim do PL e data de atualização
-->