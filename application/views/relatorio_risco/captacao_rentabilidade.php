<?php $listaCaptacaoRentabilidade = new ListaCaptacaoRentabilidade($fundoDiario) ?>
<div class="row">
  <!-- Quadro/Gráfico Captação/Rentabilidade -->
  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_captacao_rentabilidade">
        </div>
          <script type="text/javascript">
            <?php echo $listaCaptacaoRentabilidade->getGrafico()->render("chart1"); ?>
          </script>
      </div>
    </div><!-- Fim - Quadro/Gráfico Captação/Rentabilidade -->
  </div>
</div>