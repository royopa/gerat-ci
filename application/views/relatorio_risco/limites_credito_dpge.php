<?php if (count($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteIfDpgeGrupo()) > 0) : ?>
<div class="row">
<h2 class="panel_title" id="titulo_painel_2">
    DPGE 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    R$ <?php 
      echo formataMoeda($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteIfDpgeGrupo()
            ->getValorTotal()); ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php 
      echo round($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteIfDpgeGrupo()
            ->getPercentualTotal(), 2); ?>% PL  
</h2>
<div class="col-md-12">
  <div class="panel panel-info">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Conglomerado</th>
            <th>Limite</th>
            <th>Valor mercado</th>
            <th>Valor alocado</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th width="1px">&nbsp;</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php foreach ($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteIfDpgeGrupo() as $limite) : ?>
          <tr>
            <td>
              <?php echo $limite->getEmissor()->getNome(); ?>
            </td>
            <td>
              <?php echo formataMoeda($limite->getEmissor()->getValorLimite()); ?>
            </td>
            <td>
              <?php echo formataMoeda($limite->getValorMercado()); ?>
            </td>
            <td>
              R$ <?php echo formataMoeda($limite->getValorAlocado()); ?>
            </td>
            <td>
              <?php echo formataMoeda($limite->getPercentualAlocadoLimite()); ?>%
            </td>
            <td>
              <?php if (($limite->getPercentualAlocadoLimite() >= 70) && 
                ($limite->getPercentualAlocadoLimite() < 90)) : ?>
              <span class="label alerta-amarelo">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 90) && 
                ($limite->getPercentualAlocadoLimite() < 100)) : ?>
              <span class="label alerta-laranja">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 100)) : ?>
              <span class="label alerta-vermelho">Alerta</span>
              <?php endif; ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php endif; ?>