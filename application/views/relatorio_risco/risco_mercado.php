<?php
  $riscoMercado          = new RiscoMercado($fundoDiario);
  $itemRiscoMercadoGrupo = new ItemRiscoMercadoGrupo($fundoDiario, $riscoMercado);
?>

<?php if (count($itemRiscoMercadoGrupo) > 0) : ?>
<div class="row">
  <h2 class="panel_title" id="titulo_painel_2">Risco de Mercado - VaR</h2>
  <!-- Risco de Mercado -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
      <dl class="dl-horizontal-description">
        <dt>Método VaR:</dt>
        <dd><?php echo $riscoMercado->getMetodoVar(); ?></dd>
        <dt>Nível de Confiança: </dt>
        <dd><?php echo $riscoMercado->getNivelConfianca(); ?>%</dd>        
        <dt>Horizonte Tempo:</dt>
        <dd><?php echo $riscoMercado->getHorizonteTempo(); ?></dd>
        <dt>VaR: </dt>
        <dd>R$ <?php echo formataMoeda(round($riscoMercado->getValorVar1d95(), 2)); ?></dd>
        <dt>VaR / PL</dd>
        <dd><?php echo round($riscoMercado->getPercentualVarPatrimonioLiquido(), 4); ?>%</dd>
        <dt>Limite VaR</dt>
        <dd>3,00%</dd>
      </dl>

      <table class="table table-bordered">
            <thead>
              <tr>
                <th>Ativo</th>
                <th>MtM</th>
                <th>CVaR</th>
              </tr>
            </thead>
            <tbody class="table-hover table-striped">
              <?php 
                  $soma_vr_carteira_rc = 0;
                ?>
                <?php foreach ($itemRiscoMercadoGrupo as $itemRiscoMercado) : ?>
                <tr>
                  <td><?php echo $itemRiscoMercado->getNome(); ?></td>
                  <td <?php if ($itemRiscoMercado->getValorMtm() < 0) { echo "class='valor-negativo'"; } ?>>
                    <?php if ($itemRiscoMercado->getValorMtm() == 0) : ?>
                      -
                    <?php else: ?>
                      R$ <?php echo formataMoeda(($itemRiscoMercado->getValorMtm())); ?>
                    <?php endif; ?>
                  </td>
                  <td <?php if ($itemRiscoMercado->getValorCvar() < 0) { echo "class='valor-negativo'"; } ?>>                      
                    <?php if ($itemRiscoMercado->getValorCvar() == 0) : ?>
                      -
                    <?php else: ?>
                      R$ <?php echo formataMoeda(($itemRiscoMercado->getValorCvar())); ?>
                    <?php endif; ?>            
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
      </table>      
    </div>
    
    </div>
  </div><!-- /.col-md-6 -->

  <!-- Gráfico -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_risco_mercado"></div>
        <script type="text/javascript">
          <?php echo $itemRiscoMercadoGrupo->getGrafico()->render("chart1"); ?>
        </script>        
      </div>
    </div>
  </div><!-- /.col-md-7 -->

</div>
<?php endif; ?>