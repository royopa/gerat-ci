<?php $listaComposicaoCarteiraCredito = new ListaComposicaoCarteiraCredito($fundoDiario) ?>
<?php if (count($listaComposicaoCarteiraCredito) > 0) : ?>
<div class="row">
  <h2 class="panel_title" id="titulo_painel_2">Risco de Crédito</h2> 
  <!-- Composição da Carteira de Crédito -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <table class="table table-bordered" id="table_carteira_credito">
        <thead>
          <tr>
            <th>Ativos</th>
            <th>Total de Investimento</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php 
            $soma_vr_carteira_rc = 0;
          ?>
          <?php foreach ($listaComposicaoCarteiraCredito as $composicao) : ?>
          <tr>
            <td><?php echo $composicao->getNome(); ?></td>
            <td>R$ <?php echo formataMoeda(abs($composicao->getValor())); ?></td>
            <td><?php echo round($composicao->getPercentual(), 2) . '%'; ?></td>
          </tr>
          <?php 
            $soma_vr_carteira_rc = $soma_vr_carteira_rc + abs($composicao->getValor());
          ?>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div><!-- /.col-md-6 -->
  
  <!-- Gráfico da composição da carteira de crédito -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_composicao_credito"></div>
        <script type="text/javascript">
          <?php echo $listaComposicaoCarteiraCredito->getGrafico()->render("chart1"); ?>
        </script>        
      </div>
    </div>
  </div><!-- /.col-md-6 -->

</div>
<?php endif; ?>