<?php $listaComposicaoCarteira = new ListaComposicaoCarteira($fundoDiario) ?>
<?php if (count($listaComposicaoCarteira) > 0) : ?>
<div class="row">
  <h2 class="panel_title" id="titulo_painel_2">Alocação - Composição da Carteira *</h2>
  <!-- Composição da Carteira -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <table class="table table-bordered">
        <thead>
          <tr class="text-center">
            <th>Ativos</th>
            <th>Total de Investimento</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php foreach ($listaComposicaoCarteira as $composicao) : ?>
          <tr class="text-center">
            <td><?php echo $composicao->getNome(); ?></td>
            <td>R$ <?php echo formataMoeda($composicao->getValor()); ?></td>
            <td><?php echo round($composicao->getPercentual(), 2) . '%'; ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      </div>
  </div><!-- /.col-md-6 -->
  
  <!-- Gráfico da composição da carteira -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="pie_chart_composicao_carteira"></div>
        <script type="text/javascript">
          <?php echo $listaComposicaoCarteira->getGrafico()->render("chart1"); ?>
        </script>
      </div>
    </div>
  </div><!-- /.col-md-6 -->
</div>
<?php endif; ?>