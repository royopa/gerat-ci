<?php 
  $liquidezGrupo      = new LiquidezGrupo($fundoDiario);
  $termometroLiquidez = new TermometroLiquidez($fundoDiario);
?>
<div class="row">
<h2 class="panel_title" id="titulo_painel_2">Risco de Liquidez</h2>
  <div class="col-md-7">
    <div class="panel panel-info">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Quadro</th>
            <th>Volume Líquido</th>
            <th>Valor Mercado</th>
            <th>% Liquidez</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php foreach ($liquidezGrupo as $liquidez) : ?>
          <tr>
            <td><?php echo $liquidez->getNome(); ?></td>
            <td>R$ <?php echo formataMoeda($liquidez->getVolumeLiquido()); ?></td>
            <td>R$ <?php echo formataMoeda($liquidez->getValorMercado()); ?></td>
            <td><?php echo round($liquidez->getPercentualLiquidez(), 2); ?>%</td>
          </tr>
          <?php endforeach; ?>
          <tr>
            <td>Total volume líquido - VL</td>
            <td>R$ <?php echo formataMoeda($liquidezGrupo->getVolumeLiquidoTotal()); ?></td>
            <td>R$ <?php echo formataMoeda($liquidezGrupo->getValorMercadoTotal()); ?></td>
            <td><?php echo round($liquidezGrupo->getPercentualLiquidezTotal(), 2); ?>%</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td>Valor em garantia</td>
            <td>R$ <?php echo formataMoeda($liquidezGrupo->getValorEmGarantia()); ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>

        </tfoot>        
      </table>
    </div>
  </div><!-- /.col-md-6 -->
  
  <!-- Indicadores de liquidez -->
  <div class="col-md-5">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" id="titulo_painel">Indicadores de Liquidez</h3>
      </div>
      <table class="table table-bordered">
        <tbody class="">
          <tr>
            <td>Resgate projetado (RP)</td>
            <td>
              R$ 
              <?php echo formataMoeda($termometroLiquidez->getVolumeResgateProjetado()); ?>
            </td>
          </tr>
          <tr>
            <td>RL1 - RP / VL</td>
            <td>
              <?php echo $termometroLiquidez->getIndicadorRl1(); ?>%
            </td>
          </tr>
          <tr>
            <td>"Resgate Stress </br>(Maior resgate - 3 anos)"</td>
            <td>R$ <?php echo formataMoeda($fundoDiario->getMaiorResgateUltimos3Anos()) ?></td> 
          </tr>                    
        </tbody>
      </table>     
    </div>
  </div><!-- /.col-md-6 -->

</div>