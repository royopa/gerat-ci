<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
    <a class="navbar-brand" href="#">&nbsp;</a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li>
        <?php echo anchor(
                    'relatorios/relatorioRisco?co_prd=' . 
                    $fundoDiario->getFundo()->getCnpj() . 
                    '&print=yes' . 
                    '&dt_atu=' . $fundoDiario->getDataAtualizacao()->format('d/m/Y') . 
                    '&id_relatorio=' . $idRelatorio, 
                    '<i class="glyphicon glyphicon-print" ></i>', 
                      array(
                        'title'  => 'Gerar versão para impressão',
                        'target' => '_blank')
                        ); ?>
      </li>
      <!--
      <li><a href="#">Link</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li><a href="#">Separated link</a></li>
          <li><a href="#">One more separated link</a></li>
        </ul>
      </li>
      -->
    </ul>
    <?php 
    $attributes = array('class' => 'navbar-form navbar-right', 'id' => 'form_consulta', 'method' => 'get');
    echo form_open('relatorios/relatorioRisco', $attributes); 
    ?>
      <div class="form-group">
        <input type="hidden" id="co_prd" name="co_prd" value="<?php echo $fundoDiario->getFundo()->getCnpj(); ?>" />
        <input type="text" class="form-control" value="<?php echo $fundoDiario->getDataAtualizacao()->format('d/m/Y'); ?>" name="dt_atu" id="dt_atu" maxlength="10">
      </div>
      <button type="submit" class="btn btn-default">Atualiza data</button>
      <label for="dt_atu" class="error label label-danger"></label>
      <input type="hidden" id="id_relatorio" name="id_relatorio" value="<?php echo $idRelatorio; ?>" />
    </form>
  </div>
</nav>