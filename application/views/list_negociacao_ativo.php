<table id="table_export" >
    <thead>
        <tr>
            <th width="1%">#</th>
            <th>Ativo</th>
            <th>Data Negociação</th>
            <th>VR_PU</th>
            <th>NU_NEG_CONTR</th>
            <th>VR_NEG_VOLUME</th>
            <th>VR_PU_MED</th>
        </tr>
	</thead>
    <tbody>
        <?php 
        $i = 1;
        $soma_qtd = 0; 
        $media_qtd = 0;
        foreach ($negociacao as $row) : 
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['CO_ATI']; ?></td>
            <td><?php echo formata_data_brasil($row['DT_REF']); ?></td>
            <td><?php echo $row['VR_PU']; ?></td>
            <td><?php echo $row['NU_NEG_CONTR']; ?></td>
            <td><?php echo $row['VR_NEG_VOLUME']; ?></td>
            <td><?php echo $row['VR_PU_MED']; ?></td>
		</tr>
        <?php 
        $i++; 
        $soma_qtd = $soma_qtd + (int) $row['NU_NEG_CONTR'];
        $media_qtd = (int) $soma_qtd/21;
        endforeach;
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th><?php echo (int) $media_qtd; ?></th>
            <th></th>
            <th></th>
        </tr>
	</tfoot>    
</table>