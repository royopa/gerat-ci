      <!-- Main jumbotron for a primary marketing message or call to action --> 
      <!--
      <div class="jumbotron">
        <h2>Bem-vindo!</h2>
        <p>Essa página foi criada com a intenção de auxiliar na execução de rotinas diárias.</p>
        <p><a class="btn btn-primary btn-lg">Leia mais &raquo;</a></p>
      </div>
      -->
      
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Panel heading</div>
      
        <!-- Table -->
        <table cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="produtos" aria-describedby="example_info">
          <thead>
		        <tr>
              <th class="center" style="width: 10px;">#</th>
              <th class="center" style="width: 170px;">Bla</th>
              <th class="center" style="width: 170px;">BlaBla</th>
              <th class="center" style="width: 170px;">BlaBlaBla</th>
              <th class="center" style="width: 116px;">BlaBlaBla</th>
            </tr>
          </thead>
	        <tbody>
            <tr class="">
              <td class="">Gecko</td>
              <td class="">Firefox 1.0</td>
              <td class="">Win 98+ / OSX.2+</td>
              <td class="center">1.7</td>
			        <td class="center">A</td>
		        </tr>
            <tr class="">
              <td class="">Gecko</td>
			        <td class="">Firefox 1.5</td>
			        <td class="">Win 98+ / OSX.2+</td>
					    <td class="center">1.8</td>
					    <td class="center">A</td>
            </tr>  
			      <tr class="">
					    <td class="">Gecko</td>
					    <td class="">Mozilla 1.7</td>
					    <td class="">Win 98+ / OSX.1+</td>
					    <td class="center">1.7</td>
					    <td class="center">A</td>
		        </tr>
          </tbody>
        </table>
      </div>

      <div class="page-header">
        <h1>Consulta XML não processados</h1>
        <p>Utilize o formulário abaixo para consultar os XMLs não processados no XIMP.</p>
      </div>

      <form class="form-horizontal" role="form">
        <div class="form-group">
          <label for="dt_ref" class="col-lg-2 control-label">Data de referência</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="dt_ref" placeholder="Data de referência" maxlength="10">
          </div>
        </div>
        <div class="form-group">
          <label for="dt_atu" class="col-lg-2 control-label">Data de atualização</label>
          <div class="col-lg-10">
            <input type="text" class="form-control" id="dt_atu" placeholder="Data de atualização" maxlength="10">
          </div>
        </div>
        <div class="form-group">
          <div class="col-lg-offset-2 col-lg-10">
            <button type="submit" class="btn btn-default">Consultar</button>
          </div>
        </div>
      </form>


    
    
    
      <div class="page-header">
        <h1>Buttons</h1>
      </div>
      <p>
        <button type="button" class="btn btn-lg btn-default">Default</button>
        <button type="button" class="btn btn-lg btn-primary">Primary</button>
        <button type="button" class="btn btn-lg btn-success">Success</button>
        <button type="button" class="btn btn-lg btn-info">Info</button>
        <button type="button" class="btn btn-lg btn-warning">Warning</button>
        <button type="button" class="btn btn-lg btn-danger">Danger</button>
        <button type="button" class="btn btn-lg btn-link">Link</button>
      </p>
      <p>
        <button type="button" class="btn btn-default">Default</button>
        <button type="button" class="btn btn-primary">Primary</button>
        <button type="button" class="btn btn-success">Success</button>
        <button type="button" class="btn btn-info">Info</button>
        <button type="button" class="btn btn-warning">Warning</button>
        <button type="button" class="btn btn-danger">Danger</button>
        <button type="button" class="btn btn-link">Link</button>
      </p>
      <p>
        <button type="button" class="btn btn-sm btn-default">Default</button>
        <button type="button" class="btn btn-sm btn-primary">Primary</button>
        <button type="button" class="btn btn-sm btn-success">Success</button>
        <button type="button" class="btn btn-sm btn-info">Info</button>
        <button type="button" class="btn btn-sm btn-warning">Warning</button>
        <button type="button" class="btn btn-sm btn-danger">Danger</button>
        <button type="button" class="btn btn-sm btn-link">Link</button>
      </p>
      <p>
        <button type="button" class="btn btn-xs btn-default">Default</button>
        <button type="button" class="btn btn-xs btn-primary">Primary</button>
        <button type="button" class="btn btn-xs btn-success">Success</button>
        <button type="button" class="btn btn-xs btn-info">Info</button>
        <button type="button" class="btn btn-xs btn-warning">Warning</button>
        <button type="button" class="btn btn-xs btn-danger">Danger</button>
        <button type="button" class="btn btn-xs btn-link">Link</button>
      </p>



      <div class="page-header">
        <h1>Alerts</h1>
      </div>
      <div class="alert alert-success">
        <strong>Well done!</strong> You successfully read this important alert message.
      </div>
      <div class="alert alert-info">
        <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
      </div>
      <div class="alert alert-warning">
        <strong>Warning!</strong> Best check yo self, you're not looking too good.
      </div>
      <div class="alert alert-danger">
        <strong>Oh snap!</strong> Change a few things up and try submitting again.
      </div>


      <div class="page-header">
        <h1>List groups</h1>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <ul class="list-group">
            <li class="list-group-item">Cras justo odio</li>
            <li class="list-group-item">Dapibus ac facilisis in</li>
            <li class="list-group-item">Morbi leo risus</li>
            <li class="list-group-item">Porta ac consectetur ac</li>
            <li class="list-group-item">Vestibulum at eros</li>
          </ul>
        </div><!-- /.col-sm-4 -->
        <div class="col-sm-4">
          <div class="list-group">
            <a href="#" class="list-group-item active">
              Cras justo odio
            </a>
            <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item">Morbi leo risus</a>
            <a href="#" class="list-group-item">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item">Vestibulum at eros</a>
          </div>
        </div><!-- /.col-sm-4 -->
        <div class="col-sm-4">
          <div class="list-group">
            <a href="#" class="list-group-item active">
              <h4 class="list-group-item-heading">List group item heading</h4>
              <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            </a>
            <a href="#" class="list-group-item">
              <h4 class="list-group-item-heading">List group item heading</h4>
              <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            </a>
            <a href="#" class="list-group-item">
              <h4 class="list-group-item-heading">List group item heading</h4>
              <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            </a>
          </div>
        </div><!-- /.col-sm-4 -->
      </div>



      <div class="page-header">
        <h1>Panels</h1>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Panel title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Panel title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
        </div><!-- /.col-sm-4 -->
        <div class="col-sm-4">
          <div class="panel panel-success">
            <div class="panel-heading">
              <h3 class="panel-title">Panel title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Panel title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
        </div><!-- /.col-sm-4 -->
        <div class="col-sm-4">
          <div class="panel panel-warning">
            <div class="panel-heading">
              <h3 class="panel-title">Panel title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
          <div class="panel panel-danger">
            <div class="panel-heading">
              <h3 class="panel-title">Panel title</h3>
            </div>
            <div class="panel-body">
              Panel content
            </div>
          </div>
        </div><!-- /.col-sm-4 -->
      </div>



      <div class="page-header">
        <h1>Wells</h1>
      </div>
      <div class="well">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
      </div>



<div class="page-header">
    <h1>Nestable</h1>
      </div>

        <div class="dd" id="nestable">
            <ol class="dd-list">
                <li class="dd-item" data-id="1">
                    <div class="dd-handle">Item 1</div>
                </li>
                <li class="dd-item" data-id="2">
                    <div class="dd-handle">Item 2</div>
                    <ol class="dd-list">
                        <li class="dd-item"><div class="dd-handle">Item 3</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 4</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 9</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 10</div></li>
                    </ol>
                </li>
                

                <li class="dd-item" data-id="2">
                    <div class="dd-handle">Item 2</div>
                    <ol class="dd-list">
                        <li class="dd-item"><div class="dd-handle">Item 3</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 4</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 9</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 10</div></li>
                    </ol>
                </li>
                
                <li class="dd-item" data-id="2">
                    <div class="dd-handle">Item 2</div>
                    <ol class="dd-list">
                        <li class="dd-item"><div class="dd-handle">Item 3</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 4</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 9</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 10</div></li>
                    </ol>
                </li>
                
                <li class="dd-item" data-id="2">
                    <div class="dd-handle">Item 2</div>
                    <ol class="dd-list">
                        <li class="dd-item"><div class="dd-handle">Item 3</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 4</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 9</div></li>
                        <li class="dd-item"><div class="dd-handle">Item 10</div></li>
                    </ol>
                </li>                                
                
                <li class="dd-item" data-id="11">
                    <div class="dd-handle">Item 11</div>
                </li>
                <li class="dd-item" data-id="12">
                    <div class="dd-handle">Item 12</div>
                </li>
            </ol>
        </div>
        

    