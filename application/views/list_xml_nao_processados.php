<h1><?php echo $title; ?></h1>
<table id="table_export" >
  <thead>
    <tr>
      <th>#</th>
      <th>CNPJ</th>
      <th>Nome</th>
      <th>Data atualização</th>
      <th>Data referência</th>
    </tr>
	</thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($produtos as $row) : ?>
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $row['CO_PRD']; ?></td>
		  <td><?php echo $row['NO_PRD']; ?></td>
		  <td><?php echo formata_data_brasil($row['DT_ATU']); ?></td>
      <td><?php echo formata_data_brasil($row['DT_REF']); ?></td>
    </tr>
    <?php $i++; ?>
    <?php endforeach; ?>
  </tbody>
</table>