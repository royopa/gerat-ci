<div class="page-header">
  <h1>Consulta Liquidez Fundos/Carteiras</h1>
  <p>Utilize o formulário abaixo para consultar a liquidez dos fundos/carteiras na data solicitada.</p>      
</div>
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'form_consulta');
echo form_open('RiscoLiquidez/listaProdutosLiquidezDiaria', $attributes);
$data = new \DateTime('today');
?>

<div class="form-group">
  <div class="col-lg-10">
  <label for="dt_atu" class="control-label">Data de atualização</label>
    <input type="text" class="form-control" value="<?php echo $data->format('d/m/Y'); ?>" name="dt_atu" id="dt_atu" placeholder="Data de atualização" maxlength="10">
    <label for="dt_atu" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
    <button type="submit" class="btn btn-default">Consultar</button>
  </div>
</div>
<?php echo form_close(); ?>