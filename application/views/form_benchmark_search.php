<div class="page-header">
  <h1>Consulta benchmark</h1>
  <p>Utilize o formulário abaixo para consultar o benchmark.</p>      
</div>
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'form_consulta');
echo form_open('BenchmarkController/listaBenchmarks', $attributes);
?>

<div class="form-group">
  <div class="col-lg-10">
    <label for="co_bench" class="control-label">Benchmark</label>
    <?php echo form_dropdown('co_bench', $benchmarks, null, 'class="form-control"'); ?>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-10">
  <label for="dt_ini" class="control-label">Data Inicial</label>
    <input type="text" class="form-control" name="dt_ini" id="dt_ini" 
    placeholder="Data inicial" maxlength="10">
    <label for="dt_ini" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-10">
  <label for="dt_fim" class="control-label">Data Final</label>
    <input type="text" class="form-control" name="dt_fim" id="dt_fim" 
    placeholder="Data final" maxlength="10">
    <label for="dt_fim" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
    <button type="submit" class="btn btn-info">Consultar</button>
  </div>
</div>
<?php echo form_close(); ?>