<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" >
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>GERAT</title>

    <!-- Favicon caixa -->
    <link rel="icon" href="<?php echo base_url(); ?>/assets/img/favicon.ico" type="image/x-icon">

    <!-- jQuery UI CSS -->
    <link href="<?php echo base_url(); ?>assets/css/jquery-ui/css/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
  
    <!-- DataTables CSS -->
    <link href="<?php echo base_url(); ?>assets/css/datatables/media/css/jquery.dataTables.css" rel="stylesheet"> 
    <link href="<?php echo base_url(); ?>assets/css/datatables/media/css/jquery.dataTables_themeroller.css" rel="stylesheet">

    <!-- DataTables Tabletools -->
    <link href="<?php echo base_url(); ?>assets/js/DataTables-1.9.4/extras/TableTools/media/css/TableTools.css" rel="stylesheet"> 
    
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap/dist/css/bootstrap.min.cerulean.css" rel="stylesheet">
  
    <!-- CSS da página -->
    <link href="<?php echo base_url(); ?>assets/css/gerat.css" rel="stylesheet">  

    <!-- jQuery  -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>

    <!-- Highchart jQuery  -->
    <script src="<?php echo base_url(); ?>assets/js/highcharts/js/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/highcharts/js/highcharts-more.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/css3-mediaqueries.js"></script>
	
</head>