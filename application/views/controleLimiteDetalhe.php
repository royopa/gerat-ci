<html>

<head>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/stylesheet.css">
</head>
<body>
<img width="769" src="<?php echo base_url();?>assets/img/image004.png"><br /><br />
<span height="37" class="xl106">RELAT&Oacute;RIO CONTROLE DE RISCO DE MERCADO</span><br />
<img width="769" src="<?php echo base_url();?>assets/img/image005.png"><br /><br />
<span height="67" class="xl107"><?php echo $produto->NO_PRD;?></span><br />
<img width="769" src="<?php echo base_url();?>assets/img/image006.png"><br /><br />
<span height="20" class="xl90">Posi&ccedil;&atilde;o para o dia</span><br /><br />
<span class="xl79">Par&acirc;metros</span><br />
<table class="xl99">
<tr>
<td>M&eacute;todo VaR</td><td><?php echo $produto->NO_MAPS_METODO_VAR;?></td>
</tr>
<tr>
<td>Horizonte de Tempo</td><td><?php echo $produto->NU_MAPS_HORIZ;?></td>
</tr>
<tr>
<td>N&iacute;vel de Confian&ccedil;a</td><td><?php echo $produto->VR_MAPS_NIV_CONF;?></td>
</tr>
<tr>
<td>Limite de VaR</td><td><?php echo number_format($produto->FT_LIM * 100, 2, ',', '.');?></td>
</tr>
</table>
<img width="769" src="<?php echo base_url();?>assets/img/image006.png">
<table cellpadding="0" cellspacing="0">
<tr height=48 style='mso-height-source:userset;height:36.0pt'>
  <td class="xl109" height=48 width=263 style='height:36.0pt;width:197pt'>BOOKS</td>
  <td class="xl109" width="160" style='border-left:none;width:120pt'>MtM L&iacute;quido ($ mil)</td>
  <td class="xl109" width="106" style='border-left:none;width:80pt'>PL ($ mil)</td>
  <td class="xl109" width="103" style='border-left:none;width:77pt'>VaR ($ mil)</td>
  <td class="xl109" width="68" style='border-left:none;width:51pt'>VaR/PL</td>
  <td class="xl109" width="70" style='border-left:none;width:53pt'>% Limite</td>
</tr>
<tr> 
  <td height=21 class=xl110 style='height:15.75pt;border-top:none'><?php echo $produto->NO_PRD;?></td>
  <td class=xl111 style='border-top:none;border-left:none'><?php echo number_format($produto->VR_MAPS_MTM, 2, ',', '.');?></td>
  <td class=xl111 style='border-top:none;border-left:none'><?php echo number_format($produto->VR_MAPS_PL_INFO, 2, ',', '.');?></td>
  <td class=xl112 style='border-top:none;border-left:none'><?php echo number_format($produto->VR_MAPS_VAR_POL, 2, ',', '.');?></td>
  <td class=xl113 style='border-top:none;border-left:none'><?php echo number_format($produto->PC_ALOC_VAR_POL * 100, 2, ',', '.');?></td>
  <td class=xl114 style='border-top:none;border-left:none'><?php echo number_format($produto->PC_ALOC_S_LIM * 100, 2, ',', '.');?></td>
</tr>
</table>
</body>

</html>
