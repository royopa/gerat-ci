  <!-- Fixed navbar -->
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <img id="logo" height="40" src="<?php echo base_url(); ?>assets/img/logo_caixa.png">
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><?php echo anchor('gerat/index', 'Home'); ?></li>
          <li><?php echo anchor('Fundos/listaProdutosAtivos', 'Fundos/Carteiras'); ?></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Risco de Liquidez <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><?php echo anchor('RiscoLiquidez/pesquisaLiquidezDiaria', 'Consulta valor de liquidez diário'); ?></li>
              <li><?php echo anchor('RiscoLiquidez/pesquisaNegociacaoAtivo', 'Consulta as últimas negociações de um ativo'); ?></li>
              <!-- <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="#">Separated link</a></li>
              <li><a href="#">One more separated link</a></li>
              -->
            </ul>
          </li>          
          <!-- <li><a href="#about">Rotina Diária</a></li> -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Rotina Diária <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><?php echo anchor('RotinaDiaria/consultaXmlNaoProcessados', 'Consulta XML não processados'); ?></li>
              <li><?php echo anchor('RotinaDiaria/consultaDiferencasXml', 'Consulta diferenças XML'); ?></li>
              <li><?php echo anchor('benchmarkController/index', 'Consulta benchmark'); ?></li>
              <!--
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="#">Separated link</a></li>
              <li><a href="#">One more separated link</a></li>
              -->
            </ul>
          </li>
          <!-- Controle de limties -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Risco de Mercado <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><?php echo anchor('Mercado/controleLimite', 'Controle limite'); ?></li>
              <!--
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="#">Separated link</a></li>
              <li><a href="#">One more separated link</a></li>
              -->
            </ul>
          </li>
          <!-- Relatórios -->
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatórios<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><?php echo anchor(
              'http://10.4.45.208/gerat-sf/web/app.php/relatorio_atribuicao_performance'
              , 'Atribuição de performance'); ?></li>
              <!--
              <li class="divider"></li>
              <li class="dropdown-header">Nav header</li>
              <li><a href="#">Separated link</a></li>
              <li><a href="#">One more separated link</a></li>
              -->
            </ul>
          </li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>