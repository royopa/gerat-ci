<h1>Liquidez diária - Conferência</h1>

<div class="panel panel-default">
<!-- Default panel contents -->
<div class="panel-heading">
  <?php echo $produto->CO_PRD . ' - ' . 
    $produto->NO_PRD . ' - XML referência ' . $dt_ref; ?>
</div>
<?php

$nu_cotiz = $produto->NU_COTIZ;
$cotiz_calc = $nu_cotiz + 1;

?>
<table id="table_export" >
    <thead>
      <tr>
        <th colspan="11" class="text-center">
          <?php echo $produto->CO_PRD . ' - ' . 
          $produto->NO_PRD . ' - XML referência ' . $dt_ref; ?>
        </th>
      </tr>
      <tr>
        <th class="center">Ativo</th>
        <th class="center">Dt. Vcto</th>
        <th class="center">Cód.</th>
        <th class="center" title ="Líquido?">L?</th>
        <th class="center" title="Tipo">T</th>
        <th class="center" title ="Qtd fundo">Q</th>
        <th class="center" title ="Média qtd mercado">M</th>
        <th class="center">Valor mercado</th>
        <th class="center">Valor PU</th>
        <th class="center" title="Valor em garantia">Garantia</th>
        <th class="center">Valor líquido</th>
      </tr>
   </thead>  
    <tbody>
        <?php 
            $i = 1; 
            $soma_vr_liquidez = 0;
            $valor_liquido_cotas_fundo = 0;
        ?>
        <?php foreach ($posicao as $row) : ?>
        <?php 
              //var_dump($row);
              //echo '</br></br>';

              $row['VR_MERC'] = $row['VR_MERC'] - $row['VR_MERC_P'];

              //remove o valor em garantia
              $valorMercadoCalculado = $row['VR_MERC'] - $row['VR_EM_GAR'];

              $produto_model = new Produto_Model(); 

              $co_tp_ati = $row['CO_TP_ATI'];

              $mtp_ati = $produto_model->getMtpAtivo($row['CO_ATI']);

              $negociacao = array();

              switch ($mtp_ati) {
                  case 11:
                      //echo "Título Público";
                      $negociacao = $produto_model->detalheNegocMercadoTitPub($dt_atu, $row['CO_ATI']);
                      break;
              }

              $dataVencimento = new DateTime($row['DT_VCTO']);

              //var_dump($row['CO_ATI']);
              //var_dump($co_tp_ati);

              $no_ati = '';
              $no_ati = $produto_model->getNoCodAtivo($row['CO_ATI']);

              //var_dump($row);
              //echo '</br></br></br>';

              //echo $no_ati . ' - ' . $row['CO_ATI'] . ' - ' . $co_tp_ati;
              //echo '</br></br>';

              switch ($co_tp_ati) {
                  case 6:
                      //ação
                      $negociacao = $produto_model->detalheNegocMercadoAcao($dt_atu, $no_ati);
                      break;
                  case 1:
                      //echo "Debênture";
                      $negociacao = $produto_model->detalheNegocMercadoDebenture($dt_atu, $no_ati);
                      break;
                  case 10:
                      //echo "Futuro";
                      $negociacao = $produto_model->detalheNegocMercadoFuturo($dt_atu, $no_ati);
                      break;                      
              }
          
            $soma_qtd = 0; 
            $vr_ajuste = 0;
            
            foreach ($negociacao as $negocio) {
                $soma_qtd = $soma_qtd + $negocio['NU_NEG_CONTR'];
            }

            //calcula a média de qtd negociadas
            //se for ação, a média calculada é um float
            if ($co_tp_ati == 6) {
                $media_qtd_mercado = (float) ($soma_qtd/21);
            } else {
                $media_qtd_mercado = (int) ($soma_qtd/21);
            }

            //subtrai o valor que estiver em garantia do valor líquido
            //$row['VR_MERC'] = (float)$row['VR_MERC'] - (float)$row['VR_EM_GAR'];

            //pega a qtd de ativos do fundo
            $nu_qtd = (int) $row['NU_QTD'];
            
            //calcula o PU mercado
            if ($nu_qtd > 0) {
              $pu_mercado = (float)$row['VR_MERC'] / (int)$row['NU_QTD'];  
            }

            //se o ativo for futuro, o PU de mercado é o valor de ajuste
            if ($co_tp_ati == 10) {
                $pu_mercado = $produto_model->getValorAjusteMercadoFuturo($fundoDiario->getDataReferencia(), $no_ati);
                if ($row['CO_TP_OP'] == 'V') {
                    //troca o sinal
                    $pu_mercado = $pu_mercado * -1;
                }
            }

            //$pu_mercado = (float)$row['VR_MERC'] / (int)$row['NU_QTD'];

            //subtrai o valor que estiver em garantia do valor líquido
            //$valor_liquidez = $valor_liquidez - (float)$row['VR_EM_GAR'];

            //calcula a liquidez
            //pega o menor dos dois valores nu_qtd (fundo) e $media_qtd_mercado (media qtd do mercado nos ultimos 
            //21 dias), o que for menor, multiplica pelo PU de mercado corrente ()
            if ((int) $nu_qtd > (int) $media_qtd_mercado) {
                $quantidadeCalculada = $media_qtd_mercado;
            } else {
                $quantidadeCalculada = $nu_qtd;
            }

            //Calcula o valor líquido
            $valor_liquidez = $pu_mercado * $quantidadeCalculada;

            /*
            echo "Quantidade do fundo ($nu_qtd) </br>
                 Média de mercado ($media_qtd_mercado) </br>
                 $quantidadeCalculada X $pu_mercado = $valor_liquidez </br>
                ";
            */

            //se for FIC (exceto FIDC), 
            //o valor líquido é o valor de mercado calculado 
            //(Valor mercado - Valor em garantia)
            if ($row["CO_TP_ATI"] == 4 && $row['CO_STP_ATI'] != 1) {
                $valor_liquido_cotas_fundo = $valor_liquido_cotas_fundo + $valorMercadoCalculado;
                $valor_liquidez = $valorMercadoCalculado;
            } else {
                //Faz a multiplicação pelo prazo de cotização do fundo
                //somar sempre 1 ao prazo de cotização
                $valor_liquidez = $valor_liquidez * ($fundoDiario->getFundo()->getPrazoCotizacao() + 1);
            }

            //var_dump($row);
            //echo '</br></br></br>';

            //se for mercado futuro, o PU é o valor de ajuste
            //'zera' a liquidez
            if ($co_tp_ati == 10 && (float)$row['VR_MERC'] < 0 ) {
              //$valor_liquidez = 0;
            }

            //se for CDB, o valor líquido é o valor de mercado
            if ($co_tp_ati ==  7 && $row['IC_LIQUIDO'] == 1) {
                $valor_liquidez = $valorMercadoCalculado;
            }

            //se for FIDC, o valor líquido é 0
            if ($co_tp_ati ==  4 && $row['CO_STP_ATI'] == 1) {
                $row['IC_LIQUIDO'] = 0;
                $valor_liquidez = 0;
            }

            //se for operação compromissada e a data de vencimento
            //for igual a data de atualização, seta como líquido
            //e o valor líquido é o valor de mercado
            //se é operação compromissada
            if ($row['CO_ATI'] == 'OP_COMPROM00') {
                //se a data de vencimento é igual a data de atualização
                if ($dataVencimento == $fundoDiario->getDataAtualizacao()) {
                    //seta como líquido
                    $row['IC_LIQUIDO'] = 1;
                    //seta o valor de mercado como o valor líquido
                    $valor_liquidez = $valorMercadoCalculado;
                }
            }

            //se o ativo não é líquido, zera a liquidez
            if ($row['IC_LIQUIDO'] == 0) {
              $valor_liquidez = 0;
            }

            //var_dump($row);
            //var_dump($valor_liquidez);
            //var_dump($valorMercadoCalculado);

            //sempre usar o menor valor
            //se for maior que o valor de mercado, o valor líquido é o valor de mercado
            if (abs($valor_liquidez) > abs($valorMercadoCalculado)) {
                $valor_liquidez = $valorMercadoCalculado;
            }

            //var_dump($valor_liquidez);
        ?>
        <tr>
			     <!--
           <td class="center"><?php echo $i; ?></td>
           -->
           <td>
            <a href="http://www.bmfbovespa.com.br/consulta-isin/DetalheCodigosIsinInformacoes.aspx?idioma=pt-br&isin=<?php echo $row['CO_ATI']; ?>"
            target="_blank">
              <?php echo $row['CO_ATI']; ?>
           </a>
           </td>
           <td><?php echo $dataVencimento->format('d/m/Y'); ?></td>
           <td><?php echo $no_ati; ?></td>
           <td class="center"><?php echo substr(sim_nao($row['IC_LIQUIDO']), 0, 1); ?></td>
           <td><?php echo $row['CO_TP_OP']; ?></td>
			     <td><?php echo $nu_qtd; ?></td>
            <td>
                <?php echo anchor(
                            'RiscoLiquidez/listaNegociacaoAtivo?co_ati=' . $row['CO_ATI'] . '&dt_ref=' . formata_data($dt_ref) . '&dt_atu=' . $dt_atu, 
                            (int) $media_qtd_mercado, 
                            'title="Detalhe das negociações do ativo"'); ?>
            </td>
            <td><?php echo number_format($row['VR_MERC'],2,",","."); ?></td>
            <td><?php echo number_format($pu_mercado,2,",","."); ?></td>
            <td><?php echo number_format($row['VR_EM_GAR'],2,",","."); ?></td>
            <td><?php echo number_format($valor_liquidez,2,",","."); ?></td>
		    </tr>
        <?php 
            //se for cota de fundo de investimento, o valor líquido é somado somente no final
            //pois não multiplicamos o valor pelo prazo de cotização do fundo
            if ($row["CO_TP_ATI"] != 4) {
                $soma_vr_liquidez = $soma_vr_liquidez + $valor_liquidez;
            }
            $i++;
        ?>
        <?php endforeach; ?>

        <!-- Linha da coluna com o valor de CAIXA do fundo -->
        <tr>
			     <!--
           <td class="center"><?php echo $i+1; ?></td>
           -->
           <td><?php echo 'CAIXA0000000'; ?>&nbsp;</td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td class="center">S</td>
           <td>&nbsp;</td>
			     <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td><?php echo number_format($valorCaixa,2,",","."); ?></td>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
           <td><?php echo number_format($valorCaixa,2,",","."); ?></td>
		    </tr>
        <!-- Fim do valor de CAIXA do fundo -->
    </tbody>

  <tfoot>
    <tr>
      <td colspan="10" class="text-right">Valor calculado</td>
      <?php
        //Totalizador - É a soma de valor de liquidez + valor de caixa + valor liquido de cotas de fundos
        $valorLiquido = $soma_vr_liquidez + $valorCaixa + $valor_liquido_cotas_fundo;
        //se o valor líquido é maior do que o PL do fundo, o valor
        //líquido fica sendo o PL do fundo
        if ($valorLiquido > $fundoDiario->getPatrimonioLiquido()) {
            $valorLiquido = $fundoDiario->getPatrimonioLiquido();
        }
        //se o fundo é um FIC, o valor líquido é PL do fundo
        if ($fundoDiario->getFundo()->getTipo() == 2) {
            $valorLiquido = $fundoDiario->getPatrimonioLiquido();
        }
      ?>
      <td><b><?php echo number_format(round($valorLiquido), 2, ",", "."); ?></b></td>
    </tr>

    <tr>
      <td colspan="10" title="Valor informado CVM Rotina diária" class="text-right">Valor informado à CVM</td>
      <td><b><?php echo number_format($fundoDiario->getValorLiquidez(), 2, ",", "."); ?></b></td>
    </tr>

    <tr>
      <td colspan="10" class="text-right">Diferença</td>
      <?php $diferenca = $fundoDiario->getValorLiquidez() - round($valorLiquido); 
        $classeCor = 'text-success';
        if ($diferenca != 0) {
            $classeCor = 'text-danger';
        }
      ?>
      <td class="<?php echo $classeCor; ?>"><b><?php echo number_format($diferenca, 2, ",", "."); ?></b></td>
    </tr>
 
  </tfoot>    
</table>
</div>

<div class="row">
  <p>&nbsp;</p>
  <p class="text-left">Patrimônio Líquido: <b>R$ <?php echo number_format($fundoDiario->getPatrimonioLiquido(), 2, ",", "."); ?></b></p>
  <p class="text-left">Prazo de Cotização: <b><?php echo $fundoDiario->getFundo()->getPrazoCotizacao(); ?></b> dias</p>
</div>
<?php
if ($fundoDiario->getValorLiquidezConferencia() != $valorLiquido) {
    $fundoDiario->removeValorLiquidezCalculado();
    $fundoDiario->setValorLiquidezCalculado($valorLiquido);
}