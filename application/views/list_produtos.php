<h1><?php echo $title; ?></h1>
<table id="table_export" >
  <thead>
    <tr>
      <th>#</th>
      <th>CNPJ</th>
      <th>Nome</th>
      <th>Gestor</th>
      <th>#</th>
    </tr>
	</thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($produtos as $row) : ?>
    <tr>
		  <td><?php echo $i; ?></td>
      <td><?php echo $row->CO_PRD; ?></td>
			<td><?php echo $row->NO_PRD; ?></td>
			<td><?php echo $row->CO_GRU_GESTAO; ?></td>
      <td>
        <?php echo anchor(
                    'relatorios/relatorioRisco?co_prd=' . 
                    $row->CO_PRD . 
                    '&id_relatorio=1'
                    , 
                    'Relatório de Risco', 
                      array(
                        'title' => 'Consulta o relatório de risco do fundo')
                        ); ?>
      </td>
		</tr>
    <?php $i++; ?>
    <?php endforeach; ?>
  </tbody>
</table>