<div class="page-header">
  <h1>Consulta Diferenças XML</h1>
  <p>Utilize o formulário abaixo para consultar as diferenças dos XMLs processados no XIMP.</p>
</div>
<?php 
$attributes = array(
  'class'  => 'form-horizontal', 
  'id'     => 'form_consulta',
  'method' => 'get'
);
echo form_open('RotinaDiaria/diferencasXmlList', $attributes); 
?>
<div class="form-group">
  <div class="col-lg-10">
  <label for="dt_atu" class="control-label">Data de atualização</label>
    <input type="text" class="form-control" name="dt_atu" id="dt_atu" value="<?php echo data_atual(); ?>" placeholder="Data de atualização" maxlength="10">
    <label for="dt_atu" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
    <button type="submit" class="btn btn-default">Consultar</button>
  </div>
</div>
<?php echo form_close(); ?>