      <p>&nbsp;</p>
      <!--
      <p class="footer text-right">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
      -->
    </div> <!-- /container -->
    
    <!-- Script para permitir a exportação da tabela para o Excel -->
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('#table_export').dataTable( {
                "sDom": 'T<"clear">lfrtip',
                "oTableTools": {
                    "sSwfPath": "<?php echo base_url(); ?>assets/js/DataTables-1.9.4/media/swf/copy_csv_xls_pdf.swf"
                },
                "oLanguage": {
                    "sProcessing": "Aguarde enquanto os dados são carregados ...",
                    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                    "sInfoEmtpy": "Exibindo 0 a 0 de 0 registros",
                    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                    "sInfoFiltered": "",
                    "sSearch": "Procurar",
                    "oPaginate": {
                        "sFirst":    "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext":     "Próximo",
                        "sLast":     "Último"
                    },
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "bJQueryUI": true,            
            });
        });
    </script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url(); ?>assets/css/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>  
    <!-- jQuery Validation --> 
    <script src="<?php echo base_url(); ?>assets/js/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-validation/localization/messages_PT_BR.js"></script>
    <!-- Funções da página -->
    <script src="<?php echo base_url(); ?>assets/js/gerat.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/css/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/css/bootstrap/assets/js/holder.js"></script>
    <!-- Nestable jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.nestable.js"></script>
    <!-- jQuery DataTables -->
    <script src="<?php echo base_url(); ?>assets/js/DataTables-1.9.4/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/DataTables-1.9.4/extras/TableTools/media/js/TableTools.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.data-eu.js"></script>    
  
  </body>
</html>