<h1><?php echo $title; ?></h1>

<table id="table_export" >
  <thead>
    <tr>
      <th>#</th>
      <th>Data</th>
      <th>CNPJ</th>
      <th>Nome</th>
      <th>Ativo</th>
      <th>Passivo</th>
      <th>Total</th>
      <th>Valor XML</th>
      <th>Diferença</th>
    </tr>
	</thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($produtos as $row) : ?>
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo formata_data_brasil($row['DATA']); ?></td>
      <td><?php echo $row['CNPJ']; ?></td>
			<td><?php echo $row['FUNDO']; ?></td>
			<td><?php echo number_format($row['ATIVO'],2,",","."); ?></td>
      <td><?php echo number_format($row['PASSIVO'],2,",","."); ?></td>
      <td><?php echo number_format($row['TOTAL'],2,",","."); ?></td>
      <td><?php echo number_format($row['VALOR_XML'],2,",","."); ?></td>
      <td><?php echo number_format($row['DIFERENCA'],2,",","."); ?></td>
		</tr>
    <?php $i++; ?>
    <?php endforeach; ?>
  </tbody>
</table>