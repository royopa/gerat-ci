<div class="page-header">
  <h1>Consulta negociações de um ativo</h1>
  <p>Utilize o formulário abaixo para consultar a negociação de um ativo (Títulos Públicos) na data solicitada.</p>      
</div>
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'form_consulta');
echo form_open('RiscoLiquidez/listaNegociacaoAtivo', $attributes);
?>
<div class="form-group">
  <div class="col-lg-10">
  <label for="co_ati" class="control-label">ISIN do ativo</label>
    <input type="text" class="form-control" name="co_ati" id="co_ati" placeholder="ISIN" maxlength="12">
    <label for="co_ati" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-10">
  <label for="dt_ref" class="control-label">Data de referência</label>
    <input type="text" class="form-control" name="dt_ref" id="dt_ref" placeholder="Data de referência" maxlength="10">
    <label for="dt_ref" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-10">
  <label for="dt_atu" class="control-label">Data de atualização</label>
    <input type="text" class="form-control" name="dt_atu" id="dt_atu" placeholder="Data de atualização" maxlength="10">
    <label for="dt_atu" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-10">
  <label for="qtd_neg" class="control-label">Quantidade de negociações</label>
    <input type="text" class="form-control" name="qtd_neg" id="qtd_neg" maxlength="10" value="21">
    <label for="qtd_neg" class="error label label-danger"></label>
  </div>
</div>

<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
    <button type="submit" class="btn btn-default">Consultar</button>
  </div>
</div>
<?php echo form_close(); ?>