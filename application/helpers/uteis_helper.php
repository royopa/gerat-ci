<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
    public function converteData($data) {
                if(strpos($data, '-')){
                    $d = explode("-", $data);
                    return $d[2].'/'.$d[1].'/'.$d[0];
                } else if(strpos($data, '/')) {
                    $d = explode("/", $data);
                    return $d[2].'-'.$d[1].'-'.$d[0];
                }
    }
*/        
function formata_data($data) 
{
  if ((substr($data, 4, 1) == '-') && (substr($data, 7, 1) == '-')) 
  {
    return $data;
  }
  
  if ((substr($data, 4, 1) == '/') && (substr($data, 7, 1) == '/')) 
  {
    return $data;
  }  
  
  $dia = substr($data, 0, 2);
  $mes = substr($data, 3, 2);
  $ano = substr($data, 6, 4);
  $data_formatada = $ano . '-' . $mes . '-' . $dia;
        
  return $data_formatada;
}
    
function formata_data_brasil($data, $separador = '-') 
{
        //2013-05-14 00:00:00
        $dia = substr($data, 8, 2);
        $mes = substr($data, 5, 2);
        $ano = substr($data, 0, 4);
        $data_formatada = $dia . $separador . $mes . $separador . $ano;
        
        return $data_formatada;
}

/**
* Função para calcular o próximo dia útil de uma data
* Formato de entrada da $data: AAAA-MM-DD
*/
function proximoDiaUtil($data, $saida = 'd/m/Y') 
{
    // Converte $data em um UNIX TIMESTAMP
    $timestamp = strtotime($data);

    // Calcula qual o dia da semana de $data
    // O resultado será um valor numérico:
    // 1 -> Segunda ... 7 -> Domingo
    $dia = date('N', $timestamp);
    
    // Se for sábado (6) ou domingo (7), calcula a próxima segunda-feira
    if ($dia >= 6) 
    {
        $timestamp_final = $timestamp + ((8 - $dia) * 3600 * 24);
    } else {
        // Não é sábado nem domingo, mantém a data de entrada
        $timestamp_final = $timestamp;
    }
    
    return date($saida, $timestamp_final);
}

function add_date($givendate,$day=0,$mth=0,$yr=0) 
{
    $cd = strtotime($givendate);
    $newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
    date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
    date('d',$cd)+$day, date('Y',$cd)+$yr));
    return $newdate;
}

function sim_nao($ic = 0) 
{
    if ($ic == 1)
    {
        return 'Sim';
    }
    return 'Não';
}


//Todo programador já precisou ou precisará algum dia manipular datas no php  e estou disponibilizando aqui algumas funções para exibir, validar e converter datas.

//1- Função que retorna a data atual no padrão Português Brasileiro (dd/MM/YYYY):

    function data_atual()
    {

    // leitura das datas
    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');

    // definindo padrão pt (dd/MM/YYYY)
    $data = "$dia/$mes/$ano";

    return $data;
    }

    //data_atual();
    //2- Função que retorna a data atual no padrão Inglês (YYYY-MM-DD):
    function data_atual_en() {

    // leitura das datas
    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');

    // definindo padrão en (YYYY-MM-DD)
    $data_en = "$ano-$mes-$dia";

    return $data_en;
    }

    //data_atual_en();
    //3- FUNÇÃO QUE RETORNA A HORA ATUAL (Hora e Minuto)
    function hora_atual() {
     $hora = date("H:i");
     return $hora;
    }

  //Utilização:
  //hora_atual();

  //4- FUNÇÃO HORA ATUAL COMPLETA (Hora / Minuto / Segundo)
    function hora_atual_completa() {
     $hora = date("H:i:s");
     return $hora;
    }

//Utilização:

    //hora_atual_completa();

//5- FUNÇÃO QUE VALIDA A DATA

//Esta função faz todas a verificações necessárias como verificar se o mês está entre 1 e 12,  verificar se o dia está dentro dos dias permitidos para aquele mês (leva em consideração os anos bissextos) e verificar se o ano é válido.

//OBS.: As datas enviadas podem estar no formato inglês (en) ou brasileiro (pt).

    function valida_data($data, $tipo = "pt")
    {

    	if ($tipo == 'pt')
    	{
    		$d = explode("/", $data);
    		$dia = $d[0];
    		$mes = $d[1];
    		$ano = $d[2];
    	}
    	else if ($tipo == 'en')
    	{
    		$d = explode("-",$data);
    		$dia = $d[2];
    		$mes = $d[1];
    		$ano = $d[0];
    	}

    	//usando função checkdate para validar a data
    	if (checkdate($mes, $dia, $ano))
    	{
    		$data = $ano.'/'.$mes.'/'.$dia;

    		if (
    			//verificando se o ano tem 4 dígitos
    			(strlen($ano) != '4') ||
    			//verificando se o mês é menor que zero
    			($mes <= '0') ||
                            //verificando se o mês é maior que 12
                            ($mes > '12') ||
    			//verificando se o dia é menor que zero
    			($dia <= '0') ||
                            //verificando se o dia é maior que 31
                            ($dia > '31')
    		    )
    		{
    			return false;
    		}

    		if (strlen($data) == 10)
    			return true;
    	}
    	else
    	{
    		return false;
    	}
    }



    //Validar data no formato brasileiro (dd/mm/yyyy):
    //valida_data('27/07/2009', 'pt');

    //Validar data no formato inglês (yyyy-mm-dd):
    //valida_data('2009-07-27', 'en');


//6- CONVERTER DATA BRASILEIRA (dd/mm/YYY) PARA PADRAO DO BANCO DE DADOS

    function data_user_para_mysql($y){
        $data_inverter = explode("/",$y);
        $x = $data_inverter[2].'-'. $data_inverter[1].'-'. $data_inverter[0];
        return $x;
    }

    //data_user_para_mysql('11/02/1992')
    //Resultado: 1992-02-11
    
function formata_moeda($valor = 0) 
{
  return number_format($valor,2,",",".");
}

    /**
    * Formata um valor recebido para o padrão de moeda Brasil
    *
    *
    * @param float $valor A data de atualização da base de dados
    *
    * @return  string $moeda A string com a moeda formatada
    */
    function formataMoeda($valor = 0) 
    {
      return number_format($valor,2,",",".");
    }

function formatarCPF_CNPJ($campo, $formatado = true){
    //retira formato
   $codigoLimpo = preg_replace("[' '-./ t]",'',$campo);
    // pega o tamanho da string menos os digitos verificadores
    $tamanho = (strlen($codigoLimpo) -2);
    //verifica se o tamanho do código informado é válido
    if ($tamanho != 9 && $tamanho != 12){
        return false; 
    }
 
    if ($formatado){ 
        // seleciona a máscara para cpf ou cnpj
        $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##'; 
 
        $indice = -1;
        for ($i=0; $i < strlen($mascara); $i++) {
            if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
        }
        //retorna o campo formatado
        $retorno = $mascara;
 
    }else{
        //se não quer formatado, retorna o campo limpo
        $retorno = $codigoLimpo;
    }
 
    return $retorno;
}