<?php
if ( ! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Helper files
| 4. Custom config files
| 5. Language files
| 6. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packges
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/

$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in the system/libraries folder
| or in your application/libraries folder.
|
| Prototype:
|
|     $autoload['libraries'] = array('database', 'session', 'xmlrpc');
*/

$autoload['libraries'] = array('twiggy');

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|     $autoload['helper'] = array('url', 'file');
*/

$autoload['helper'] 
      = array(
            'form', 
            'url', 
            'file', 
            'uteis'
            );

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|     $autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/

$autoload['config'] = array('twiggy');

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|     $autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/

$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|     $autoload['model'] = array('model1', 'model2');
|
*/
$autoload['model']
    = array(
            'Produto_Model',
            'classes/AtivoCreditoGrupo',
            'classes/Grupo',
            'classes/CcbGrupo',
            'classes/CciGrupo',
            'classes/CdbGrupo',
            'classes/CdbSubordinadoGrupo',
            'classes/CriGrupo',
            'classes/DebentureGrupo',
            'classes/DpgeGrupo',
            'classes/FidcGrupo',
            'classes/LetraFinanceiraGrupo',
            'classes/LetraFinanceiraSubordinadaGrupo',
            'classes/LetraHipotecariaGrupo',
            'classes/ComposicaoAtivosCredito',
            'classes/Emissor',
            'classes/Fundo',
            'classes/LhGrupo',
            'classes/LimiteAlocacaoGrupo',
            'classes/LimiteFidcGrupo',
            'classes/ListaAtivosCredito',
            'classes/OperacaoCompromissadaGrupo',
            'classes/ListaComposicao',
            'classes/AtivoCredito',
            'classes/Composicao',
            'classes/LimiteAlocacao',
            'classes/LimiteIfDpgeGrupo',
            'classes/LimiteIfGrupo',            
            'classes/LimiteNaoIfGrupo',
            'classes/ListaComposicaoCarteira',
            'classes/ListaComposicaoCarteiraCredito',
            'classes/SerieDados',
            'classes/SerieDadosLimiteAlocacao',
            'classes/CaptacaoRentabilidade',
            'classes/Dbal',
            'classes/FundoDiario',
            'HighchartsPHP-master/Highchart',
            'HighchartsPHP-master/HighchartOption',
            'HighchartsPHP-master/HighchartOptionRenderer',
            'classes/Grafico',
            'classes/Liquidez',
            'classes/LiquidezGrupo',
            'classes/ListaCaptacaoRentabilidade',
            'classes/ListaDistribuicaoCotistas',
            'classes/ListaLimiteAlocacaoGrupo',
            'classes/TermometroLiquidez',
            'classes/ItemRiscoMercadoGrupo',
            'classes/ItemRiscoMercado',
            'classes/Relatorio',
            'classes/ViewRelatorio',
            'classes/RiscoMercado',
            'classes/CronogramaVencimentoGrupo',
            'classes/CronogramaVencimento',
            'classes/Benchmark',
            'classes/DesempenhoMensal',
            'classes/DesempenhoMensalGrupo',
            'classes/DesempenhoDiario',
            'classes/DesempenhoDiarioGrupo',
            'classes/DesempenhoTipoAtivo',
            'classes/DesempenhoTipoAtivoGrupo',
            );
/* End of file autoload.php */
/* Location: ./application/config/autoload.php */
