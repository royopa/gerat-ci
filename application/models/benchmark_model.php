<?php 
/**
* Benchmark_Model File Doc Comment
*
* @category CI_Model
* @package  Models
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Benchmark_Model_CI_Model
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Benchmark_Model extends CI_Model
{
    /**
    * Consulta no banco de dados os fundos/carteiras
    *
    * Tabela Produto
    *
    * @param int $ic_desat Indicador se o fundo está ativo ou desativado
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getBenchmarks()
    {
        $this->db->select('CO_BENCH, NO_BENCH');

        $this->db->from('BENCH');

        $this->db->order_by("NO_BENCH", "ASC"); 

        $query = $this->db->get();
        return $query->result_array();
    }
    
    /**
    * Consulta no banco de dados a composição dos ativos de crédito do fundo
    *
    * Tabela Bench_Fator 
    *        BENCH
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getListaBenchmarks
    ($co_bench = null, $dt_ini = null, $dt_fim = null)
    {
        $sql 
            = "
            SELECT 
                B.CO_BENCH, 
                B.NO_BENCH,
                Bench_Fator.DT_REF,
                Bench_Fator.VR_FAT,
                Bench_Fator.VR_RET_LOG_DIA,
                Bench_Fator.VR_RET_DIA,
                BD.VR_DUR
            FROM 
                Bench_Fator 
            INNER JOIN 
                BENCH B 
            ON 
                B.CO_BENCH = Bench_Fator.CO_BENCH
            LEFT JOIN
                Bench_Dia BD
            ON 
                B.CO_BENCH = BD.CO_BENCH AND
                Bench_Fator.DT_REF = BD.DT_REF
            WHERE 
                Bench_Fator.DT_REF >= '$dt_ini' AND 
                Bench_Fator.DT_REF <= '$dt_fim' AND
                B.CO_BENCH = $co_bench 
            ORDER BY 
                Bench_Fator.DT_REF
              ";

        //return $sql;

        return $this->_executaSql($sql);
    }
}