<?php 
/**
* Liquidez_Model File Doc Comment
*
* @category CI_Model
* @package  Models
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Liquidez_Model_CI_Model
*
* @category CI_Model
* @package  Models
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Liquidez_Model extends CI_Model
{
    /**
    * Executa a query sql para consulta no banco de dados
    *
    * @param string $sql O comando SQL que será executado
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    private function _executaSqlS($sql)
    {
        $dados = array();

        try {
            $query = $this->db->query(utf8_decode($sql));

            if ($query->num_rows() <> 0) {
                $dados = $query->result_array();
            }

        } catch (Exception $e) {
            log_message('error', __method__ . print_r($e, true));
            return ($e);
        }
        
        return $dados;
    }

    /**
    * Consulta a composição da carteira de todos fundos VITER
    * para a página inicial do SIRAT web
    *
    * Tabela CR245002_RL_11
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getComposicaoCarteiraGeral($dt_atu = 0)
    {
        if ($dt_atu == 0) {
            return;
        }        

        $sql 
            = "
            SELECT 
                NO_MTP_ATI, 
                SUM(VR_MERC) VR_MERC
            FROM 
                CR245002_RL_11
            WHERE 
                DT_ATU = '$dt_atu' AND (
                'VITER' = 'VITER' OR
                NO_GRU_GESTAO = 'VITER' OR
                (NO_SEG = 'VITER' AND 
                ('0' = 1 AND 
                CO_POL_LIQ = 1 OR 
                '0' = 0 AND 
                ISNULL(CO_POL_LIQ, 0) = 0)) OR
                ('VITER' = 'CR 245' AND 
                CO_POL_LIQ = 1) OR
                CO_PRD = 'VITER')
            GROUP BY 
                NO_MTP_ATI 
            ORDER BY 
                NO_MTP_ATI
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta o volume de vencimentos por prazo de todos fundos VITER
    * para a página inicial do SIRAT web
    *
    * Tabela CR245002_RL_17
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getVolumeVencimentosGeral($dt_atu = 0)
    {
        if ($dt_atu == 0) {
            return;
        }        

        $sql 
            = "
            SELECT 
                NO_FAIXA, 
                SUM(VR_FAIXA)
            FROM 
                CR245002_RL_17
            WHERE 
                DT_ATU = '2013-09-10' AND
                ('VITER' = 'VITER' OR
                NO_GRU_GESTAO = 'VITER' OR
                (NO_SEG = 'VITER' AND 
                ('0' = 1 AND 
                CO_POL_LIQ = 1 OR '0' = 0 AND ISNULL(CO_POL_LIQ, 0) = 0)) OR
        ('VITER' = 'CR 245' AND CO_POL_LIQ = 1) 
    )
GROUP BY NO_FAIXA
            ";

        return $this->_executaSql($sql);
    }





}