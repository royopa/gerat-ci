<?php 
/**
* Produtos File Doc Comment
*
* @category Library
* @package  Libraries
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include_once "../HighchartsPHP-master/Highchart.php";

/**
* GraficosSirat Class
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Grafico extends Highchart
{
    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getChartWithLegend
        ($dados_serie = null, $render_div = '', $name = '', $title = '', $vr_pl = 1)
    {
        $chart = new Highchart();

        $chart->chart->renderTo             = $render_div;
        $chart->chart->plotBackgroundColor  = null;
        $chart->chart->plotBorderWidth      = null;
        $chart->chart->plotShadow           = false;
        $chart->title->text                 = $title;

        /*
        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; 
            }"
        );
        */
    
        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + Highcharts.numberFormat(this.percentage,2) + '%';
            }"
        );

        $chart->plotOptions->pie->allowPointSelect = 1;
        $chart->plotOptions->pie->cursor = "pointer";
        $chart->plotOptions->pie->dataLabels->enabled = false;
        $chart->plotOptions->pie->showInLegend = 1;

        $chart->series[] = array(
          'type' => "pie",
          'name' => $name,
          'data' => $dados_serie);    

        return $chart;
    }

    /**
    * Monta o gráfico Highchart 
    *
    * @access  public
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getCaptacaoRentabilidadeChart
        ($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->zoomType = "xy";
        $chart->title->text     = $title;

        $chart->xAxis = array(
            array(
                'categories' => $serieDados[0]
            )
        );


        $chart->xAxis->categories = $serieDados[0];
        $chart->xAxis->labels->rotation = - 25;
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 70;
        $chart->legend->y = 30;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        //$chart->xAxis->labels->rotation = - 45;
        //$chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $leftYaxis = new HighchartOption();
        $leftYaxis->labels->formatter = new HighchartJsExpr(
            "function() {
                return this.value +'%'; 
            }"
            );

        $leftYaxis->labels->style->color = "#89A54E";
        $leftYaxis->title->text = "Rentabilidade Diária";
        $leftYaxis->title->style->color = "#89A54E";

        $rightYaxis = new HighchartOption();
        $rightYaxis->title->text = "Captação Líquida";
        $rightYaxis->title->style->color = "#4572A7";

        $rightYaxis->labels->formatter = new HighchartJsExpr(
            "function() {
                return 'R$ ' + Highcharts.numberFormat(this.value,2,',','.') +'';
            }"
            );

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.series.name + ': ' + this.x +': '+ 
                (this.series.name == 'Captação Líquida' ? 
                    ' R$ ' : '') + 
                Highcharts.numberFormat(this.y,2,',','.') +
                (this.series.name == 'Captação Líquida' ? '' : ' %'); 
            }");

        $rightYaxis->labels->style->color = "#4572A7";
        $rightYaxis->opposite = 1;
        $chart->yAxis = array(
            $leftYaxis,
            $rightYaxis
        );
        
        /*
        * Legenda 
        */
        $chart->legend->layout = "vertical";
        $chart->legend->align = "left";
        $chart->legend->x = 120;
        $chart->legend->verticalAlign = "top";
        $chart->legend->y = 1;
        $chart->legend->floating = 1;
        $chart->legend->backgroundColor = "#FFFFFF";

        $chart->series[] = array(
            'name' => "Captação Líquida",
            'color' => "#4572A7",
            'type' => "column",
            'yAxis' => 1,
            'data' => $serieDados[1]
        );

        $chart->series[] = array(
            'name' => "Rentabilidade Diária",
            'color' => "#89A54E",
            'type' => "spline",
            'data' => $serieDados[2]
        );        

        $chart->exporting->enabled = true;

        return $chart;
    }

    /**
    * Monta o gráfico Highchart 
    *
    * @access  public
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getDesempenhoMensalChart
        ($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;

        $chart->xAxis->categories = $serieDados[0];
        $chart->xAxis->labels->rotation = - 45;
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $chart->yAxis->title->text = "Rentabilidade (%)";
        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 70;
        $chart->legend->y = 30;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.series.name +' : ' + this.x + ' : ' + this.y + '%';
            }");

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        $chart->series[] = array(
            'name' => "Fundo",
            'data' => $serieDados[1]
        );

        $chart->series[] = array(
            'name' => "Benchmark",
            'data' => $serieDados[2]
        );

        return $chart;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getColumnChart
        ($dados_serie = null, $render_div = '', $name = '', $title = '')
    {
        $subtitle   = "";

        $chart = new Highchart();
        
        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;
        $chart->subtitle->text  = $subtitle;

        $chart->xAxis->categories = array(
            " % em relação ao PL",
        );

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = "Patrimônio Líquido %";

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.series.name + ' : ' + this.y + '%';
            }"
            
        );

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        foreach ($dados_serie as $row) {
          
            $chart->series[] = array(
                'name' => $row[0],
                'data' => array(
                    $row[1]
                )
            );
        }
        return $chart;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getItemRiscoMercadoGrupoChart
        ($dadosSerie = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type = "column";
        $chart->title->text = $title;

        $chart->xAxis->categories = $dadosSerie['categorias'];

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = "";

        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 60;
        $chart->legend->y = 35;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        $chart->xAxis->labels->rotation = - 20;
        $chart->xAxis->labels->style->font = "normal 12px Verdana, sans-serif";

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.x +' : ' + this.series.name + ' ' + this.y + '%';
            }"
            );

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        $chart->series[] = array(
            'name' => "cVar",
            'data' => $dadosSerie['serieCvar']
        );

        $chart->series[] = array(
            'name' => "MtM",
            'data' => $dadosSerie['serieMtm']
        );

        $chart->series[] = array(
            'name' => "Retorno",
            'data' => $dadosSerie['serieRetorno']
        );

        return $chart;
    }


    /**
    * Monta um array com a série da composição da carteira que será usada 
    * para montagem do gráfico de pizza da composição do fundo
    *
    * @param mixed[] $composicao O CNPJ do fundo/carteira
    * @param float   $vr_pl      O patrimônio líquido do fundo/carteira
    *
    * @return  mixed[] $dados_serie Um array com a composiçao da carteira
    */
    public function getDadosSerieComposicaoCarteira
        ($composicao = null, $vr_pl = 1)
    {
        $dados_serie = array();
        $soma_percentual = 0;
        $id_ultimo_elemento = count($composicao) - 1;
    
        foreach ($composicao as $row) {
            /**
            * pega o nome da classe do ativo 
            * (Ações, Tit Publicos, Privados, etc para gerar o grafico)
            */
            $classe_ativo = utf8_encode($row['NO_MTP_ATI']);

            /**
            * pega o percentual do ativo sobre o PL do fundo
            * para mostrar o gráfico
            */            
            $percentual = round(($row['VR_MERC'] / $vr_pl) * 100, 2);

            /**
            * se é o último elemento, subtrai 100 menos a porcentagem, para 
            * mostrar o gráfico corretamente
            */
            if (end($composicao) == $row) {
                $percentual = 100 - $soma_percentual;  
            }
      
            $dados_composicao = array(0 => $classe_ativo, 1 => $percentual);

            $dados_serie[] = $dados_composicao;
      
            $soma_percentual = $soma_percentual + $percentual;
        }
        
        return $dados_serie;
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que 
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @param mixed[] $composicao O CNPJ do fundo/carteira
    * @param float   $vr_pl      O patrimônio líquido do fundo/carteira
    *
    * @return  mixed[] $dados_serie Um array com a composiçao da carteira
    */
    public function getDadosSerieComposicaoCarteiraCredito
        ($composicao_cr = null, $vr_pl = 1)
    {
        $dados_serie_credito = array();

        foreach ($composicao_cr as $row) {
            //pega o nome da classe do ativo 
            //(Ações, Tit Publicos, Privados, etc para gerar o grafico)
            $classe_ativo = utf8_encode($row['NO_TP_ATI']);

            //pega o valor de mercado do fundo abs - módulo do número, 
            //para mostrar o gráfico
            $vr_merc = abs($row['VR_MERC']);

            //pega o percentual do ativo sobre o PL do fundo para gerar o gráfico
            $percentual = round(($vr_merc / $vr_pl) * 100, 2);

            $dados_composicao_credito 
                = array(0 => $classe_ativo, 1 => $percentual);

            $dados_serie_credito[] = $dados_composicao_credito;
        }
        return $dados_serie_credito;
    }

    /**
    * Monta um array com a série de distribuição de cotistas de um fundo
    * Será usado para montagem do gráfico de coluna da distribuição de cotistas
    *
    * @param mixed[] $composicao A distribuição de cotistas vinda do banco
    * @param float   $vr_pl      O patrimônio líquido do fundo/carteira
    *
    * @return  mixed[] $dados_serie Um array com a composiçao da carteira
    */
    public function getDadosSerieDistribuicaoCotistas
        ($composicao = null, $vr_pl = 1)
    {
        $dados_serie = array();

        foreach ($composicao as $row) {
            //pega o nome do fundo/FIC
            $nome_fundo = utf8_encode($row['NO_PRD']);

            //pega o valor da cota para mostrar o gráfico
            $vr_cota = abs($row['VR_COTA_FF']);

            //pega o percentual do valor da cota sobre o PL do fundo
            $percentual = round(($row['VR_COTA_FF'] / $vr_pl) * 100, 2);

            $dados_composicao = array(0 => $nome_fundo, 1 => $percentual);

            $dados_serie[] = $dados_composicao;
        }
        return $dados_serie;
    }
}