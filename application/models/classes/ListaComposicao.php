<?php
/**
 * ListaComposicao File Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 *
 */

/**
 * ListaComposicao Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaComposicao extends ArrayIterator
{
    /**
     * @var SerieDados
     * A série de dados que será usada para geração do gráfico
     */
    private $serieDados;

    /**
     * @var FundoDiario
     * O fundo diário da composição
     */
    private $fundoDiario;

    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the A série de dados que será usada para geração do gráfico.
     *
     * @return SerieDados
     */
    public function getSerieDados()
    {
        return $this->serieDados;
    }

    /**
     * Sets the A série de dados que será usada para geração do gráfico.
     *
     * @param SerieDados $serieDados the serie dados
     *
     * @return self
     */
    public function setSerieDados(SerieDados $serieDados)
    {
        $this->serieDados = $serieDados;

        return $this;
    }

    /**
     * Gets the O fundo diário da composição.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário da composição.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Grafico
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Grafico $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }
}
