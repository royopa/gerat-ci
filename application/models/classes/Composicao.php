<?php
/**
* Composicao File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * Composicao Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 * Dados da composição da carteira de um fundo
 * Ex:
 *    Ativos                 | Total de Investimento  |  % PL
 *    Caixa                  | R$ 1.769.842.496,79    |  16.09%
 *    Caixa Priv             | R$ 137.854.192,28      |  1.25%
 *    Derivativos            | R$ 2.316,80            |  0%
 *    Fundos                 | R$ 39.326.907,11       |  0.36%
 *    Títulos Privados       | R$ 479.283.763,42      |  4.36%
 *    Títulos Privados - IF  | R$ 4.095.378.691,01    |  37.24%
 *    Títulos Públicos       | R$ 4.477.030.394,81    |  40.71%
 *
 * Perfil de distribuição de cotistas de um fundo
 * Ex:
 *    FIC/Carteira           |  Valor aplicado        |  % PL
 *    FIC EXECUTIVO LP       |  R$ 7.599.352.103,71   |  69.09%
 *    FIC INVESTIDOR LP      |  R$ 3.327.564.130,85   |  30.25%
 *    FIC DINAMICO LP        |  R$ 71.585.260,10      |  0.65%
 *
 */
class Composicao
{
    /**
     * @var string
     * O nome dos ativos
     * O nome dos ativos/da descrição
     */
    private $nome;

    /**
     * @var float
     * O valor total investido nesse nome de ativo
     */
    private $valor;

    /**
     * @var float
     * O percentual sobre o patrimônio líquido
     */
    private $percentual;

    /**
     * @var FundoDiario
     * O fundo diário da composição
     */
    private $fundoDiario;

    public function __construct
        (FundoDiario $fundoDia = null)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
        }
    }

    /**
     * Gets the nome dos ativos.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the nome dos ativos.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the O fundo diário da composição.
     * Gets the valor.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário da composição.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;
    }

    /**
     * Gets the O valor total investido nesse nome de ativo.
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Sets the O valor total investido nesse nome de ativo.
     *
     * @param float $valor the valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * Gets the O percentual sobre o patrimônio líquido.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual sobre o patrimônio líquido.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * Composicao::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return Composicao
     */
    public function create(FundoDiario $fundoDia, $row)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDiario = $fundoDia;
        }

        if (count($row) > 0) {

            $this->setNome(utf8_encode($row['nome']));
            $this->setValor((float) abs($row['valor']));
            $this->setPercentual(
                (float) ($this->getValor() / $fundoDia->getPatrimonioLiquido() * 100)
            );
        }

        return $this;
    }
}
