<?php
/**
* ListaDistribuicaoCotistas File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * ListaDistribuicaoCotistas Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaDistribuicaoCotistas extends ListaComposicao
{
    public function __construct
        (FundoDiario $fundoDia = null)
    {
        //var_dump($fundoDia);
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
            $this->fetchAll($fundoDia);
            $this->setSerieDados(new SerieDados($this));
            $this->montaGrafico();
        }
    }

    /**
     * ListaDistribuicaoCotistas::fetchAll()
     *
     * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return ListaDistribuicaoCotistas
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDia = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupoDb
            = $model->getDistribuicaoCotistas(
                $fundoDia->getFundo()->getCnpj(),
                $fundoDia->getDataAtualizacao()->format('Y-m-d')
            );

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {
                $composicao = new Composicao();
                $dados = array();
                $dados['nome']       = $row['NO_PRD'];
                $dados['valor']      = $row['VR_COTA_FF'];
                //adiciona a composicao no objeto lista
                $this->append($composicao->create($fundoDia, $dados));
            }
        }

        return $this;
    }
    /**
     * ListaDistribuicaoCotistas::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $grafico
                    ->getColumnChart(
                        $this->getSerieDados()->getSerieArray(),
                        'chart_distribuicao_cotistas',
                        null,
                        'Distribuição de cotistas'
                    )
            );

        return $this;
    }
}
