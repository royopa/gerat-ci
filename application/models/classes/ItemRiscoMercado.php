<?php
/**
* ItemRiscoMercado File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* IF, IF_DPGE, NãoIF, FIDC
*
*/

/**
 * ItemRiscoMercado Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ItemRiscoMercado
{
    /**
     * @var string
     * O nome do tipo de ativo
     */
    private $nome;

    /**
     * @var float
     * O valor MtM
     */
    private $valorMtm;

    /**
     * @var float
     * O valor CVaR
     */
    private $valorCvar;

    /**
     * @var float
     * O valor VaRStress
     */
    private $valorVarStress;

    /**
     * @var float
     * O percentual do VaR sobre o PL do fundo
     */
    private $percentualPatrimonioLiquido;

    /**
     * @var float
     * Percentual MtM
     */
    private $percentualMtm;

    /**
     * @var float
     * Percentual CVaR
     */
    private $percentualCvar;

    /**
     * @var float
     * Percentual retorno
     */
    private $percentualRetorno;

    /**
     * ItemRiscoMercado::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return ItemRiscoMercado
     */
    public function create(
        FundoDiario $fundoDiario,
        $row = null,
        RiscoMercado $riscoMercado = null
        )
    {
        if (count($row) > 0) {
            $this->setNome(utf8_encode($row['NO_GR_ATIVO']));
            $this->setValorMtm((float) $row['VR_MTM']);
            $this->setValorCvar((float) $row['VR_CVAR']);
            $this->setValorVarStress((float) $row['VR_VAR_S']);
            $this->setPercentualPatrimonioLiquido
                =
                ($this->getValorCvar() / $fundoDiario->getPatrimonioLiquido());

            if ($riscoMercado instanceof RiscoMercado) {
                //var_dump($row['VR_CVAR']);
                //var_dump($riscoMercado->getValorVar1d95());
                //var_dump($this->getPercentualCvar());
                //var_dump((float) $row['VR_CVAR'] / $riscoMercado->getValorVar1d95() * 100);
                $this->percentualCvar = round((float) $row['VR_CVAR'] / $riscoMercado->getValorVar1d95() * 100, 2);
                $this->percentualMtm  = round((float) $row['VR_MTM'] / $riscoMercado->getValorMtm() * 100, 2);
            }
        }

        return $this;
    }

    /**
     * Gets the O nome do tipo de ativo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the O nome do tipo de ativo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the O valor MtM.
     *
     * @return float
     */
    public function getValorMtm()
    {
        return $this->valorMtm;
    }

    /**
     * Sets the O valor MtM.
     *
     * @param float $valorMtm the valor mtm
     *
     * @return self
     */
    public function setValorMtm($valorMtm)
    {
        $this->valorMtm = $valorMtm;

        return $this;
    }

    /**
     * Gets the O valor CVaR.
     *
     * @return float
     */
    public function getValorCvar()
    {
        return $this->valorCvar;
    }

    /**
     * Sets the O valor CVaR.
     *
     * @param float $valorCvar the valor cvar
     *
     * @return self
     */
    public function setValorCvar($valorCvar)
    {
        $this->valorCvar = $valorCvar;

        return $this;
    }

    /**
     * Gets the O percentual do VaR sobre o PL do fundo.
     *
     * @return float
     */
    public function getPercentualPatrimonioLiquido()
    {
        return $this->percentualPatrimonioLiquido;
    }

    /**
     * Sets the O percentual do VaR sobre o PL do fundo.
     *
     * @param float $percentualPatrimonioLiquido the percentual patrimonio liquido
     *
     * @return self
     */
    public function setPercentualPatrimonioLiquido($percentualPatrimonioLiquido)
    {
        $this->percentualPatrimonioLiquido = $percentualPatrimonioLiquido;

        return $this;
    }

    /**
     * Gets the O valor VaRStress.
     *
     * @return float
     */
    public function getValorVarStress()
    {
        return $this->valorVarStress;
    }

    /**
     * Sets the O valor VaRStress.
     *
     * @param float $valorVarStress the valor var stress
     *
     * @return self
     */
    public function setValorVarStress($valorVarStress)
    {
        $this->valorVarStress = $valorVarStress;

        return $this;
    }

    /**
     * Gets the Percentual MtM.
     *
     * @return float
     */
    public function getPercentualMtm()
    {
        return $this->percentualMtm;
    }

    /**
     * Sets the Percentual MtM.
     *
     * @param float $percentualMtm the percentual mtm
     *
     * @return self
     */
    public function setPercentualMtm($percentualMtm)
    {
        $this->percentualMtm = $percentualMtm;

        return $this;
    }

    /**
     * Gets the Percentual CVaR.
     *
     * @return float
     */
    public function getPercentualCvar()
    {
        return $this->percentualCvar;
    }

    /**
     * Sets the Percentual CVaR.
     *
     * @param float $percentualCvar the percentual cvar
     *
     * @return self
     */
    public function setPercentualCvar($percentualCvar)
    {
        $this->percentualCvar = $percentualCvar;

        return $this;
    }

    /**
     * Gets the Percentual retorno.
     *
     * @return float
     */
    public function getPercentualRetorno()
    {
        return $this->percentualRetorno;
    }

    /**
     * Sets the Percentual retorno.
     *
     * @param float $percentualRetorno the percentual retorno
     *
     * @return self
     */
    public function setPercentualRetorno($percentualRetorno)
    {
        $this->percentualRetorno = $percentualRetorno;

        return $this;
    }
}
