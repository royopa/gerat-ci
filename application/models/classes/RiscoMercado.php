<?php
/**
* RiscoMercado File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * RiscoMercado Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class RiscoMercado
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var string
     * O nome do método VaR do fundo
     */
    private $metodoVar;

    /**
     * @var float
     * O nível de confiança
     */
    private $nivelConfianca;
    
    /**
     * @var float
     * O horizonte tempo
     */
    private $horizonteTempo;
    
    /**
     * @var float
     * Valor lambda
     */
    private $valorLambda;
    
    /**
     * @var string
     * Nome do benchmark do fundo
     */
    private $nomeBenchmark;

    /**
     * @var float
     * Valor MtM
     */
    private $valorMtm;
    
    /**
     * @var float
     * Valor MtM
     */
    private $valorVar1d95;

    /**
     * @var float
     * Valor perda carteira 1 dia
     */
    private $valorVar1d95Stress;

    /**
     * @var float
     * Valor perda carteira 1 dia stress
     */
    private $valorVar21d95;
    
    /**
     * @var string
     * Nome do fator
     */
    private $nomeFator;

    /**
     * @var float
     * Valor Duration
     */
    private $valorDuration;

    /**
     * @var float
     * Valor Duration modificada
     */
    private $valorDurationModificada;

    /**
     * @var float
     * Valor Cotas
     */
    private $valorCotas;

    /**
     * @var float
     * Valor PL informado
     */
    private $patrimonioLiquidoInformado;

    /**
     * @var float
     * Valor PL calculado
     */
    private $patrimonioLiquidoCalculado;

    /**
     * @var float
     * O percentual do VaR sobre o Patrimônio Líquido do fundo
     */
    private $percentualVarPatrimonioLiquido;

    public function __construct
        (FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario     = $fundoDiario;
            $this->fetchAll($fundoDiario);
        }
    }

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the O nome do método VaR do fundo.
     *
     * @return string
     */
    public function getMetodoVar()
    {
        return $this->metodoVar;
    }

    /**
     * Sets the O nome do método VaR do fundo.
     *
     * @param string $metodoVar the metodo var
     *
     * @return self
     */
    public function setMetodoVar($metodoVar)
    {
        $this->metodoVar = $metodoVar;

        return $this;
    }

    /**
     * Gets the O nível de confiança.
     *
     * @return float
     */
    public function getNivelConfianca()
    {
        return $this->nivelConfianca;
    }

    /**
     * Sets the O nível de confiança.
     *
     * @param float $nivelConfianca the nivel confianca
     *
     * @return self
     */
    public function setNivelConfianca($nivelConfianca)
    {
        $this->nivelConfianca = $nivelConfianca;

        return $this;
    }

    /**
     * Gets the O horizonte tempo.
     *
     * @return float
     */
    public function getHorizonteTempo()
    {
        return $this->horizonteTempo;
    }

    /**
     * Sets the O horizonte tempo.
     *
     * @param float $horizonteTempo the horizonte tempo
     *
     * @return self
     */
    public function setHorizonteTempo($horizonteTempo)
    {
        $this->horizonteTempo = $horizonteTempo;

        return $this;
    }

    /**
     * Gets the Valor lambda.
     *
     * @return float
     */
    public function getValorLambda()
    {
        return $this->valorLambda;
    }

    /**
     * Sets the Valor lambda.
     *
     * @param float $valorLambda the valor lambda
     *
     * @return self
     */
    public function setValorLambda($valorLambda)
    {
        $this->valorLambda = $valorLambda;

        return $this;
    }

    /**
     * Gets the O percentual do VaR sobre o Patrimônio Líquido do fundo.
     *
     * @return float
     */
    public function getPercentualVarPatrimonioLiquido()
    {
        return $this->percentualVarPatrimonioLiquido * 100;
    }

    /**
     * Sets the O percentual do VaR sobre o Patrimônio Líquido do fundo.
     *
     * @param float $percentualVarPatrimonioLiquido the percentual var patrimonio liquido
     *
     * @return self
     */
    public function setPercentualVarPatrimonioLiquido($percentualVarPatrimonioLiquido)
    {
        $this->percentualVarPatrimonioLiquido = $percentualVarPatrimonioLiquido;

        return $this;
    }

    /**
     * Gets the Nome do benchmark do fundo.
     *
     * @return string
     */
    public function getNomeBenchmark()
    {
        return $this->nomeBenchmark;
    }

    /**
     * Sets the Nome do benchmark do fundo.
     *
     * @param string $nomeBenchmark the nome benchmark
     *
     * @return self
     */
    public function setNomeBenchmark($nomeBenchmark)
    {
        $this->nomeBenchmark = $nomeBenchmark;

        return $this;
    }

    /**
     * Gets the Valor MtM.
     *
     * @return float
     */
    public function getValorMtm()
    {
        return $this->valorMtm;
    }

    /**
     * Sets the Valor MtM.
     *
     * @param float $valorMtm the valor mtm
     *
     * @return self
     */
    public function setValorMtm($valorMtm)
    {
        $this->valorMtm = $valorMtm;

        return $this;
    }

    /**
     * Gets the Valor MtM.
     *
     * @return float
     */
    public function getValorVar1d95()
    {
        return $this->valorVar1d95;
    }

    /**
     * Sets the Valor MtM.
     *
     * @param float $valorVar1d95 the valor var1d95
     *
     * @return self
     */
    public function setValorVar1d95($valorVar1d95)
    {
        $this->valorVar1d95 = $valorVar1d95;

        return $this;
    }

    /**
     * Gets the Valor perda carteira 1 dia.
     *
     * @return float
     */
    public function getValorVar1d95Stress()
    {
        return $this->valorVar1d95Stress;
    }

    /**
     * Sets the Valor perda carteira 1 dia.
     *
     * @param float $valorVar1d95Stress the valor var1d95 stress
     *
     * @return self
     */
    public function setValorVar1d95Stress($valorVar1d95Stress)
    {
        $this->valorVar1d95Stress = $valorVar1d95Stress;

        return $this;
    }

    /**
     * Gets the Valor perda carteira 1 dia stress.
     *
     * @return float
     */
    public function getValorVar21d95()
    {
        return $this->valorVar21d95;
    }

    /**
     * Sets the Valor perda carteira 1 dia stress.
     *
     * @param float $valorVar21d95 the valor var21d95
     *
     * @return self
     */
    public function setValorVar21d95($valorVar21d95)
    {
        $this->valorVar21d95 = $valorVar21d95;

        return $this;
    }

    /**
     * Gets the Nome do fator.
     *
     * @return string
     */
    public function getNomeFator()
    {
        return $this->nomeFator;
    }

    /**
     * Sets the Nome do fator.
     *
     * @param string $nomeFator the nome fator
     *
     * @return self
     */
    public function setNomeFator($nomeFator)
    {
        $this->nomeFator = $nomeFator;

        return $this;
    }

    /**
     * Gets the Valor Duration.
     *
     * @return float
     */
    public function getValorDuration()
    {
        return $this->valorDuration;
    }

    /**
     * Sets the Valor Duration.
     *
     * @param float $valorDuration the valor duration
     *
     * @return self
     */
    public function setValorDuration($valorDuration)
    {
        $this->valorDuration = $valorDuration;

        return $this;
    }

    /**
     * Gets the Valor Duration modificada.
     *
     * @return float
     */
    public function getValorDurationModificada()
    {
        return $this->valorDurationModificada;
    }

    /**
     * Sets the Valor Duration modificada.
     *
     * @param float $valorDurationModificada the valor duration modificada
     *
     * @return self
     */
    public function setValorDurationModificada($valorDurationModificada)
    {
        $this->valorDurationModificada = $valorDurationModificada;

        return $this;
    }

    /**
     * Gets the Valor Cotas.
     *
     * @return float
     */
    public function getValorCotas()
    {
        return $this->valorCotas;
    }

    /**
     * Sets the Valor Cotas.
     *
     * @param float $valorCotas the valor cotas
     *
     * @return self
     */
    public function setValorCotas($valorCotas)
    {
        $this->valorCotas = $valorCotas;

        return $this;
    }

    /**
     * Gets the Valor PL informado.
     *
     * @return float
     */
    public function getPatrimonioLiquidoInformado()
    {
        return $this->patrimonioLiquidoInformado;
    }

    /**
     * Sets the Valor PL informado.
     *
     * @param float $patrimonioLiquidoInformado the patrimonio liquido informado
     *
     * @return self
     */
    public function setPatrimonioLiquidoInformado($patrimonioLiquidoInformado)
    {
        $this->patrimonioLiquidoInformado = $patrimonioLiquidoInformado;

        return $this;
    }

    /**
     * Gets the Valor PL calculado.
     *
     * @return float
     */
    public function getPatrimonioLiquidoCalculado()
    {
        return $this->patrimonioLiquidoCalculado;
    }

    /**
     * Sets the Valor PL calculado.
     *
     * @param float $patrimonioLiquidoCalculado the patrimonio liquido calculado
     *
     * @return self
     */
    public function setPatrimonioLiquidoCalculado($patrimonioLiquidoCalculado)
    {
        $this->patrimonioLiquidoCalculado = $patrimonioLiquidoCalculado;

        return $this;
    }

    /**
     * ItemRiscoMercadoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return ItemRiscoMercadoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            //instancia o model Dbal para fazer acesso ao banco de dados
            $dbal = new Dbal();
            $conn = $dbal->getConn();

            $sql
                = "
                    SELECT
                        NO_MAPS_METODO_VAR,
                        VR_MAPS_NIV_CONF,
                        NU_MAPS_HORIZ,
                        VR_MAPS_LAMBDA,
                        NO_MAPS_CURRENCY,
                        NO_MAPS_VAR_TITLE,
                        VR_MAPS_COTAS,
                        VR_MAPS_PL_INFO,
                        VR_MAPS_PL_CALC,
                        VR_MAPS_MTM,
                        VR_MAPS_VAR_POL,
                        VR_MAPS_VAR_1D_95,
                        VR_MAPS_VAR_1D_95_STRESS,
                        VR_MAPS_VAR_21D_95,
                        VR_MAPS_VAR_1D_99,
                        VR_MAPS_VAR_15D_99,
                        VR_MAPS_VAR_21D_99,
                        NO_MAPS_BENCH,
                        NU_MAPS_MULT,
                        NO_MAPS_IS_NORMALIZED,
                        VR_MAPS_DURATION,
                        VR_MAPS_DURATION_MOD,
                        CO_MAX_FATOR,
                        VR_MAX_FATOR,
                        CO_MAX_ATIVO,
                        VR_MAX_ATIVO,
                        VR_MAX_ATIVO2
                    FROM
                        PRODUTO_DIA
                    WHERE
                        CO_PRD = :cnpj AND
                        DT_ATU = :dataAtualizacao
                  ";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );
            $stmt->bindValue(
                'dataAtualizacao',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );
            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $this->metodoVar      = utf8_encode($row['NO_MAPS_METODO_VAR']);
                $this->nivelConfianca = (float) ($row['VR_MAPS_NIV_CONF']);
                $this->horizonteTempo = (int) ($row['NU_MAPS_HORIZ']);
                $this->valorLambda    = (float) ($row['VR_MAPS_LAMBDA']);
                $this->nomeBenchmark  = utf8_encode($row['NO_MAPS_BENCH']);
                $this->valorMtm       = (float) $row['VR_MAPS_MTM'];
                $this->valorVar1d95   = (float) $row['VR_MAPS_VAR_1D_95'];
                $this->valorVar1d95Stress  = (float) $row['VR_MAPS_VAR_1D_95_STRESS'];
                $this->valorVar21d95  = (float) $row['VR_MAPS_VAR_21D_95'];
                $this->nomeFator      = utf8_encode($row['CO_MAX_FATOR']);
                $this->valorDuration  = (float) $row['VR_MAPS_DURATION'];
                $this->valorDurationModificada  = (float) $row['VR_MAPS_DURATION_MOD'];
                $this->valorCotas  = (float) $row['VR_MAPS_COTAS'];
                $this->patrimonioLiquidoInformado  = (float) $row['VR_MAPS_PL_INFO'];
                $this->patrimonioLiquidoCalculado  = (float) $row['VR_MAPS_PL_CALC'];
                $this->percentualVarPatrimonioLiquido 
                    = $this->valorVar1d95 / $fundoDiario->getPatrimonioLiquido();
            }
        }
        return $this;
    }    
}
