<?php
/**
* Relatorio File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * Relatorio Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class Relatorio extends ArrayIterator
{
    /**
     * @var int
     * O id do relatório
     */
    private $id;

    /**
     * @var string
     * O nome do relatório
     */
    private $nome;

    /**
     * @var string
     * A descrição do relatório
     */
    private $descricao;

    /**
     * @var Doctrine\DBAL\Connection
     * A conexão DBAL do relatório
     */
    private $conn;

    public function __construct(Doctrine\DBAL\Connection $conn = null)
    {
        if ($conn instanceof Doctrine\DBAL\Connection) {
            $this->conn = $conn;
        }
    }

    /**
     * Gets the O id do relatório.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the O id do relatório.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the O nome do relatório.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the O nome do relatório.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the A descrição do relatório.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Sets the A descrição do relatório.
     *
     * @param string $descricao the descricao
     *
     * @return self
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Gets the A conexão DBAL do relatório.
     *
     * @return Doctrine\DBAL\Connection
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * Sets the A conexão DBAL do relatório.
     *
     * @param Doctrine\DBAL\Connection $conn the conn
     *
     * @return self
     */
    public function setConn(Doctrine\DBAL\Connection $conn)
    {
        $this->conn = $conn;

        return $this;
    }

    /**
     * Gets the object in the database
     *
     * @return self
     */
    public function get($id = null)
    {
        $this->id = $id;

        $sql
            = "
            SELECT
                relatorio_id AS idRelatorio,
                relatorios.nome AS nomeRelatorio, 
                relatorios.descricao AS descricaoRelatorio,
                views_relatorios.posicao,
                views.id AS idView,
                views.nome AS nomeView,
                views.path AS pathView,
                views.descricao AS descricaoView
            FROM
                views_relatorios
            INNER JOIN 
                views
            ON
                views.id = views_relatorios.view_id
            INNER JOIN
                relatorios
            ON
                relatorios.id = views_relatorios.relatorio_id
            WHERE
                relatorios.id = :id
            ORDER BY
                views_relatorios.posicao
        ";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue("id", $id);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            
            $this->nome      = $row['nomeRelatorio'];
            $this->descricao = $row['descricaoRelatorio'];

            $view = new ViewRelatorio();
            $view->setId($row['idView']);
            $view->setNome($row['nomeView']);
            $view->setPath($row['pathView']);
            $view->setDescricao($row['descricaoView']);
            $view->setPosicao($row['posicao']);
            
            $this->append($view);
        }

        return $this;
    }
}
