<?php
/**
* DesempenhoMensalGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoMensalGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class DesempenhoMensalGrupo extends ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }    

    public function __construct(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $datas = $this->getUltimoDiaMes($fundoDiario);
            $this->fetchAll($fundoDiario, $datas);
            $this->montaGrafico();
        }
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que 
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @return  mixed[] $dados_serie Um array com os arrays que serão usados
    */
    private function montaSerieDados()
    {
        $dadosSerie        = array();
        $datas             = array();
        $retornosFundo     = array();
        $retornosBenchmark = array();

        foreach ($this as $desempenhoMensal) {
            
            $datas[] = $desempenhoMensal->getData()->format('m/Y');
            
            $retornosFundo[]
                = $desempenhoMensal->getRentabilidadeFundo();

            $retornosBenchmark[]
                = $desempenhoMensal->getRentabilidadeBenchmark();
        }

        $dadosSerie[0] = $datas;
        $dadosSerie[1] = $retornosFundo;
        $dadosSerie[2] = $retornosBenchmark;

        return $dadosSerie;
    }

    /**
     * DesempenhoMensalGrupo::montaGrafico()
     *
     * @return DesempenhoMensalGrupo
     *
     * Monta o gráfico de colunas dos retornos fundo/benchmark
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        
        $this->grafico = 
            $grafico->getDesempenhoMensalChart(
                    $this->montaSerieDados(),
                    'chart_desempenho_mensal',
                    null,
                    'Rentabilidade do Fundo x Benchmark'
                );

        return $this;
    }

    /**
     * DesempenhoDiarioGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return DesempenhoDiarioGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function getUltimoDiaMes(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            //instancia o model Dbal para fazer acesso ao banco de dados
            $dbal = new Dbal();
            $conn = $dbal->getConn();

            $sql
                = "
                SELECT
                    MAX(produto_dia.DT_REF) DT_REF
                FROM
                    produto_dia
                INNER JOIN
                    produto_dia_2
                ON
                    produto_dia.co_prd = produto_dia_2.co_prd AND
                    produto_dia.dt_ref = produto_dia_2.dt_ref
                WHERE
                    produto_dia.co_prd = :cnpj AND
                    produto_dia.dt_ref < :dataAtualizacao
                GROUP BY
                    MONTH(produto_dia.DT_REF)
                ORDER BY 
                    produto_dia.DT_REF DESC                    
                ";

            $stmt = $conn->prepare($sql);
            
            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );

            $stmt->bindValue(
                'dataAtualizacao',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );

            $stmt->execute();

            $datas = array();

            while ($row = $stmt->fetch()) {
                $datas[] = $row['DT_REF'];
            }
            
            /*
            * remove o primeiro elemento do array datas, que é o mês atual, que
            * ainda não terminou, portanto, ainda não tem rendimento mensal
            * fechado
            */
            unset($datas[0]);

            return $datas;
        }
    }

    public function fetchAll(FundoDiario $fundoDiario = null, $datas = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            //instancia o model Dbal para fazer acesso ao banco de dados
            $dbal = new Dbal();
            $conn = $dbal->getConn();

            $stmt = $conn->executeQuery(
                '
                SELECT 
                    DT_REF,
                    VR_RET_MES
                FROM 
                    produto_dia_2
                WHERE 
                    dt_ref 
                IN 
                    (?) AND
                CO_PRD = ? 
                ',
                array($datas, $fundoDiario->getFundo()->getCnpj()),
                array(\Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
            );

            while ($row = $stmt->fetch()) {
                $desempenho = new DesempenhoMensal();
                $desempenho->create($row, $fundoDiario);
                $this->append($desempenho);
            }
        }
        return $this;
    }
}
