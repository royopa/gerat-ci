<?php
/**
* ViewRelatorio File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * ViewRelatorio Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class ViewRelatorio extends ArrayIterator
{
    /**
     * @var int
     * O id do relatório
     */
    private $id;

    /**
     * @var string
     * O nome do relatório
     */
    private $nome;

    /**
     * @var string
     * A descrição do relatório
     */
    private $descricao;

    /**
     * @var string
     * O path de localização da view
     */
    private $path;

    /**
     * @var int
     * A posição da view dentro do relatório
     */
    private $posicao;

    /**
     * Gets the O id do relatório.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the O id do relatório.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the O nome do relatório.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the O nome do relatório.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the A descrição do relatório.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Sets the A descrição do relatório.
     *
     * @param string $descricao the descricao
     *
     * @return self
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Gets the O path de localização da view.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Sets the O path de localização da view.
     *
     * @param string $path the path
     *
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Gets the A posição da view dentro do relatório.
     *
     * @return int
     */
    public function getPosicao()
    {
        return $this->posicao;
    }

    /**
     * Sets the A posição da view dentro do relatório.
     *
     * @param int $posicao the posicao
     *
     * @return self
     */
    public function setPosicao($posicao)
    {
        $this->posicao = $posicao;

        return $this;
    }
}
