<?php
/**
* LimiteIfDpgeGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * LimiteIfDpgeGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteIfDpgeGrupo extends LimiteAlocacaoGrupo
{
    public function __construct
        (FundoDiario $fundoDia = null)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->setNome('DPGE');
            $this->fundoDia = $fundoDia;
            $this->fetchAll($fundoDia);
            $this->preencheTotalizadores();
        }
    }

    /**
     * LimiteIfDpgeGrupo::preencheTotalizadores()
     *
     * @return LimiteIfDpgeGrupo
     *
     * Preenche os totalizadores de valor e percentual total de alocação
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $limite) {
            $this->setValorTotal(
                $this->getValorTotal() + $limite->getValorAlocado()
            );
        }

        $this->setPercentualTotal(
            ($this->getValorTotal() / $this->fundoDia->getPatrimonioLiquido()) * 100
            );

        return $this;
    }    
}
