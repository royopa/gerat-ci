<?php
/**
* AtivoCreditoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * AtivoCreditoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class AtivoCreditoGrupo extends ArrayIterator
{
    /**
     * @var string
     * Nome do tipo de ativo de crédito
     */
    private $nome;

    /**
     * Gets the Nome do tipo de ativo de crédito.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the Nome do tipo de ativo de crédito.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    public function __construct
        (FundoDiario $fundoDia = null)
    {
        //var_dump($fundoDia);
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
            $this->fetchAll($fundoDia);
        }
    }

    /**
     * AtivoCreditoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return AtivoCreditoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDia = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupoDb
            = $model->getAtivosCredito(
                $fundoDia->getFundo()->getCnpj(),
                $fundoDia->getDataAtualizacao()->format('Y-m-d'),
                $this->nome
            );

        //var_dump($this->nome);
        //var_dump($grupoDb);

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {

                $ativoCredito = new AtivoCredito();

                //adiciona o ativo de crédito no objeto grupo
                $this->append($ativoCredito->create($fundoDia, $row));
            }
        }
        return $this;
    }
}