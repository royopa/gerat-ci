<?php
/**
* Dbal File Doc Comment
*
* @category Library
* @package  Libraries
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * Dbal Class Doc Comment
 *
 * @category Library
 * @package  Libraries
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 * http://docs.doctrine-project.org/en/2.0.x/cookbook/integrating-with-codeigniter.* * html
 */
use Doctrine\Common\ClassLoader;

class Dbal
{
    public  $connDbal  = null;
    private $conn      = null;
    private $connRisco = null;
    
    public function __construct($connectionParams = null)
    {
        if (is_null($connectionParams)) {
            
            //dev
            $connectionParams = array(
                'dbname'   => 'model',
                'user'     => 'sa',
                'password' => 'sa',
                'host'     => 'localhost',
                'driver'   => 'pdo_sqlsrv2000',
            );

            //prd
            $connectionParams = array(
                'dbname'   => 'SIRAT',
                'user'     => 'sirat',
                'password' => 'sirat',
                'host'     => 'sp0000sr024',
                'driver'   => 'pdo_sqlsrv2000',
            );
        }

        //prd
        $connectionParamsRisco = array(
            'dbname'   => 'RISCO',
            'user'     => 'systemls',
            'password' => 'systemls',
            'host'     => 'sp0000sr024',
            'driver'   => 'pdo_sqlsrv2000',
        );

        // Set up class loading. You could use different autoloaders, provided by
        //your favorite framework, if you want to.
        $rootDir = substr(BASEPATH, 0, count(BASEPATH) - 8);
        require_once $rootDir.'/vendor/DoctrineDBAL/Doctrine/Common/ClassLoader.php';

        $classLoader = new ClassLoader('Doctrine', $rootDir.'/vendor/DoctrineDBAL/');
        $classLoader->register();

        $config = new \Doctrine\DBAL\Configuration();

        $this->connDbal = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        $this->conn = $this->connDbal;

        $this->connRisco = \Doctrine\DBAL\DriverManager::getConnection($connectionParamsRisco, $config);
    }

    /**
     * Gets the value of connDbal.
     *
     * @return mixed
     */
    public function getConnDbal()
    {
        return $this->connDbal;
    }

    /**
     * Sets the value of connDbal.
     *
     * @param mixed $connDbal the conn dbal
     *
     * @return self
     */
    public function setConnDbal($connDbal)
    {
        $this->connDbal = $connDbal;

        return $this;
    }

    /**
     * Gets the value of conn.
     *
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * Sets the value of conn.
     *
     * @param mixed $conn the conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }

    /**
     * Gets the value of connRisco.
     *
     * @return mixed
     */
    public function getConnRisco()
    {
        return $this->connRisco;
    }
    
    /**
     * Sets the value of connRisco.
     *
     * @param mixed $connRisco the conn risco
     *
     * @return self
     */
    public function setConnRisco($connRisco)
    {
        $this->connRisco = $connRisco;

        return $this;
    }
}
