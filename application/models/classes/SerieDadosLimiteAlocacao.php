<?php
/**
* SerieDadosLimiteAlocacao File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * SerieDadosLimiteAlocacao Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class SerieDadosLimiteAlocacao
{
    /**
     * @var LimiteAlocacaoGrupo
     * O grupo com os limites de alocação para geração da série
     */
    private $limiteAlocacaoGrupo;

    /**
     * @var mixed[]
     * A lista de composição em formato array para ser usada no gráfico
     */
    private $serieArray;

    public function __construct
        (LimiteAlocacaoGrupo $limiteAlocacaoGrupo = null)
    {
        if ($limiteAlocacaoGrupo instanceof LimiteAlocacaoGrupo) {
            $this->limiteAlocacaoGrupo = $limiteAlocacaoGrupo;
            $this->montaArray();
        }
    }

    /**
     * Gets the O grupo com os limites de alocação para geração da série.
     *
     * @return LimiteAlocacaoGrupo;
     */
    public function getLimiteAlocacaoGrupo()
    {
        return $this->limiteAlocacaoGrupo;
    }

    /**
     * Sets the O grupo com os limites de alocação para geração da série.
     *
     * @param LimiteAlocacaoGrupo; $limiteAlocacaoGrupo the limite alocacao grupo
     *
     * @return self
     */
    public function setLimiteAlocacaoGrupo(LimiteAlocacaoGrupo $limiteAlocacaoGrupo)
    {
        $this->limiteAlocacaoGrupo = $limiteAlocacaoGrupo;

        return $this;
    }

    /**
     * Gets the A lista de composição em formato array para ser usada no gráfico.
     *
     * @return mixed[]
     */
    public function getSerieArray()
    {
        return $this->serieArray;
    }

    /**
     * Sets the A lista de composição em formato array para ser usada no gráfico.
     *
     * @param array $serieArray the serie array
     *
     * @return self
     */
    public function setSerieArray(array $serieArray)
    {
        $this->serieArray = $serieArray;

        return $this;
    }

    /**
     * Monta o array, get dados série que será usado no gráfico
     *
     * @return self
     */
    public function montaArray()
    {
        $dadosSerie     = array();

        foreach ($this->limiteAlocacaoGrupo as $limite) {

            $dados = array(
                0 => $limite->getEmissor()->getNome(),
                1 => round($limite->getPercentualAlocado(), 2)
            );            

            $dadosSerie[]   = $dados;
        }

        $this->serieArray = $dadosSerie;

        return;
    }
}
