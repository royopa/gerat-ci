<?php
/**
* CronogramaVencimento File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * CronogramaVencimento Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class CronogramaVencimento
{
    /**
     * @var DateTime
     * Data de vencimento
     */
    private $dataVencimento;

    /**
     * @var float
     * O volume do que irá vencer
     */
    private $valorVolume;

    /**
     * @var float
     * O percentual em relação ao PL do fundo
     */
    private $percentual;

    /**
     * Gets the Data de vencimento.
     *
     * @return DateTime
     */
    public function getDataVencimento()
    {
        return $this->dataVencimento;
    }

    /**
     * Sets the Data de vencimento.
     *
     * @param DateTime $dataVencimento the data vencimento
     *
     * @return self
     */
    public function setDataVencimento(DateTime $dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;

        return $this;
    }

    /**
     * Gets the O volume do que irá vencer.
     *
     * @return float
     */
    public function getValorVolume()
    {
        return $this->valorVolume;
    }

    /**
     * Sets the O volume do que irá vencer.
     *
     * @param float $valorVolume the valor volume
     *
     * @return self
     */
    public function setValorVolume($valorVolume)
    {
        $this->valorVolume = $valorVolume;

        return $this;
    }

    /**
     * Gets the O percentual em relação ao PL do fundo.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual em relação ao PL do fundo.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * CronogramaVencimento::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return CronogramaVencimento
     */
    public function create(FundoDiario $fundoDiario, $row)
    {
        if ((count($row) > 0) && ($fundoDiario instanceof FundoDiario)) {

            $this->setDataVencimento(
                new DateTime(
                    $row['ano'] . 
                    '-' . 
                    $row['mes'] .
                    '-' . 
                    01
                    )
            );
            
            $this->setValorVolume((float) $row['VR_VOLUME']);
            $this->setPercentual((($this->valorVolume / $fundoDiario->getPatrimonioLiquido()) * 100));
        }

        return $this;
    }
}
