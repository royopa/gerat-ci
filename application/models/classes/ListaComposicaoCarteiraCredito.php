<?php
/**
 * ListaComposicaoCarteiraCredito File Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 *
 */

/**
 * ListaComposicaoCarteiraCredito Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaComposicaoCarteiraCredito extends ListaComposicao
{
    public function __construct
        (FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario = $fundoDiario;
            $this->fetchAll($fundoDiario);
            $this->setSerieDados(new SerieDados($this));
            $this->montaGrafico();
        }
    }

    /**
     * ListaComposicaoCarteiraCredito::fetchAll()
     *
     * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return ListaComposicaoCarteiraCredito
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupoDb
            = $model->getComposicaoCarteiraCredito(
                $fundoDiario->getFundo()->getCnpj(),
                $fundoDiario->getDataAtualizacao()->format('Y-m-d')
            );

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {
                $composicao = new Composicao();
                $dados = array();
                $dados['nome']  = $row['NO_TP_ATI'];
                $dados['valor'] = $row['VR_MERC'];
                //adiciona a composicao no objeto lista
                $this->append($composicao->create($fundoDiario, $dados));
            }
        }

        return $this;
    }

    /**
     * ListaComposicaoCarteiraCredito::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $this
                    ->getColumnChart(
                        $this->getSerieDados()->getSerieArray(),
                        'chart_composicao_credito',
                        null,
                        'Ativos Crédito (% PL)'
                    )
            );

        return $this;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getColumnChart
        ($dados_serie = null, $render_div = '', $name = '', $title = '')
    {
        $subtitle   = "";

        $chart = new Highchart();
        
        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;
        $chart->subtitle->text  = $subtitle;

        $chart->xAxis->categories = array(
            " % em relação ao PL",
        );

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = "Patrimônio Líquido %";

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.series.name + ' : ' + this.y + '%';
            }"
            
        );

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        foreach ($dados_serie as $row) {
          
            $chart->series[] = array(
                'name' => $row[0],
                'data' => array(
                    $row[1]
                )
            );
        }
        return $chart;
    }    
}
