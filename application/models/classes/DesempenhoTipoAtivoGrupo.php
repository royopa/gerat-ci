<?php
/**
* DesempenhoTipoAtivoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoTipoAtivoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class DesempenhoTipoAtivoGrupo extends ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * @var float
     * O resultado financeiro total
     */
    private $resultado;

    /**
     * @var float
     * Rentabilidade total do fundo
     */
    private $rentabilidadeTotal;

    /**
     * @var float
     * O valor das despesas do fundo
     */
    private $despesa;

    /**
     * Gets the gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $desempenho;
    }

    public function __construct(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fetchAll($fundoDiario);
            $this->montaGrafico();
        }
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @return  mixed[] $dados_serie Um array com os arrays que serão usados
    */
    public function montaSerieDados()
    {
        $dadosSerie        = array();
        $datas             = array();

        foreach ($this as $desempenhoTipoAtivo) {

            $desempenhoArray = array();
            $desempenhoArray['name']      = $desempenhoTipoAtivo->getNome();
            $desempenhoArray['y']         = $desempenhoTipoAtivo->getPercentual();
            $desempenhoArray['valor']     = number_format($desempenhoTipoAtivo->getValor(),2,",",".");

            $dadosSerie[] = $desempenhoArray;
        }

        return $dadosSerie;
    }

    /**
     * DesempenhoTipoAtivoGrupo::montaGrafico()
     *
     * @return DesempenhoTipoAtivoGrupo
     *
     * Monta o gráfico de colunas dos retornos fundo/benchmark
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();

        $this->grafico =
            $this->getDesempenhoTipoAtivoChart(
                    $this->montaSerieDados(),
                    'chart_desempenho_tipo_ativo',
                    null,
                    'Desempenho por tipo de ativo'
                );

        return $this;
    }

    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            $dbal = new Dbal();
            $conn = $dbal->getConnRisco();

            /*
            Ao definir o período, colocar
            @dt_ini = primeiro dia útil do mês considerado
            @dt_fim = primeiro dia útil do mês seguinte
            */

            $dataFim = $fundoDiario->getDataAtualizacao()->format('Y-m') . '-01';


            $dataInicio = $this->getMinDataReferenciaMesAnterior($fundoDiario);
            $dataInicio = $dataInicio->format('Y-m-d');

            $dataFim = $this->getMinDataReferenciaMesAtual($fundoDiario);
            $dataFim = $dataFim->format('Y-m-d');

            $cnpj = $fundoDiario->getFundo()->getCnpj();

            $sql
                = "
                SELECT
                    m.co_mtp_ati,
                    m.no_mtp_ati,
                    a.no_tp_ati,
                    a.vr_result,
                    a.pc_ret_tp_ati AS pc_ret_tp_ati_s_bench_aj
                FROM (
                    SELECT
                        CASE
                            WHEN t.co_mtp_ati is null THEN
                                CASE
                                     WHEN a.no_tp_ati = 'Compromiss. Privado' then 10 
                                     WHEN a.no_tp_ati = 'Compromiss. Público' then 11 
                                     WHEN a.no_tp_ati = 'Futuro DI1' then 4
                                    ELSE ''
                                END
                            ELSE
                                t.co_mtp_ati
                        END
                        co_mtp_ati,
                        a.no_tp_ati,
                        a.vr_result,
                        a.pc_ret_tp_ati
                    FROM
                        arr_periodo_produto_tipo_ativo a
                    LEFT JOIN
                        sirat.dbo.ativo_tipo t
                    ON
                        a.no_tp_ati = t.no_tp_ati
                    WHERE
                        a.co_prd = '$cnpj' AND
                        a.dt_ini = '$dataInicio' AND
                        a.dt_fim = '$dataFim'
                ) a
                LEFT JOIN
                    sirat.dbo.ativo_tipo_macrotipo m
                ON
                    a.co_mtp_ati = m.co_mtp_ati
                ORDER BY
                    CASE
                        WHEN
                            a.co_mtp_ati = 11 THEN '0'
                        WHEN
                            a.co_mtp_ati in (9, 10) THEN '1'
                        ELSE
                            m.no_mtp_ati
                    END
                ";                

            $stmt = $conn->prepare($sql);

            //echo $sql . '</br></br>';

            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $desempenho = new DesempenhoTipoAtivo();
                $desempenho->create($row);
                $this->append($desempenho);
            }

            $sqlDespesa
                = "
                SELECT
                    'Despesas' AS no_mtp_ati,
                    'Despesas' AS no_tp_ati,
                    vr_desp AS vr_result,
                    pc_ret_desp AS pc_ret_tp_ati_s_bench_aj
                FROM
                    arr_periodo_produto
                WHERE
                    co_prd = '$cnpj' AND
                    dt_ini = '$dataInicio' AND
                    dt_fim = '$dataFim'
                ";

            //echo $sqlDespesa;

            $despesa = $conn->fetchAssoc(
                $sqlDespesa,
                array($cnpj, $dataInicio, $dataFim));

            $desempenho = new DesempenhoTipoAtivo();
            $desempenho->create($despesa);
            $this->append($desempenho);
        }

        return $this;
    }

    /**
     * DesempenhoTipoAtivoGrupo::getMinDataReferenciaMesAnterior(FundoDiario $fundoDiario)
     *
     * @param $dataAtualizacao DateTime
     *
     */
    public function getMinDataReferenciaMesAnterior(FundoDiario $fundoDiario = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $sql
            = "
            SELECT
                MIN(DT_REF) AS DT_REF
            FROM
                arr_produto_dia
            WHERE
                CO_PRD = ? AND
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $minDataReferenciaMesAnterior = new DateTime($fundoDiario->getDataAtualizacao()->format('Y-m-d'));
        $minDataReferenciaMesAnterior->sub(new DateInterval('P1M3D'));
        $mes = (int) $minDataReferenciaMesAnterior->format('m');
        $ano = (int) $minDataReferenciaMesAnterior->format('Y');

        $data
            = $conn->fetchAssoc(
                $sql,
                array(
                    $fundoDiario->getFundo()->getCnpj(),
                    $mes,
                    $ano)
                );

        return new DateTime($data['DT_REF']);
    }

    /**
     * DesempenhoTipoAtivoGrupo::getMinDataReferenciaMesAtual(FundoDiario $fundoDiario)
     *
     * @param $dataAtualizacao DateTime
     *
     */
    public function getMinDataReferenciaMesAtual(FundoDiario $fundoDiario = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $sql
            = "
            SELECT
                MIN(DT_REF) AS DT_REF
            FROM
                arr_produto_dia
            WHERE
                CO_PRD = ? AND
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $mes = (int) $fundoDiario->getDataAtualizacao()->format('m');
        $ano = (int) $fundoDiario->getDataAtualizacao()->format('Y');

        $data
            = $conn->fetchAssoc(
                $sql,
                array(
                    $fundoDiario->getFundo()->getCnpj(),
                    $mes,
                    $ano)
                );

        return new DateTime($data['DT_REF']);
    }

    /**
    * Monta o gráfico Highchart
    *
    * @access  public
    *
    * @return  Highchart $chart O gráfico highchart que será renderizado
    */
    public function getDesempenhoTipoAtivoChart
        ($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();
        $chart->includeExtraScripts();

        $chart->chart = array(
            'renderTo' => 'chart_desempenho_tipo_ativo',
            'type'     => 'waterfall'
        );
        $chart->title->text = 'Contribuição de Rentabilidade por Ativo';
        
        $chart->subtitle->text = 'Rentabilidade Total do Fundo: 100.81% ';
        
        $chart->xAxis->type = 'category';
       
        $chart->legend->enabled = false;

        $chart->tooltip->pointFormat = 
            "R$ <b>{point.valor}</b> : <b>{point.y:,.2f}</b>%";

        $somaSerie = array(
                    'name' => 'Total',
                    'isIntermediateSum' => true,
                    'color' => new HighchartJsExpr(
                        'Highcharts.getOptions().colors[1]'
                    )
                );

        $serieDados[] = $somaSerie;
       
        $chart->xAxis->labels->rotation = 25;
        $chart->xAxis->labels->style->font = "normal 12px Verdana, sans-serif";

        $chart->yAxis->title->text = "Contribuição %";

        $chart->series = array(
            array(
                'upColor' => new HighchartJsExpr(
                    'Highcharts.getOptions().colors[2]'
                    ),
                'color' => new HighchartJsExpr(
                    'Highcharts.getOptions().colors[3]'
                    ),
                'data' => $serieDados,
                'dataLabels' => array(
                    'enabled' => false,
                    'inside'  => false,
                    'zIndex'  => 6,                    
                    'formatter' => new HighchartJsExpr(
                        "function () {
                            return Highcharts.numberFormat(this.y, 2, ',') + '%';
                        }"
                    ),
                    'style' => array(
                        'color'      => '#274b6d',
                        //'color'      => '#FFFFFF',
                        //'color'      => '#4F4F4F',
                        'fontWeight' => 'bold',
                        //'textShadow' => '0px 0px 3px black'
                    )
                ),
                'pointPadding' => 0
            )
        );

        return $chart;
    }    
}
