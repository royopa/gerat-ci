<?php
/**
* ItemRiscoMercadoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * ItemRiscoMercadoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ItemRiscoMercadoGrupo extends ArrayIterator
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var RiscoMercado
     * Dados de risco de mercado
     */
    private $riscoMercado;

    /**
     * @var float
     * A soma total dos valores MtM líquidos
     */
    private $valorMtmTotal;

    /**
     * @var float
     * A soma total dos valores CVaR
     */
    private $valorCvarTotal;

    /**
     * @var float
     * A soma total dos percentuais em relação ao PL dos valores alocados
     */
    private $percentualTotal;

    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;
    
    public function __construct
        (FundoDiario $fundoDiario = null, RiscoMercado $riscoMercado= null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario     = $fundoDiario;
            $this->riscoMercado    = $riscoMercado;
            $this->valorEmGarantia = $fundoDiario->getValorEmGarantia();
            $this->fetchAll($fundoDiario);
            $this->preencheTotalizadores();
        }
    }

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the A soma total dos valores MtM líquidos.
     *
     * @return float
     */
    public function getValorMtmTotal()
    {
        return $this->valorMtmTotal;
    }

    /**
     * Sets the A soma total dos valores MtM líquidos.
     *
     * @param float $valorMtmTotal the valor mtm total
     *
     * @return self
     */
    public function setValorMtmTotal($valorMtmTotal)
    {
        $this->valorMtmTotal = $valorMtmTotal;

        return $this;
    }

    /**
     * Gets the A soma total dos valores CVaR.
     *
     * @return float
     */
    public function getValorCvarTotal()
    {
        return $this->valorCvarTotal;
    }

    /**
     * Sets the A soma total dos valores CVaR.
     *
     * @param float $valorCvarTotal the valor cvar total
     *
     * @return self
     */
    public function setValorCvarTotal($valorCvarTotal)
    {
        $this->valorCvarTotal = $valorCvarTotal;

        return $this;
    }

    /**
     * Gets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @return float
     */
    public function getPercentualTotal()
    {
        return $this->percentualTotal;
    }

    /**
     * Sets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @param float $percentualTotal the percentual total
     *
     * @return self
     */
    public function setPercentualTotal($percentualTotal)
    {
        $this->percentualTotal = $percentualTotal;

        return $this;
    }

    /**
     * ItemRiscoMercadoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return ItemRiscoMercadoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            //instancia o model Dbal para fazer acesso ao banco de dados
            $dbal = new Dbal();
            $conn = $dbal->getConn();

            $sql
                = "
                  SELECT
                    NO_GR_ATIVO,
                    VR_MTM,
                    VR_CVAR,
                    VR_VAR_S
                    FROM
                        CR245002_RM_F_16
                    WHERE
                        CO_PRD = :cnpj AND
                        DT_ATU = :dataAtualizacao
                    ORDER BY
                        NO_GR_ATIVO
                  ";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );
            $stmt->bindValue(
                'dataAtualizacao',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );
            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $itemRiscoMercado = new ItemRiscoMercado();
                //adiciona o ativo de crédito no objeto grupo
                $this->append($itemRiscoMercado->create($fundoDiario, $row, $this->getRiscoMercado()));
            }
        }

        return $this;
    }

    /**
     * ItemRiscoMercadoGrupo::preencheTotalizadores()
     *
     * @return ItemRiscoMercadoGrupo
     *
     * Preenche os totalizadores de valor e percentual total de alocação
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $itemRiscoMercado) {

            $this->valorCvarTotal
                = $this->valorCvarTotal + $itemRiscoMercado->getValorCvar();

            $this->valorMtmTotal
                = $this->valorMtmTotal + $itemRiscoMercado->getValorMtm();

            $this->percentualTotal
                = ($this->valorCvarTotal / $this->getFundoDiario()->getPatrimonioLiquido()) * 100;
        }

        return $this;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        $grafico = new Grafico();
        
        $this->grafico = 
            $grafico->getItemRiscoMercadoGrupoChart(
                    $this->montaSerieDados(),
                    'chart_risco_mercado',
                    null,
                    'Relação Risco x Retorno'
                );
       
        return $this->grafico;
    }

    /**
    * Monta um array com a série de dados
    *
    * @return  mixed[] $dados_serie Um array com os arrays separados por séries
    */
    private function montaSerieDados()
    {
        $dadosSerie        = array();
        $categorias        = array();
        $serieCvar         = array();
        $serieMtm          = array();
        $serieRetorno      = array();

        foreach ($this as $itemRiscoMercado) {
            
            $categorias[] = $itemRiscoMercado->getNome();
            
            $serieCvar[]
                = $itemRiscoMercado->getPercentualCvar();

            $serieMtm[]
                = $itemRiscoMercado->getPercentualMtm();

            $serieRetorno[]
                = $itemRiscoMercado->getPercentualRetorno();
        }

        $dadosSerie['categorias']   = $categorias;
        $dadosSerie['serieCvar']    = $serieCvar;
        $dadosSerie['serieMtm']     = $serieMtm;
        $dadosSerie['serieRetorno'] = $serieRetorno;

        return $dadosSerie;
    }
    
    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    /**
     * Gets the Dados de risco de mercado.
     *
     * @return RiscoMercado
     */
    public function getRiscoMercado()
    {
        return $this->riscoMercado;
    }
    
    /**
     * Sets the Dados de risco de mercado.
     *
     * @param RiscoMercado $riscoMercado the risco mercado
     *
     * @return self
     */
    public function setRiscoMercado(RiscoMercado $riscoMercado)
    {
        $this->riscoMercado = $riscoMercado;

        return $this;
    }
}
