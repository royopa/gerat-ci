<?php
/*
 * This file is part of the Gerat package.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */

//namespace Gerat\classes;

class ComposicaoAtivosCredito
{
/*    
    $titulo        - é o título da tabela/div
    $composicao    - o registro do banco de dados com os ativos
    $vr_pl         - o pl do fundo
    $maior_resgate - o maior resgate ocorrido nos últimos 3 anos
    $pl_stress     - $vr_pl - $maior_resgate;
    $produto_dia['VR_RESG_PROJ'] 
*/

    /**
     * @var Matricula
     */
    private $_matricula;

    /**
     * @var string
     */
    private $_nome;

    /**
     * @var string
     */
    private $_ocupacao;

    /**
     * @var Funcao
     */
    private $_funcao;

    /**
     * @var Equipe
     */
    private $_equipe;

    /**
     * @var Lotacao
     */
    private $_lotacaoAdministrativa;

    /**
     * @var Lotacao
     */
    private $_lotacaoFisica;

    /**
     * @var DateTime
     */
    private $_dataNascimento;

    /**
     * Empregado::getMatricula()
     *
     * @param void
     *
     * @return Matricula
     */
    public function getMatricula()
    {
        return $this->_matricula;
    }

    /**
     * Empregado::getNome()
     *
     * @param void
     *
     * @return string
     */
    public function getNome()
    {
        return $this->_nome;
    }

    /**
     * Empregado::getOcupacao()
     *
     * @param void
     *
     * @return string
     */
    public function getOcupacao()
    {
        return $this->_ocupacao;
    }

    /**
     * Empregado::getFuncao()
     *
     * @param void
     *
     * @return Funcao
     */
    public function getFuncao()
    {
        return $this->_funcao;
    }

    /**
     * Empregado::getEquipe()
     *
     * @param void
     *
     * @return Equipe
     */
    public function getEquipe()
    {
        return $this->_equipe;
    }

    /**
     * Empregado::getLotacaoAdministrativa()
     *
     * @param void
     *
     * @return Lotacao
     */
    public function getLotacaoAdministrativa()
    {
        return $this->_lotacaoAdministrativa;
    }

    /**
     * Empregado::getLotacaoFisica()
     *
     * @param void
     *
     * @return Lotacao
     */
    public function getLotacaoFisica()
    {
        return $this->_lotacaoFisica;
    }

    /**
     * Empregado::getDataNascimento()
     *
     * @param void
     *
     * @return DateTime
     */
    public function getDataNascimento()
    {
        return $this->_dataNascimento;
    }

    /**
     * Empregado::setMatricula()
     *
     * @param Matricula
     *
     * @return Empregado
     */
    public function setMatricula(Matricula $matricula)
    {
        $this->_matricula = $matricula;;

        return $this;
    }

    /**
     * Empregado::setNome()
     *
     * @param string
     *
     * @return Empregado
     */
    public function setNome($nome)
    {
        $this->_nome = (string) $nome;;

        return $this;
    }

    /**
     * Empregado::setOcupacao()
     *
     * @param string
     *
     * @return Empregado
     */
    public function setOcupacao($ocupacao)
    {
        $this->_ocupacao = (string) $ocupacao;;

        return $this;
    }

    /**
     * Empregado::setFuncao()
     *
     * @param Funcao
     *
     * @return Empregado
     */
    public function setFuncao(Funcao $funcao)
    {
        $this->_funcao = $funcao;;

        return $this;
    }

    /**
     * Empregado::setEquipe()
     *
     * @param Equipe
     *
     * @return Empregado
     */
    public function setEquipe(Equipe $equipe)
    {
        $this->_equipe = $equipe;;

        return $this;
    }

    /**
     * Empregado::setLotacaoAdministrativa()
     *
     * @param Lotacao
     *
     * @return Empregado
     */
    public function setLotacaoAdministrativa(Lotacao $lotacaoAdministrativa)
    {
        $this->_lotacaoAdministrativa = $lotacaoAdministrativa;;

        return $this;
    }

    /**
     * Empregado::setLotacaoFisica()
     *
     * @param Lotacao
     *
     * @return Empregado
     */
    public function setLotacaoFisica(Lotacao $lotacaoFisica)
    {
        $this->_lotacaoFisica = $lotacaoFisica;;

        return $this;
    }

    /**
     * Empregado::setDataNascimento()
     *
     * @param DateTime
     *
     * @return Empregado
     */
    public function setDataNascimento(DateTime $dataNascimento)
    {
        $this->_dataNascimento = $dataNascimento;;

        return $this;
    }

    public function toArray()
    {
        //var_dump($this->getEquipe()->getNome());
        $dados = array(
            'matricula' => $this->getMatricula()->getCodigo(),
            'matricula_digito' => $this->getMatricula()->getDigito(),
            'nome'      => $this->getNome(),
            'ocupacao'  => $this->getOcupacao(),
            'lotacao_administrativa_codigo' => $this->getLotacaoAdministrativa()->getCodigo(),
            'lotacao_administrativa_nome'   => $this->getLotacaoAdministrativa()->getNome(),
            'lotacao_fisica_codigo' => $this->getLotacaoFisica()->getCodigo(),
            'lotacao_fisica_nome'   => $this->getLotacaoFisica()->getNome(),
            'equipe_codigo' => $this->getEquipe()->getCodigo(),
            'equipe_nome'   => $this->getEquipe()->getNome(),
        );

        if ( $this->getDataNascimento() instanceof DateTime ) {
            $dados['data_nascimento'] = $this->getDataNascimento()->format('Y-m-d');
        } else {
            $dados['data_nascimento'] = null;
        }

        if ( $this->getFuncao()->getCodigo() ) {
            $dados['funcao_codigo'] = $this->getFuncao()->getCodigo();
            $dados['funcao_nome']   = $this->getFuncao()->getNome();
        } else {
            $dados['funcao'] = null;
        }



        //var_dump(htmlspecialchars($this->getEquipe()->getNome()));
//        var_dump($dados['equipe']);exit;

        return $dados;
    }
}
