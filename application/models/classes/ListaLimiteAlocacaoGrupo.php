<?php
/**
* ListaLimiteAlocacaoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* IF, IF_DPGE, NãoIF, FIDC
*
*/

/**
 * ListaLimiteAlocacaoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaLimiteAlocacaoGrupo extends ArrayIterator
{
    /**
     * @var FundoDiario
     * O fundo diário
     */
    private $fundoDia;

    /**
     * @var LimiteIfGrupo
     * Grupo de IFs e seus ratings e limites e valores alocados
     */
    private $limiteIfGrupo;

    /**
     * @var LimiteIfDpgeGrupo
     * Grupo de IF DPGE e seus ratings e limites e valores alocados
     */
    private $limiteIfDpgeGrupo;

    /**
     * @var LimiteNaoIfGrupo
     * Grupo de Não IFs e seus ratings e limites e valores alocados
     */
    private $limiteNaoIfGrupo;

    /**
     * @var LimiteFidcGrupo
     * Grupo de IF FIDC e seus ratings e limites e valores alocados
     */
    private $limiteFidcGrupo;

    public function __construct
        (FundoDiario $fundoDia = null)
    {
        //var_dump($fundoDia);
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
            $this->preencheDados($fundoDia);
        }
    }

    /**
     * Busca as informações no banco e preenche o objeto.
     *
     * @param FundoDia $fundoDia the fundo dia
     *
     * @return self
     */
    public function preencheDados(FundoDiario $fundoDia)
    {
        //$this->append(new LimiteIfGrupo($fundoDia));
        $this->setLimiteIfGrupo(new LimiteIfGrupo($fundoDia));
        //$this->append(new LimiteIfDpgeGrupo($fundoDia));
        //$this->append(new LimiteNaoIfGrupo($fundoDia));
        $this->setLimiteNaoIfGrupo(new LimiteNaoIfGrupo($fundoDia));
        //$this->append(new LimiteFidcGrupo($fundoDia));

        $this->setLimiteFidcGrupo(new LimiteFidcGrupo($fundoDia));

        $this->setLimiteIfDpgeGrupo(new LimiteIfDpgeGrupo($fundoDia));
        //var_dump($this);
        return $this;
    }

    /**
     * Gets the O fundo diário.
     *
     * @return FundoDiario
     */
    public function getFundoDia()
    {
        return $this->fundoDia;
    }

    /**
     * Sets the O fundo diário.
     *
     * @param FundoDiario $fundoDia the fundo dia
     *
     * @return self
     */
    public function setFundoDia(FundoDiario $fundoDia)
    {
        $this->fundoDia = $fundoDia;

        return $this;
    }

    /**
     * Gets the Grupo de IFs e seus ratings e limites e valores alocados.
     *
     * @return LimiteIfGrupo
     */
    public function getLimiteIfGrupo()
    {
        return $this->limiteIfGrupo;
    }

    /**
     * Sets the Grupo de IFs e seus ratings e limites e valores alocados.
     *
     * @param LimiteIfGrupo $limiteIfGrupo the limite if grupo
     *
     * @return self
     */
    public function setLimiteIfGrupo(LimiteIfGrupo $limiteIfGrupo)
    {
        $this->limiteIfGrupo = $limiteIfGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de IF DPGE e seus ratings e limites e valores alocados.
     *
     * @return LimiteIfDpgeGrupo
     */
    public function getLimiteIfDpgeGrupo()
    {
        return $this->limiteIfDpgeGrupo;
    }

    /**
     * Sets the Grupo de IF DPGE e seus ratings e limites e valores alocados.
     *
     * @param LimiteIfDpgeGrupo $limiteIfDpgeGrupo the limite if dpge grupo
     *
     * @return self
     */
    public function setLimiteIfDpgeGrupo(LimiteIfDpgeGrupo $limiteIfDpgeGrupo)
    {
        $this->limiteIfDpgeGrupo = $limiteIfDpgeGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Não IFs e seus ratings e limites e valores alocados.
     *
     * @return LimiteNaoIfGrupo
     */
    public function getLimiteNaoIfGrupo()
    {
        return $this->limiteNaoIfGrupo;
    }

    /**
     * Sets the Grupo de Não IFs e seus ratings e limites e valores alocados.
     *
     * @param LimiteNaoIfGrupo $limiteNaoIfGrupo the limite nao if grupo
     *
     * @return self
     */
    public function setLimiteNaoIfGrupo(LimiteNaoIfGrupo $limiteNaoIfGrupo)
    {
        $this->limiteNaoIfGrupo = $limiteNaoIfGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de IF FIDC e seus ratings e limites e valores alocados.
     *
     * @return LimiteFidcGrupo
     */
    public function getLimiteFidcGrupo()
    {
        return $this->limiteFidcGrupo;
    }

    /**
     * Sets the Grupo de IF FIDC e seus ratings e limites e valores alocados.
     *
     * @param LimiteFidcGrupo $limiteFidcGrupo the limite fidc grupo
     *
     * @return self
     */
    public function setLimiteFidcGrupo(LimiteFidcGrupo $limiteFidcGrupo)
    {
        $this->limiteFidcGrupo = $limiteFidcGrupo;

        return $this;
    }
}
