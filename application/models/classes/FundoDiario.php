<?php
/*
 * This file is part of the Gerat package.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */

//namespace Gerat\classes;

class FundoDiario
{
    /**
     * @var Fundo
     * O fundo ao qual as informações diárias pertencem
     */
    private $fundo;

    /**
     * @var DateTime
     * Data do arquivo XML processado na rotina
     */
    private $dataReferencia;

    /**
     * @var DateTime
     * Data de processamento da rotina
     */
    private $dataAtualizacao;

    /**
     * @var float
     * Patrimônio líquido do fundo
     */
    private $patrimonioLiquido;

    /**
     * @var float
     * Patrimônio líquido stress
     */
    private $patrimonioLiquidoStress;

    /**
     * @var float
     * Valor da cota
     */
    private $valorCota;

    /**
     * @var float
     * Valor que foi aplicado no fundo
     */
    private $valorAplicadoD0;

    /**
     * @var float
     * Valor que foi resgatado no fundo
     */
    private $valorResgatadoD0;

    /**
     * @var float
     * Resgate projetado
     */
    private $resgateProjetado;

    /**
     * @var TermometroLiquidez
     * O termometro de risco de liquidez - CR245
     */
    private $termometroLiquidez;

    /**
     * @var float
     * O valor do maior resgate ocorrido nos últimos 3 anos
     */
    private $maiorResgateUltimos3Anos;

    /**
     * @var float
     * Valor que está em garantia
     */
    private $valorEmGarantia;

    /**
     * @var float
     * Valor liquidez informada para a CVM diariamente
     */
    private $valorLiquidez;

    /**
     * @var float
     * Valor liquidez informada calculada para conferência
     */
    private $valorLiquidezConferencia;

    /**
     * @var ListaLimiteAlocacaoGrupo
     * Lista os limites de alocação dos ativos do fundo por tipo
     */
    private $listaLimiteAlocacaoGrupo;

    public function __construct
        (Fundo $fundo = null, DateTime $dataAtualizacao = null)
    {
        if ($fundo instanceof Fundo) {
            $this->fundo           = $fundo;
            $this->dataAtualizacao = $dataAtualizacao;
            $this->processa();
        }
    }

    /**
     * Gets the Lista os limites de alocação dos ativos do fundo por tipo.
     *
     * @return ListaLimiteAlocacaoGrupo
     */
    public function getListaLimiteAlocacaoGrupo()
    {
        return $this->listaLimiteAlocacaoGrupo;
    }

    /**
     * Sets the Lista os limites de alocação dos ativos do fundo por tipo.
     *
     * @param ListaLimiteAlocacaoGrupo $listaLimiteAlocacaoGrupo the lista limite alocacao grupo
     *
     * @return self
     */
    public function setListaLimiteAlocacaoGrupo(ListaLimiteAlocacaoGrupo $listaLimiteAlocacaoGrupo)
    {
        $this->listaLimiteAlocacaoGrupo = $listaLimiteAlocacaoGrupo;

        return $this;
    }

    /**
     * Sets the Patrimônio líquido stress.
     *
     * @param float $patrimonioLiquidoStress the patrimonio liquido stress
     *
     * @return self
     */
    public function setPatrimonioLiquidoStress($patrimonioLiquidoStress)
    {
        $this->patrimonioLiquidoStress = (float) $patrimonioLiquidoStress;

        return $this;
    }

    /**
     * Gets the Valor da cota.
     *
     * @return float
     */
    public function getValorCota()
    {
        return (float) $this->valorCota;
    }

    /**
     * FundoDiario::getValorLiquidez()
     *
     * @param void
     *
     * @return float
     */
    public function getValorLiquidez()
    {
        return (float) $this->valorLiquidez;
    }

    /**
     * Sets the valor liquidez
     *
     * @param float $valorLiquidez the valor liquidez diário
     *
     * @return self
     */
    public function setValorLiquidez($valorLiquidez)
    {
        $this->valorLiquidez = (float) $valorLiquidez;

        return $this;
    }

    /**
     * Sets the Valor da cota.
     *
     * @param float $valorCota the valor cota
     *
     * @return self
     */
    public function setValorCota($valorCota)
    {
        $this->valorCota = (float) $valorCota;

        return $this;
    }

    /**
     * Gets the Valor que foi aplicado no fundo.
     *
     * @return float
     */
    public function getValorAplicadoD0()
    {
        return (float) $this->valorAplicadoD0;
    }

    /**
     * Sets the Valor que foi aplicado no fundo.
     *
     * @param float $valorAplicadoD0 the valor aplicado d0
     *
     * @return self
     */
    public function setValorAplicadoD0($valorAplicadoD0)
    {
        $this->valorAplicadoD0 = (float) $valorAplicadoD0;

        return $this;
    }

    /**
     * Gets the Valor que foi resgatado no fundo.
     *
     * @return float
     */
    public function getValorResgatadoD0()
    {
        return (float) $this->valorResgatadoD0;
    }

    /**
     * Sets the Valor que foi resgatado no fundo.
     *
     * @param float $valorResgatadoD0 the valor resgatado d0
     *
     * @return self
     */
    public function setValorResgatadoD0($valorResgatadoD0)
    {
        $this->valorResgatadoD0 = (float) $valorResgatadoD0;

        return $this;
    }

    /**
     * Gets the Valor que está em garantia.
     *
     * @return float
     */
    public function getValorEmGarantia()
    {
        return (float) $this->valorEmGarantia;
    }

    /**
     * Sets the Valor que está em garantia.
     *
     * @param float $valorEmGarantia the valor em garantia
     *
     * @return self
     */
    public function setValorEmGarantia($valorEmGarantia)
    {
        $this->valorEmGarantia = (float) $valorEmGarantia;

        return $this;
    }

    /**
     * FundoDiario::getFundo()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getFundo()
    {
        return $this->fundo;
    }

    /**
     * FundoDiario::getDataReferencia()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getDataReferencia()
    {
        return $this->dataReferencia;
    }

    /**
     * FundoDiario::getDataAtualizacao()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getDataAtualizacao()
    {
        return $this->dataAtualizacao;
    }

    /**
     * Fundo::getPatrimonioLiquido()
     *
     * @param void
     *
     * @return float
     */
    public function getPatrimonioLiquido()
    {
        return (float) $this->patrimonioLiquido;
    }

    /**
     * Fundo::getPatrimonioLiquidoStress()
     *
     * @param void
     *
     * @return float
     */
    public function getPatrimonioLiquidoStress()
    {
        return (float) ($this->patrimonioLiquido - $this->maiorResgateUltimos3Anos);
    }

    /**
     * Fundo::getResgateProjetado()
     *
     * @param void
     *
     * @return float
     */
    public function getResgateProjetado()
    {
        return $this->resgateProjetado;
    }

    /**
     * Fundo::getMaiorResgateUltimos3Anos()
     *
     * @param void
     *
     * @return float
     */
    public function getMaiorResgateUltimos3Anos()
    {
        return (float) $this->maiorResgateUltimos3Anos;
    }



    /**
     * Fundo::setFundo()
     *
     * @param Fundo
     *
     * @return Fundo
     */
    public function setFundo(Fundo $fundo)
    {
        $this->fundo = $fundo;

        return $this;
    }

    /**
     * FundoDiario::setDataReferencia()
     *
     * @param DateTime
     *
     * @return FundoDiario
     */
    public function setDataReferencia(DateTime $dataReferencia)
    {
        $this->dataReferencia = new DateTime($dataReferencia);

        return $this;
    }

    /**
     * FundoDiario::setDataAtualizacao()
     *
     * @param DateTime
     *
     * @return FundoDiario
     */
    public function setDataAtualizacao(DateTime $dataAtualizacao)
    {
        $this->dataAtualizacao = new DateTime($dataAtualizacao);

        return $this;
    }

    /**
     * Fundo::setPatrimonioLiquido()
     *
     * @param float
     *
     * @return FundoDiario
     */
    public function setPatrimonioLiquido($patrimonioLiquido)
    {
        $this->patrimonioLiquido = (float) $patrimonioLiquido;

        return $this;
    }

    /**
     * Fundo::setResgateProjetado()
     *
     * @param float
     *
     * @return FundoDiario
     */
    public function setResgateProjetado($resgateProjetado)
    {
        $this->resgateProjetado = (float) $resgateProjetado;

        return $this;
    }

    /**
     * Fundo::setMaiorResgateUltimos3Anos()
     *
     * @param float
     *
     * @return FundoDiario
     */
    public function setMaiorResgateUltimos3Anos($maiorResgateUltimos3Anos)
    {
        $this->maiorResgateUltimos3Anos = (float) $maiorResgateUltimos3Anos;

        return $this;
    }

    /**
     * Fundo::processa()
     * Faz a busca no banco de dados e preenche o objeto
     *
     * @return FundoDiario
     */
    public function processa()
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
                SELECT
                    Produto_Dia.CO_PRD,
                    Produto_Dia.DT_ATU,
                    Produto_Dia.DT_REF,
                    VR_ATI,
                    VR_PL,
                    VR_COTA,
                    VR_APL_D0,
                    VR_RES_D0,
                    DH_CAPT_LIQ_D0,
                    VR_CAPT_LIQ_D0,
                    VR_PL_D0,
                    VR_ALAVANC,
                    VR_ALAVANC_2,
                    IC_CHECADO,
                    VR_RESG_PROJ,
                    VR_RESG_MAX_3A,
                    PC_RESG_MAX_3A,
                    DT_RESG_MAX_3A,
                    VR_ALUG,
                    VR_EM_GAR,
                    VR_ARB_V,
                    VR_ARB_C,
                    VR_TOP_COTISTA,
                    NO_XML,
                    DT_COTIZ,
                    VR_SALDO_PERM_ALTA,
                    VR_A_EMIT,
                    VR_A_RESG,
                    VR_DURATION,
                    VR_RET_DIA,
                    VR_ISG,
                    NO_MAPS_METODO_VAR,
                    VR_MAPS_NIV_CONF,
                    NU_MAPS_HORIZ,
                    VR_MAPS_LAMBDA,
                    NO_MAPS_CURRENCY,
                    NO_MAPS_VAR_TITLE,
                    VR_MAPS_COTAS,
                    VR_MAPS_PL_INFO,
                    VR_MAPS_PL_CALC,
                    VR_MAPS_MTM,
                    VR_MAPS_VAR_POL,
                    VR_MAPS_VAR_1D_95,
                    VR_MAPS_VAR_1D_95_STRESS,
                    VR_MAPS_VAR_21D_95,
                    VR_MAPS_VAR_1D_99,
                    VR_MAPS_VAR_15D_99,
                    VR_MAPS_VAR_21D_99,
                    NO_MAPS_BENCH,
                    NU_MAPS_MULT,
                    NO_MAPS_IS_NORMALIZED,
                    VR_MAPS_DURATION,
                    VR_MAPS_DURATION_MOD,
                    CO_MAX_FATOR,
                    VR_MAX_FATOR,
                    CO_MAX_ATIVO,
                    VR_MAX_ATIVO,
                    VR_MAX_ATIVO2,
                    Perfil_CVM_Liq.LIQ,
                    Perfil_CVM_Liq_Conferencia.LIQ AS LIQ_CONF
                FROM
                    Produto_Dia
                LEFT JOIN
                    Perfil_CVM_Liq
                ON 
                    Produto_Dia.CO_PRD = Perfil_CVM_Liq.CO_PRD AND
                    Produto_Dia.DT_REF = Perfil_CVM_Liq.DT_REF
                LEFT JOIN
                    Perfil_CVM_Liq_Conferencia
                ON 
                    Produto_Dia.CO_PRD = Perfil_CVM_Liq_Conferencia.CO_PRD AND
                    Produto_Dia.DT_REF = Perfil_CVM_Liq_Conferencia.DT_REF
                WHERE
                    Produto_Dia.CO_PRD = :cnpj AND
                    Produto_Dia.DT_ATU = :dataAtualizacao
            ";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(
                'cnpj',
                $this->getFundo()->getCnpj(),
                'integer'
                );
            $stmt->bindValue(
                'dataAtualizacao',
                $this->getDataAtualizacao(),
                'datetime'
                );
            $stmt->execute();

            while ($fundoDiaDb = $stmt->fetch()) {
                $this->dataReferencia           = new DateTime($fundoDiaDb['DT_REF']);
                $this->dataAtualizacao          = new DateTime($fundoDiaDb['DT_ATU']);
                $this->patrimonioLiquido        = (float) ($fundoDiaDb['VR_PL']);
                $this->valorCota                = (float) ($fundoDiaDb['VR_COTA']);
                $this->valorAplicadoD0          = (float) ($fundoDiaDb['VR_APL_D0']);
                $this->valorResgatadoD0         = (float) ($fundoDiaDb['VR_RES_D0']);
                $this->resgateProjetado         = (float) ($fundoDiaDb['VR_RESG_PROJ']);
                $this->maiorResgateUltimos3Anos = (float) ($fundoDiaDb['VR_RESG_MAX_3A']);
                $this->valorEmGarantia          = (float) ($fundoDiaDb['VR_EM_GAR']);
                $this->valorLiquidez            = (float) ($fundoDiaDb['LIQ']);
                $this->valorLiquidezConferencia = (float) ($fundoDiaDb['LIQ_CONF']);
            }
        
        return $this;
    }

    /**
     * Fundo::processa()
     * Faz a busca no banco de dados e preenche o objeto
     *
     * @return FundoDiario
     */
    public function processaDependencias()
    {
        /**
         * Preenche todas as listas e dependências que dependem de fudno diario
         */
        $this->listaLimiteAlocacaoGrupo = new ListaLimiteAlocacaoGrupo($this);
       
        return $this;
    }

    /**
     * FundoDiario::setValorLiquidezCalculado()
     * Preenche o valor líquido calculado
     *
     * @return FundoDiario
     */
    public function setValorLiquidezCalculado($valorLiquido)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        if ($this->getValorLiquidezConferencia() == 0) {
            
            $conn->insert('Perfil_CVM_Liq_Conferencia', array(
                'LIQ'    => (float) $valorLiquido,
                'CO_PRD' => $this->getFundo()->getCnpj(),
                'DT_REF' => $this->getDataReferencia()->format('Y-m-d')
                )
            );

        }
       
        return $this;
    }

    /**
     * Gets the O termometro de risco de liquidez - CR245.
     *
     * @return TermometroLiquidez
     */
    public function getTermometroLiquidez()
    {
        return $this->termometroLiquidez;
    }
    
    /**
     * Sets the O termometro de risco de liquidez - CR245.
     *
     * @param TermometroLiquidez $termometroLiquidez the termometro liquidez
     *
     * @return self
     */
    public function setTermometroLiquidez(TermometroLiquidez $termometroLiquidez)
    {
        $this->termometroLiquidez = $termometroLiquidez;

        return $this;
    }

    /**
     * Gets the Valor liquidez informada calculada para conferência.
     *
     * @return float
     */
    public function getValorLiquidezConferencia()
    {
        if ($this->valorLiquidezConferencia == 0) {
            $this->valorLiquidezConferencia == null;
        }
        return $this->valorLiquidezConferencia;
    }
    
    /**
     * Sets the Valor liquidez informada calculada para conferência.
     *
     * @param float $valorLiquidezConferencia the valor liquidez conferencia
     *
     * @return self
     */
    public function setValorLiquidezConferencia($valorLiquidezConferencia)
    {
        $this->valorLiquidezConferencia = $valorLiquidezConferencia;

        return $this;
    }


    /**
     * FundoDiario::removeValorLiquidezCalculado()
     * Remove o valor líquido calculado
     *
     * @return FundoDiario
     */
    public function removeValorLiquidezCalculado()
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();
            
        $conn->delete('Perfil_CVM_Liq_Conferencia', array(
            'CO_PRD' => $this->getFundo()->getCnpj(),
            'DT_REF' => $this->getDataReferencia()->format('Y-m-d')
            )
        );
       
        return $this;
    }
}
