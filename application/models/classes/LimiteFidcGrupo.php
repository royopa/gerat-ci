<?php
/**
* LimiteFidcGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * LimiteFidcGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteFidcGrupo extends LimiteAlocacaoGrupo
{
    public function __construct
        (FundoDiario $fundoDia = null)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->setNome('FIDC');
            $this->fundoDia = $fundoDia;
            $this->fetchAll($fundoDia);
        }
    }
}
