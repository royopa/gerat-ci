<?php
/**
* DesempenhoTipoAtivo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoTipoAtivo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */ 
class DesempenhoTipoAtivo
{
    /**
     * @var int
     * O código do ativo
     */
    private $codigo;

    /**
     * @var string
     * O nome do ativo
     */
    private $nome;

    /**
     * @var float
     * O valor do resultado financeiro
     */
    private $valor;

    /**
     * @var float
     * O percentual da participação do ativo sobre a rentabilidade
     */
    private $percentual;

    /**
     * Gets the O nome do ativo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }
    
    /**
     * Sets the O nome do ativo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the Tipo do ativo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
    /**
     * Sets the O Tipo do ativo.
     *
     * @param string $tipo the Tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Gets the O valor do resultado financeiro.
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }
    
    /**
     * Sets the O valor do resultado financeiro.
     *
     * @param float $valor the valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Gets the O percentual da participação do ativo sobre a rentabilidade.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }
    
    /**
     * Sets the O percentual da participação do ativo sobre a rentabilidade.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * DesempenhoTipoAtivo::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoTipoAtivo
     */
    public function create($row)    
    {
        if (count($row) > 0) {
            $this->setCodigo(utf8_encode($row['co_mtp_ati']));
            $this->setNome(utf8_encode($row['no_tp_ati']));
            //se for Compromiss. Público - Tipo = Títulos Públicos
            if ($this->getNome() == 'Compromiss. Público') {
                $this->setTipo('Títulos Públicos');
            } else {
                $this->setTipo(utf8_encode($row['no_mtp_ati']));
            }
            $this->setValor((float) $row['vr_result']);
            $this->setPercentual((float) $row['pc_ret_tp_ati_s_bench']);
        }
        
        return $this;
    }


    /**
     * Gets the O código do ativo.
     *
     * @return int
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    
    /**
     * Sets the O código do ativo.
     *
     * @param int $codigo the codigo
     *
     * @return self
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }
}
