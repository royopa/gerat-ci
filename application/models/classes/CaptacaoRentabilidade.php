<?php
/**
* Captação Rentabilidade File Doc Comment
*
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
* GraficosSirat Class
*
* @category Classe
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/

//namespace Gerat\classes;

class CaptacaoRentabilidade
{
    /**
     * @var DateTime
     * Data de referência dos dados de captação e rentabilidade
     */
    private $dataReferencia;

    /**
     * @var float
     * O valor aplicado no dia
     */
    private $valorAplicado;

    /**
     * @var float
     * O valor resgatado no dia
     */
    private $valorResgatado;

    /**
     * @var float
     * Percentual de retorno/rentabilidade no dia
     */
    private $rentabilidadeDiaria;

    /**
     * @var float
     * A captação líquida no dia - valor aplicado - valor resgatado
     */
    private $captacaoLiquida;


    /**
     * Gets the Data de referência dos dados de captação e rentabilidade.
     *
     * @return DateTime
     */
    public function getDataReferencia()
    {
        return $this->dataReferencia;
    }

    /**
     * Gets the Percentual de retorno/rentabilidade no dia.
     *
     * @return float
     */
    public function getRentabilidadeDiaria()
    {
        return $this->rentabilidadeDiaria;
    }

    /**
     * Sets the Percentual de retorno/rentabilidade no dia.
     *
     * @param float $rentabilidade the rentabilidade
     *
     * @return self
     */
    public function setRentabilidadeDiaria($rentabilidadeDiaria)
    {
        $this->rentabilidadeDiaria = $rentabilidadeDiaria;

        return $this;
    }

    /**
     * Sets the Data de referência dos dados de captação e rentabilidade.
     *
     * @param DateTime $dataReferencia the data referencia
     *
     * @return self
     */
    public function setDataReferencia(DateTime $dataReferencia)
    {
        $this->dataReferencia = $dataReferencia;

        return $this;
    }

    /**
     * Gets the O valor aplicado no dia.
     *
     * @return float
     */
    public function getValorAplicado()
    {
        return $this->valorAplicado;
    }

    /**
     * Sets the O valor aplicado no dia.
     *
     * @param float $valorAplicado the valor aplicado
     *
     * @return self
     */
    public function setValorAplicado($valorAplicado)
    {
        $this->valorAplicado = $valorAplicado;

        return $this;
    }

    /**
     * Gets the O valor resgatado no dia.
     *
     * @return float
     */
    public function getValorResgatado()
    {
        return $this->valorResgatado;
    }

    /**
     * Sets the O valor resgatado no dia.
     *
     * @param float $valorResgatado the valor resgatado
     *
     * @return self
     */
    public function setValorResgatado($valorResgatado)
    {
        $this->valorResgatado = $valorResgatado;

        return $this;
    }

    /**
     * Gets the A captação líquida no dia - valor aplicado - valor resgatado.
     *
     * @return float
     */
    public function getCaptacaoLiquida()
    {
        return $this->captacaoLiquida;
    }

    /**
     * Sets the A captação líquida no dia - valor aplicado - valor resgatado.
     *
     * @param float $captacaoLiquida the captacao liquida
     *
     * @return self
     */
    public function setCaptacaoLiquida($captacaoLiquida)
    {
        $this->captacaoLiquida = $captacaoLiquida;

        return $this;
    }

    /**
     * CaptacaoRentabilidade::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return CaptacaoRentabilidade
     */
    public function create($row)
    {
        if (count($row) > 0) {
            $this->setDataReferencia(new DateTime($row['DT_REF']));
            $this->setValorAplicado((float) $row['VR_APL']);
            $this->setValorResgatado((float) $row['VR_RESG']);
            $this->setCaptacaoLiquida(
                $this->getValorAplicado() - $this->getValorResgatado()
            );
            $this->setRentabilidadeDiaria((float) $row['VR_RET_DIA']);
        }

        return $this;
    }
}
