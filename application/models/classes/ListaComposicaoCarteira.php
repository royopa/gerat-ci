<?php
/**
* ListaComposicaoCarteira File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * ListaComposicaoCarteira Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaComposicaoCarteira extends ListaComposicao
{
    public function __construct
        (FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario = $fundoDiario;
            $this->fetchAll($fundoDiario);
            $this->setSerieDados(new SerieDados($this));
            $this->montaGrafico();
        }
    }

    /**
     * ListaComposicaoCarteira::fetchAll()
     *
     * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return ListaComposicaoCarteira
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupoDb
            = $model->getComposicaoCarteira(
                $fundoDiario->getFundo()->getCnpj(),
                $fundoDiario->getDataAtualizacao()->format('Y-m-d')
            );

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {
                $composicao = new Composicao();
                $dados = array();
                $dados['nome']       = $row['NO_MTP_ATI'];
                $dados['valor']      = $row['VR_MERC'];
                //adiciona a composicao no objeto lista
                $this->append($composicao->create($fundoDiario, $dados));
            }
        }

        return $this;
    }

    /**
     * ListaComposicaoCarteira::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $this
                    ->getChartWithLegend(
                        $this->getSerieDados()->getSerieArray(),
                        'pie_chart_composicao_carteira',
                        null,
                        'Composição Carteira (% PL)'
                    )
            );

        return $this;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getChartWithLegend
        ($dados_serie = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo             = $render_div;
        $chart->chart->plotBackgroundColor  = null;
        $chart->chart->plotBorderWidth      = null;
        $chart->chart->plotShadow           = false;
        $chart->title->text                 = $title;
   
        $chart->tooltip->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.point.name + ' : ' + Highcharts.numberFormat(this.y,2) + '%';
            }"
        );


        $chart->plotOptions->pie->dataLabels->enabled = false;
        $chart->plotOptions->pie->allowPointSelect = true;
        $chart->plotOptions->pie->cursor = "pointer";
        $chart->plotOptions->pie->showInLegend = true;

        $chart->plotOptions->pie->dataLabels->color = "#000000";
        $chart->plotOptions->pie->dataLabels->connectorColor = "#000000";

        $chart->plotOptions->pie->dataLabels->formatter = new HighchartJsExpr(
            "function() {
                return '' + this.point.name + ' : ' + Highcharts.numberFormat(this.y,2) + '%';
            }"
        );

        $chart->series[] = array(
          'type' => "pie",
          'name' => $name,
          'data' => $dados_serie);    

        return $chart;
    }
}
