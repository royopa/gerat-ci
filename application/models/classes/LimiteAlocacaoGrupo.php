<?php
/**
* LimiteAlocacaoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * LimiteAlocacaoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteAlocacaoGrupo extends ArrayIterator
{
    /**
     * @var float
     * A soma total dos valores alocados
     */
    private $valorTotal;

    /**
     * @var float
     * A soma total dos percentuais em relação ao PL dos valores alocados
     */
    private $percentualTotal;

    /**
     * @var string
     * Nome do tipo de ativo de crédito
     */
    private $nome;

    /**
     * @var SerieDadosLimiteAlocacao
     * A série de dados que será usada para geração do gráfico
     */
    private $serieDadosLimiteAlocacao;

    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the Nome do tipo de ativo de crédito.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Gets the A soma total dos valores alocados.
     *
     * @return float
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * Sets the A soma total dos valores alocados.
     *
     * @param float $valorTotal the valor total
     *
     * @return self
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;

        return $this;
    }

    /**
     * Gets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @return float
     */
    public function getPercentualTotal()
    {
        return $this->percentualTotal;
    }

    /**
     * Sets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @param float $percentualTotal the percentual total
     *
     * @return self
     */
    public function setPercentualTotal($percentualTotal)
    {
        $this->percentualTotal = $percentualTotal;

        return $this;
    }

    /**
     * Gets the A série de dados que será usada para geração do gráfico.
     *
     * @return SerieDadosLimiteAlocacao
     */
    public function getSerieDadosLimiteAlocacao()
    {
        return $this->serieDadosLimiteAlocacao;
    }

    /**
     * Sets the A série de dados que será usada para geração do gráfico.
     *
     * @param SerieDadosLimiteAlocacao $serieDadosLimiteAlocacao the serie dados limite alocacao
     *
     * @return self
     */
    public function setSerieDadosLimiteAlocacao(SerieDadosLimiteAlocacao $serieDadosLimiteAlocacao)
    {
        $this->serieDadosLimiteAlocacao = $serieDadosLimiteAlocacao;

        return $this;
    }

    /**
     * Sets the Nome do tipo de ativo de crédito.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    public function __construct
        (FundoDiario $fundoDia = null)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
            $this->fetchAll($fundoDia);
        }
    }

    /**
     * LimiteAlocacaoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return LimiteAlocacaoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDia = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupoDb
            = $model->getLimiteAlocacao(
                $fundoDia->getFundo()->getCnpj(),
                $fundoDia->getDataAtualizacao()->format('Y-m-d'),
                $this->nome
            );

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {

                $limiteAlocacao = new limiteAlocacao();
                $this->append($limiteAlocacao->create($fundoDia, $row));
            }
        }

        return $this;
    }

    /**
     * LimiteAlocacaoGrupo::preencheTotalizadores()
     *
     * @return LimiteAlocacaoGrupo
     *
     * Preenche os totalizadores de valor e percentual total de alocação
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $limite) {
            $this->valorTotal
                = $this->valorTotal + $limite->getValorAlocado();
            
            $this->percentualTotal
                = $this->percentualTotal + $limite->getPercentualAlocado();
        }
        return $this;
    }
}
