<?php
/**
* ListaCaptacaoRentabilidade File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * ListaCaptacaoRentabilidade Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

class ListaCaptacaoRentabilidade extends ArrayIterator
{
    /**
     * @var string
     * Quantidade de dias para retorno dos dados de captação e rentabilidade
     */
    private $quantidade;

    /**
     * @var string
     * A série de dados usada para geração do gráfico
     */
    private $serieDados;

    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the Quantidade de dias para retorno dos dados de captação e rentabilidade.
     *
     * @return string
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * Gets the A série de dados usada para geração do gráfico.
     *
     * @return string
     */
    public function getSerieDados()
    {
        return $this->serieDados;
    }

    /**
     * Sets the A série de dados usada para geração do gráfico.
     *
     * @param string $serieDados the serie dados
     *
     * @return self
     */
    public function setSerieDados($serieDados)
    {
        $this->serieDados = $serieDados;

        return $this;
    }

    /**
     * Sets the Quantidade de dias para retorno dos dados de captação e rentabilidade.
     *
     * @param string $quantidade the quantidade
     *
     * @return self
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    public function __construct
        (FundoDiario $fundoDia = null)
    {
        //var_dump($fundoDia);
        if ($fundoDia instanceof FundoDiario) {
            $this->quantidade = 21;
            $this->fundoDia   = $fundoDia;
            $this->fetchAll($fundoDia);
            $this->montaSerieDados();
            $this->montaGrafico();
        }
    }

    /**
     * ListaCaptacaoRentabilidade::fetchAll()
     *
     * @return ListaCaptacaoRentabilidade
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll()
    {
        $model = new Produto_Model();

        $grupoDb
            = $model->getDadosCaptacaoRentabilidadeDiaria(
                $this->fundoDia->getFundo()->getCnpj(),
                $this->fundoDia->getDataAtualizacao()->format('Y-m-d'),
                $this->quantidade
            );

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {
                $captacaoRentabilidade = new CaptacaoRentabilidade();
                $this->append($captacaoRentabilidade->create($row));
            }
        }

        return $this;
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que 
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @param mixed[] $composicao O CNPJ do fundo/carteira
    * @param float   $vr_pl      O patrimônio líquido do fundo/carteira
    *
    * @return  mixed[] $dados_serie Um array com a composiçao da carteira
    */
    private function montaSerieDados()
    {
        $dados_serie = array();

        $datasRef          = array();
        $captacoesLiquidas = array();
        $retornosDia       = array();

        $this->serieDados = array();

        foreach ($this as $captacaoRentabilidade) {
            
            $datasRef[]
                = $captacaoRentabilidade
                    ->getDataReferencia()
                    ->format('d/m');
            
            $retornosDia[]
                = round($captacaoRentabilidade->getRentabilidadeDiaria(), 3);

            $captacoesLiquidas[] = $captacaoRentabilidade->getCaptacaoLiquida();
        }

        $this->serieDados[0] = array_reverse($datasRef);
        $this->serieDados[1] = array_reverse($captacoesLiquidas);
        $this->serieDados[2] = array_reverse($retornosDia);
    }

    /**
     * ListaCaptacaoRentabilidade::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $grafico
                    ->getCaptacaoRentabilidadeChart(
                        $this->getSerieDados(),
                        'chart_captacao_rentabilidade',
                        null,
                        'Captação x Rentabilidade'
                    )
            );
        return $this;
    }
}
