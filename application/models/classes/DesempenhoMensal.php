<?php
/**
* DesempenhoMensalGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoMensalGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class DesempenhoMensal
{
    /**
     * @var DateTime
     * Mês e ano de rentabilidades
     */
    private $data;

    /**
     * @var float
     * A rentabilidade do fundo no mês
     */
    private $rentabilidadeFundo;

    /**
     * @var float
     * A rentabilidade do benchmark no mês
     */
    private $rentabilidadeBenchmark;

    /**
     * @var float
     * O percentual das rentabilidades do fundo sobre o benchmark
     */
    private $percentual;

    /**
     * Gets the Mês e ano de rentabilidades.
     *
     * @return DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the Mês e ano de rentabilidades.
     *
     * @param DateTime $data the data
     *
     * @return self
     */
    public function setData(DateTime $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the A rentabilidade do fundo no mês.
     *
     * @return float
     */
    public function getRentabilidadeFundo()
    {
        return $this->rentabilidadeFundo;
    }

    /**
     * Sets the A rentabilidade do fundo no mês.
     *
     * @param float $rentabilidadeFundo the rentabilidade fundo
     *
     * @return self
     */
    public function setRentabilidadeFundo($rentabilidadeFundo)
    {
        $this->rentabilidadeFundo = $rentabilidadeFundo;

        return $this;
    }

    /**
     * Gets the A rentabilidade do benchmark no mês.
     *
     * @return float
     */
    public function getRentabilidadeBenchmark()
    {
        return $this->rentabilidadeBenchmark;
    }

    /**
     * Sets the A rentabilidade do benchmark no mês.
     *
     * @param float $rentabilidadeBenchmark the rentabilidade benchmark
     *
     * @return self
     */
    public function setRentabilidadeBenchmark($rentabilidadeBenchmark)
    {
        $this->rentabilidadeBenchmark = $rentabilidadeBenchmark;

        return $this;
    }

    /**
     * Gets the O percentual das rentabilidades do fundo sobre o benchmark.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual das rentabilidades do fundo sobre o benchmark.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * DesempenhoMensal::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoMensal
     */
    public function create($row, FundoDiario $fundoDiario = null)
    {
        if (count($row) > 0) {

            $this->setData(new DateTime($row['DT_REF']));
            $this->setRentabilidadeFundo(
                (float) round($row['VR_RET_MES'], 6)
                );
            //$this->setRentabilidadeFundo((float) $row['VR_LIM']);
            //$this->setPercentual(
                //$this->getRentabilidadeFundo() / $this->getRentabilidadeBenchmark()
                //);
            $benchmark = $fundoDiario->getFundo()->getBenchmark();

            $minDataReferenciaInicial
                = $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getMinDataReferenciaMesAnterior($this->getData());

            $maxDataReferenciaInicial
                = $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getMaxDataReferenciaMesAnterior($this->getData());

            $fatorFinal =
                $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getFator($this->getData());

            $fatorInicial =
                $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getFator($maxDataReferenciaInicial);

            $this->setRentabilidadeBenchmark(
                round((($fatorFinal / $fatorInicial) - 1) * 100, 6)
                );

            $this->setPercentual(
                round(( $this->getRentabilidadeFundo() / $this->getRentabilidadeBenchmark()) * 100, 2)
                );            

        }
        return $this;
    }
}
