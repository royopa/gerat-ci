<?php
/*
 * This file is part of the Gerat package.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */

//namespace Gerat\classes;

class Fundo
{
    /**
     * @var string
     * Nome do fundo
     */
    private $nome;

    /**
     * @var string
     * CNPJ do fundo
     */
    private $cnpj;

    /**
     * @var boolean
     * Indicador se o fundo está ativo ou não
     */
    private $isAtivo;

    /**
     * @var float
     * Percentual da taxa de administração do fundo
     */
    private $taxaAdministracao;

    /**
     * @var string
     * Nome do fundo no SISFIN
     */
    private $nomeSisfin;

    /**
     * @var Benchmark
     * o Benchmark do fundo
     */
    private $benchmark;

    /**
     * @var DateTime
     * Data de início do fundo
     */
    private $dataInicio;

    /**
     * @var int
     * Tipo do fundo
     */
    private $tipo;

    /**
     * @var int
     * Prazo de cotização do fundo em dias
     */
    private $prazoCotizacao;

    /**
     * @param string $co_prd O CNPJ do fundo
     */
    public function __construct($co_prd = 0)
    {
        if ((int) $co_prd > 0) {
            $this->cnpj = $co_prd;
            $this->processa();
        }
    }

    /**
     * Fundo::getNome()
     *
     * @param void
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Fundo::getCnpj()
     *
     * @param void
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Fundo::getIsAtivo()
     *
     * @param void
     *
     * @return boolean
     */
    public function getIsAtivo()
    {
        return abs($this->isAtivo * -1);
    }

    /**
     * Fundo::getTaxaAdministracao()
     *
     * @param void
     *
     * @return float
     */
    public function getTaxaAdministracao()
    {
        return $this->taxaAdministracao;
    }

    /**
     * Fundo::setNome()
     *
     * @param string
     *
     * @return Fundo
     */
    public function setNome($nome)
    {
        //$this->nome = utf8_encode($nome);
        $this->nome = $nome;

        return $this;
    }

    /**
     * Fundo::setCnpj()
     *
     * @param string
     *
     * @return Fundo
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Fundo::setIsAtivo()
     *
     * @param boolean
     *
     * @return Fundo
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = abs($isAtivo * -1);

        return $this;
    }

    /**
     * Fundo::setTaxaAdministracao()
     *
     * @param float
     *
     * @return Fundo
     */
    public function setTaxaAdministracao($taxaAdministracao)
    {
        $this->taxaAdministracao = $taxaAdministracao;

        return $this;
    }

    /**
     * Fundo::processa()
     * Faz a busca no banco de dados e preenche o objeto
     *
     * @return FundoDiario
     */
    public function processa()
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                Produto.CO_PRD,
                Produto.NO_PRD,
                Produto.CO_TIPO,
                Produto.IC_DESAT,
                Produto.NO_PRD_SISFIN,
                Produto.PC_TX_ADM,
                Produto.CO_BENCH,
                Produto.DT_INI_PRD,
                Produto.NU_COTIZ,
                BENCH.NO_BENCH
            FROM
                Produto
            LEFT JOIN 
                BENCH
            ON
                BENCH.CO_BENCH = Produto.CO_BENCH
            WHERE
                CO_PRD = :cnpj
            ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(
            'cnpj',
            $this->cnpj,
            'integer'
            );

        $stmt->execute();

        while ($fundoDb = $stmt->fetch()) {

            $this->cnpj = $fundoDb['CO_PRD'];
            $this->nome = $fundoDb['NO_PRD'];
            $this->tipo = $fundoDb['CO_TIPO'];
            $this->isAtivo = $fundoDb['IC_DESAT'];
            $this->setNomeSisfin = $fundoDb['NO_PRD_SISFIN'];
            $this->taxaAdministracao = round($fundoDb['PC_TX_ADM'], 2);
            $this->dataInicio = new DateTime($fundoDb['DT_INI_PRD']);
            $this->prazoCotizacao = (int) $fundoDb['NU_COTIZ'];

            $benchmark = new Benchmark();

            if (is_null($fundoDb['CO_BENCH'])) {
                $benchmark->setCodigo(37);
                $benchmark->setNome('CDI');
            } else {
                $benchmark->setCodigo((int) $fundoDb['CO_BENCH']);
                $benchmark->setNome($fundoDb['NO_BENCH']);
            }

            $this->benchmark = $benchmark;


            //CO_BENCH
            //NU_VAR_HORIZ
            //HR_VAR_BIV_CONF
            //VR_VAR_LAMBDA
        }

        return $this;
    }

    /**
     * Fundo::getBenchmark()
     *
     * @param void
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Fundo::setBenchmark()
     *
     * @param codigo
     *
     * @return Benchmark
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Gets the Nome do fundo no SISFIN.
     *
     * @return string
     */
    public function getNomeSisfin()
    {
        return $this->nomeSisfin;
    }
    
    /**
     * Sets the Nome do fundo no SISFIN.
     *
     * @param string $nomeSisfin the nome sisfin
     *
     * @return self
     */
    public function setNomeSisfin($nomeSisfin)
    {
        $this->nomeSisfin = $nomeSisfin;

        return $this;
    }

    /**
     * Gets the o Benchmark do fundo.
     *
     * @return Benchmark
     */
    public function getBenchmark()
    {
        return $this->benchmark;
    }
    
    /**
     * Gets the data de início do fundo.
     *
     * @return DateTime
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * Sets the data de início do fundo.
     *
     * @param DateTime $dataInicio A data de início do fundo
     *
     * @return self
     */
    public function setDataInicio(DateTime $dataInicio)
    {
        $this->dataInicio = $dataInicio;

        return $this;
    }

    /**
     * Sets the o Benchmark do fundo.
     *
     * @param Benchmark $benchmark the benchmark
     *
     * @return self
     */
    public function setBenchmark(Benchmark $benchmark)
    {
        $this->benchmark = $benchmark;

        return $this;
    }

    /**
     * Gets the Prazo de cotização do fundo em dias.
     *
     * @return int
     */
    public function getPrazoCotizacao()
    {
        return $this->prazoCotizacao;
    }
    
    /**
     * Sets the Prazo de cotização do fundo em dias.
     *
     * @param int $prazoCotizacao the prazo cotizacao
     *
     * @return self
     */
    public function setPrazoCotizacao($prazoCotizacao)
    {
        $this->prazoCotizacao = $prazoCotizacao;

        return $this;
    }

    /**
     * Gets the Tipo do fundo.
     *
     * @return int
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
    /**
     * Sets the Tipo do fundo.
     *
     * @param int $tipo the tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }    
}
