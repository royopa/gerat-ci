<?php
/**
* LhGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * LhGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LhGrupo extends AtivoCreditoGrupo
{
    public function __construct
        (FundoDiario $fundoDia = null)
    {
        //var_dump($fundoDia);
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
            $this->setNome('LH');
            $this->fetchAll($fundoDia);
        }
    }
}
