<?php
/**
* ListaAtivosCredito File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * ListaAtivosCredito Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License 
 * @link     http://www.caixa.gov.br
 *
 */
class ListaAtivosCredito extends ArrayIterator
{
    /**
     * @var FundoDia
     * O fundo diário
     */
    private $fundoDia;

    /**
     * @var DpgeGrupo
     * Grupo de DPGEs para o fundo diário
     */
    private $dpgeGrupo;

    /**
     * @var FidcGrupo
     * Grupo de FIDCs para o fundo diário
     */
    private $fidcGrupo;

    /**
     * @var LetraFinanceiraGrupo
     * Grupo de Letras Financeiras para o fundo diário
     */
    private $letraFinanceiraGrupo;

    /**
     * @var LetraHipotecariaGrupo
     * Grupo de Letras Hipotecárias para o fundo diário
     */
    private $letraHipotecariaGrupo;

    /**
     * @var OperacaoCompromissadaGrupo
     * Grupo de Operações Compromissadas para o fundo diário
     */
    private $operacaoCompromissadaGrupo;

    /**
     * @var LetraFinanceiraSubordinadaGrupo
     * Grupo de LFs SUB para o fundo diário
     */
    private $letraFinanceiraSubordinadaGrupo;

    /**
     * @var CdbSubordinadoGrupo
     * Grupo de CDBs SUB para o fundo diário
     */
    private $cdbSubordinadoGrupo;

    /**
     * @var DebenturesGrupo
     * Grupo de Debêntures para o fundo diário
     */
    private $debenturesGrupo;

    /**
     * @var CriGrupo
     * Grupo de CRIs para o fundo diário
     */
    private $criGrupo;

    /**
     * @var CdbGrupo
     * Grupo de Cdbs para o fundo diário
     */
    private $cdbGrupo;

    /**
     * @var CciGrupo
     * Grupo de Cci para o fundo diário
     */
    private $cciGrupo;
 
    /**
     * @var CcbGrupo
     * Grupo de CCBs para o fundo diário
     */
    private $ccbGrupo;

    /**
     * @var LhGrupo
     * Grupo de LHs para o fundo diário
     */
    private $lhGrupo;

    public function __construct(FundoDiario $fundoDia = null)
    {
        parent::__construct();

        if ($fundoDia instanceof FundoDiario) {
            /*
             * faça alguma coisa - preenche de acordo com os dados do banco
             */
        }   
    }

    /**
     * Gets the Grupo de DPGEs para o fundo diário.
     *
     * @return DpgeGrupo
     */
    public function getDpgeGrupo()
    {
        return $this->dpgeGrupo;
    }

    /**
     * Sets the Grupo de DPGEs para o fundo diário.
     *
     * @param DpgeGrupo $dpgeGrupo the dpge grupo
     *
     * @return self
     */
    public function setDpgeGrupo(DpgeGrupo $dpgeGrupo)
    {
        $this->dpgeGrupo = $dpgeGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de FIDCs para o fundo diário.
     *
     * @return FidcGrupo
     */
    public function getFidcGrupo()
    {
        return $this->fidcGrupo;
    }

    /**
     * Sets the Grupo de FIDCs para o fundo diário.
     *
     * @param FidcGrupo $fidcGrupo the fidc grupo
     *
     * @return self
     */
    public function setFidcGrupo(FidcGrupo $fidcGrupo)
    {
        $this->fidcGrupo = $fidcGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Letras Financeiras para o fundo diário.
     *
     * @return LetraFinanceiraGrupo
     */
    public function getLetraFinanceiraGrupo()
    {
        return $this->letraFinanceiraGrupo;
    }

    /**
     * Sets the Grupo de Letras Financeiras para o fundo diário.
     *
     * @param LetraFinanceiraGrupo $letraFinanceiraGrupo the letra financeira grupo
     *
     * @return self
     */
    public function setLetraFinanceiraGrupo(LetraFinanceiraGrupo $letraFinanceiraGrupo)
    {
        $this->letraFinanceiraGrupo = $letraFinanceiraGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Letras Hipotecárias para o fundo diário.
     *
     * @return LetraHipotecariaGrupo
     */
    public function getLetraHipotecariaGrupo()
    {
        return $this->letraHipotecariaGrupo;
    }

    /**
     * Sets the Grupo de Letras Hipotecárias para o fundo diário.
     *
     * @param LetraHipotecariaGrupo $letraHipotecariaGrupo the letra hipotecaria grupo
     *
     * @return self
     */
    public function setLetraHipotecariaGrupo(LetraHipotecariaGrupo $letraHipotecariaGrupo)
    {
        $this->letraHipotecariaGrupo = $letraHipotecariaGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Operações Compromissadas para o fundo diário.
     *
     * @return OperacaoCompromissadaGrupo
     */
    public function getOperacaoCompromissadaGrupo()
    {
        return $this->operacaoCompromissadaGrupo;
    }

    /**
     * Sets the Grupo de Operações Compromissadas para o fundo diário.
     *
     * @param OperacaoCompromissadaGrupo $operacaoCompromissadaGrupo the operacao compromissada grupo
     *
     * @return self
     */
    public function setOperacaoCompromissadaGrupo(OperacaoCompromissadaGrupo $operacaoCompromissadaGrupo)
    {
        $this->operacaoCompromissadaGrupo = $operacaoCompromissadaGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de LFs SUB para o fundo diário.
     *
     * @return LetraFinanceiraSubordinadaGrupo
     */
    public function getLetraFinanceiraSubordinadaGrupo()
    {
        return $this->letraFinanceiraSubordinadaGrupo;
    }

    /**
     * Sets the Grupo de LFs SUB para o fundo diário.
     *
     * @param LetraFinanceiraSubordinadaGrupo $letraFinanceiraSubordinadaGrupo the letra financeira subordinada grupo
     *
     * @return self
     */
    public function setLetraFinanceiraSubordinadaGrupo(LetraFinanceiraSubordinadaGrupo $letraFinanceiraSubordinadaGrupo)
    {
        $this->letraFinanceiraSubordinadaGrupo = $letraFinanceiraSubordinadaGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de CDBs SUB para o fundo diário.
     *
     * @return CdbSubordinadoGrupo
     */
    public function getCdbSubordinadoGrupo()
    {
        return $this->cdbSubordinadoGrupo;
    }

    /**
     * Sets the Grupo de CDBs SUB para o fundo diário.
     *
     * @param CdbSubordinadoGrupo $cdbSubordinadoGrupo the cdb subordinado grupo
     *
     * @return self
     */
    public function setCdbSubordinadoGrupo(CdbSubordinadoGrupo $cdbSubordinadoGrupo)
    {
        $this->cdbSubordinadoGrupo = $cdbSubordinadoGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Debêntures para o fundo diário.
     *
     * @return DebenturesGrupo
     */
    public function getDebenturesGrupo()
    {
        return $this->debenturesGrupo;
    }

    /**
     * Sets the Grupo de Debêntures para o fundo diário.
     *
     * @param DebenturesGrupo $debenturesGrupo the debentures grupo
     *
     * @return self
     */
    public function setDebenturesGrupo(DebenturesGrupo $debenturesGrupo)
    {
        $this->debenturesGrupo = $debenturesGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de CRIs para o fundo diário.
     *
     * @return CriGrupo
     */
    public function getCriGrupo()
    {
        return $this->criGrupo;
    }

    /**
     * Sets the Grupo de CRIs para o fundo diário.
     *
     * @param CriGrupo $criGrupo the cri grupo
     *
     * @return self
     */
    public function setCriGrupo(CriGrupo $criGrupo)
    {
        $this->criGrupo = $criGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Cdbs para o fundo diário.
     *
     * @return CdbGrupo
     */
    public function getCdbGrupo()
    {
        return $this->cdbGrupo;
    }

    /**
     * Sets the Grupo de Cdbs para o fundo diário.
     *
     * @param CdbGrupo $cdbGrupo the cdb grupo
     *
     * @return self
     */
    public function setCdbGrupo(CdbGrupo $cdbGrupo)
    {
        $this->cdbGrupo = $cdbGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de Cci para o fundo diário.
     *
     * @return CciGrupo
     */
    public function getCciGrupo()
    {
        return $this->cciGrupo;
    }

    /**
     * Sets the Grupo de Cci para o fundo diário.
     *
     * @param CciGrupo $cciGrupo the cci grupo
     *
     * @return self
     */
    public function setCciGrupo(CciGrupo $cciGrupo)
    {
        $this->cciGrupo = $cciGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de CCBs para o fundo diário.
     *
     * @return CcbGrupo
     */
    public function getCcbGrupo()
    {
        return $this->ccbGrupo;
    }

    /**
     * Sets the Grupo de CCBs para o fundo diário.
     *
     * @param CcbGrupo $ccbGrupo the ccb grupo
     *
     * @return self
     */
    public function setCcbGrupo(CcbGrupo $ccbGrupo)
    {
        $this->ccbGrupo = $ccbGrupo;

        return $this;
    }

    /**
     * Gets the Grupo de LHs para o fundo diário.
     *
     * @return LhGrupo
     */
    public function getLhGrupo()
    {
        return $this->lhGrupo;
    }

    /**
     * Sets the Grupo de LHs para o fundo diário.
     *
     * @param LhGrupo $lhGrupo the lh grupo
     *
     * @return self
     */
    public function setLhGrupo(LhGrupo $lhGrupo)
    {
        $this->lhGrupo = $lhGrupo;

        return $this;
    }

    /**
     * Gets the O fundo diário.
     *
     * @return FundoDia
     */
    public function getFundoDia()
    {
        return $this->fundoDia;
    }

    /**
     * Sets the O fundo diário.
     *
     * @param FundoDia $fundoDia the fundo dia
     *
     * @return self
     */
    public function setFundoDia(FundoDia $fundoDia)
    {
        $this->fundoDia = $fundoDia;

        return $this;
    }

    /**
     * Sets the O fundo diário.
     *
     * @param FundoDia $fundoDia the fundo dia
     *
     * @return self
     */
    public function preencheAtivos(FundoDia $fundoDia)
    {
        //preenche as DPGEs que o fundo possui
        $this->append(new DpgeGrupo($fundoDia));
        //preenche os FIDCs que o fundo possui
        $this->append(new FidcGrupo($fundoDia));
        //preenche as Letras Financeiras que o fundo possui
        $this->append(new LetraFinanceiraGrupo($fundoDia));
        //preenche os CCBs que o fundo possui
        $this->append(new CcbGrupo($fundoDia));
        //preenche os CCIs que o fundo possui
        $this->append(new CciGrupo($fundoDia));
        //preenche os CDBs que o fundo possui
        $this->append(new CdbGrupo($fundoDia));
        //preenche os CDBs Subordinados que o fundo possui
        $this->append(new CdbSubordinadoGrupo($fundoDia));
        //preenche as CRIs que o fundo possui
        $this->append(new CriGrupo($fundoDia));
        //preenche as Debêntures que o fundo possui
        $this->append(new DebentureGrupo($fundoDia));
        //preenche as Letra Financeira Subordinada que o fundo possui
        $this->append(new LetraFinanceiraSubordinadaGrupo($fundoDia));
        //preenche as Letras Hipotecárias que o fundo possui
        $this->append(new LetraHipotecariaGrupo($fundoDia));
        //preenche as LHs que o fundo possui
        $this->append(new LhGrupo($fundoDia));
        //preenche as Operações Compromissadas que o fundo possui
        $this->append(new OperacaoCompromissadaGrupo($fundoDia));
    }
}