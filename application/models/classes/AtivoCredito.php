<?php
/**
* AtivoCredito File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * AtivoCredito Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class AtivoCredito
{
    /**
     * @var float
     * O percentual de limite de alocação
     */
    private $percentualLimite;

    /**
     * @var float
     * O percentual alocado
     */
    private $percentualAlocado;

    /**
     * @var float
     * O percentual do valor de mercado do ativo em relação ao PL do fundo
     */
    private $percentualValorMercado;

    /**
     * @var float
     * O percentual do cupom
     */
    private $percentualCupom;

    /**
     * @var float
     * O percentual do índice - ex: o CDC paga 105 do CDI
     */
    private $percentualIndice;

    /**
     * @var string
     * O código do ativo/ISIN
     */
    private $isin;

    /**
     * @var string
     * O nome do ativo
     */
    private $nome;

    /**
     * @var boolean
     * Indica se permite recompra
     */
    private $recompra;

    /**
     * @var DateTime
     * Data de emissão do ativo
     */
    private $dataEmissao;

    /**
     * @var DateTime
     * Data de emissão do ativo
     */
    private $dataVencimento;

    /**
     * @var float
     * O valor de mercado do ativo
     */
    private $valorMercado;

    /**
     * @var float
     * O valor limite de alocação do ativo
     */
    private $valorLimite;

    /**
     * @var float
     * O valor alocado do ativo no fundo
     */
    private $valorAlocado;

    /**
     * @var int
     * O código de alerta
     */
    private $isinAlerta;

    /**
     * @var Emissor
     * O emissor do ativo
     */
    private $emissor;

    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * AtivoCredito::getNome()
     *
     * @param void
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;

    }

    /**
     * Gets the O percentual de limite de alocação.
     *
     * @return float
     */
    public function getPercentualLimite()
    {
        return $this->percentualLimite * 100;
    }

    /**
     * Gets the o percentual alocado sobre o percentual limite.
     *
     * @return float
     */
    public function getPercentualAlocadoSobreLimite()
    {
        return $this->percentualAlocado / $this->percentualLimite * 100;
    }

    /**
     * Sets the O percentual de limite de alocação.
     *
     * @param float $percentualLimite the percentual limite
     *
     * @return self
     */
    public function setPercentualLimite($percentualLimite)
    {
        $this->percentualLimite = $percentualLimite;

        return $this;
    }

    /**
     * Gets the O percentual alocado.
     *
     * @return float
     */
    public function getPercentualAlocado()
    {
        return $this->percentualAlocado * 100;
    }

    /**
     * Sets the O percentual alocado.
     *
     * @param float $percentualAlocado the percentual alocado
     *
     * @return self
     */
    public function setPercentualAlocado($percentualAlocado)
    {
        $this->percentualAlocado = $percentualAlocado;

        return $this;
    }

    /**
     * Gets the O percentual do valor de mercado do ativo em relação ao PL do fundo.
     *
     * @return float
     */
    public function getPercentualValorMercado()
    {
        return $this->percentualValorMercado * 100;
    }

    /**
     * Gets the O percentual do valor de mercado do ativo em relação ao PL de stress do fundo.
     *
     * @return float
     */
    public function getPercentualLimiteStress()
    {
        return $this->percentualValorMercado * 100;
    }

    /**
     * Sets the O percentual do valor de mercado do ativo em relação ao PL do fundo.
     *
     * @param float $percentualValorMercado the percentual valor mercado
     *
     * @return self
     */
    public function setPercentualValorMercado($percentualValorMercado)
    {
        $this->percentualValorMercado = $percentualValorMercado;

        return $this;
    }

    /**
     * Gets the O percentual do cupom.
     *
     * @return float
     */
    public function getPercentualCupom()
    {
        return $this->percentualCupom;
    }

    /**
     * Sets the O percentual do cupom.
     *
     * @param float $percentualCupom the percentual cupom
     *
     * @return self
     */
    public function setPercentualCupom($percentualCupom)
    {
        $this->percentualCupom = $percentualCupom;

        return $this;
    }

    /**
     * Gets the O percentual do índice - ex: o CDC paga 105 do CDI.
     *
     * @return float
     */
    public function getPercentualIndice()
    {
        return $this->percentualIndice;
    }

    /**
     * Sets the O percentual do índice - ex: o CDC paga 105 do CDI.
     *
     * @param float $percentualIndice the percentual indice
     *
     * @return self
     */
    public function setPercentualIndice($percentualIndice)
    {
        $this->percentualIndice = $percentualIndice;

        return $this;
    }

    /**
     * Gets the Indica se permite recompra.
     *
     * @return boolean
     */
    public function getRecompra()
    {
        return $this->recompra;
    }

    /**
     * Sets the Indica se permite recompra.
     *
     * @param boolean $recompra the recompra
     *
     * @return self
     */
    public function setRecompra($recompra)
    {
        $this->recompra = $recompra;

        return $this;
    }

    /**
     * Gets the Data de emissão do ativo.
     *
     * @return DateTime
     */
    public function getDataEmissao()
    {
        return $this->dataEmissao;
    }

    /**
     * Sets the Data de emissão do ativo.
     *
     * @param DateTime $dataEmissao the data emissao
     *
     * @return self
     */
    public function setDataEmissao(DateTime $dataEmissao)
    {
        $this->dataEmissao = $dataEmissao;

        return $this;
    }

    /**
     * Gets the Data de emissão do ativo.
     *
     * @return DateTime
     */
    public function getDataVencimento()
    {
        return $this->dataVencimento;
    }

    /**
     * Sets the Data de emissão do ativo.
     *
     * @param DateTime $dataVencimento the data vencimento
     *
     * @return self
     */
    public function setDataVencimento(DateTime $dataVencimento)
    {
        $this->dataVencimento = $dataVencimento;

        return $this;
    }

    /**
     * Gets the O valor limite de alocação do ativo.
     *
     * @return float
     */
    public function getValorLimite()
    {
        return $this->valorLimite;
    }

    /**
     * Sets the O valor limite de alocação do ativo.
     *
     * @param $valorLimite the valor limite
     *
     * @return self
     */
    public function setValorLimite($valorLimite)
    {
        $this->valorLimite = (float) $valorLimite;

        return $this;
    }

    /**
     * Gets the O valor alocado do ativo no fundo.
     *
     * @return float
     */
    public function getValorAlocado()
    {
        return $this->valorAlocado;
    }

    /**
     * Sets the O valor alocado do ativo no fundo.
     *
     * @param $valorAlocado the valor alocado
     *
     * @return self
     */
    public function setValorAlocado($valorAlocado)
    {
        $this->valorAlocado = (float) $valorAlocado;

        return $this;
    }

    /**
     * Gets the O código de alerta.
     *
     * @return int
     */
    public function getCodigoAlerta()
    {
        return $this->codigoAlerta;
    }

    /**
     * Sets the O código de alerta.
     *
     * @param int $codigoAlerta the codigo alerta
     *
     * @return self
     */
    public function setCodigoAlerta($codigoAlerta)
    {
        $this->codigoAlerta = $codigoAlerta;

        return $this;
    }

    /**
     * AtivoCredito::getValorMercado()
     *
     * @param void
     *
     * @return float
     */
    public function getValorMercado()
    {
        return $this->valorMercado;
    }

    /**
     * AtivoCredito::getEmissor()
     *
     * @param void
     *
     * @return Emissor
     */
    public function getEmissor()
    {
        return $this->emissor;
    }

    /**
     * AtivoCredito::getFundoDiario()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * AtivoCredito::setFundoDiario()
     *
     * @param float
     *
     * @return AtivoCredito
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * AtivoCredito::setValorMercado()
     *
     * @param float
     *
     * @return AtivoCredito
     */
    public function setValorMercado($valorMercado)
    {
        $this->valorMercado = (float) $valorMercado;

        return $this;
    }

    /**
     * AtivoCredito::setNome()
     *
     * @param string
     *
     * @return AtivoCredito
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * AtivoCredito::setIsin()
     *
     * @param string
     *
     * @return AtivoCredito
     */
    public function setIsin($isin)
    {
        $this->nome = $isin;

        return $this;
    }

    /**
     * AtivoCredito::setEmissor()
     *
     * @param Emissor
     *
     * @return AtivoCredito
     */
    public function setEmissor(Emissor $emissor)
    {
        $this->emissor = $emissor;

        return $this;
    }

    public function toArray()
    {
        $dados = array(
            'nome'           => $this->getNome(),
            'valor_mercado'  => $this->getValorMercado(),
            'emissor_nome'   => $this->getEmissor()->getNome(),
            'emissor_rating' => $this->getEmissor()->getRating(),
            'emissor_limite' => $this->getEmissor()->getLimite(),
        );

        return $dados;
    }

    /**
     * AtivoCredito::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return AtivoCredito
     */
    public function create(FundoDiario $fundoDia, $row)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->setFundoDiario($fundoDia);
        }

        if (count($row) > 0) {

            $this->setFundoDiario($fundoDia);

            $this->setNome($row['NO_TP_EMISS']);
            $this->setValorMercado((float) $row['VR_MERC']);
            $this->setValorLimite((float) $row['VR_LIM']);
            $this->setValorAlocado((float) $row['VR_ALOC']);
            $this->setCodigoAlerta($row['CO_ALERTA']);
            $this->setRating($row['NO_RATING']);

            $this->setPercentualLimite((float) $row['PC_LIM']);

            $this->setPercentualAlocado((float) $row['PC_ALOC']);

            $this->setIsin($row['CO_ATI']);
            $this->setRecompra($row['IC_RECOMPRA']);
            $this->setDataEmissao(new DateTime($row['DT_EMISS']));
            $this->setDataVencimento(new DateTime($row['DT_VCTO']));

            $this->setPercentualValorMercado((float) $row['PC_MERC']);

            $this->setPercentualCupom((float) $row['PC_COUPOM']);

            $emissor = new Emissor();
            $emissor->setNome($row['NO_EMISS']);
            $emissor->setRating($row['NO_RATING']);
            $emissor->setLimite((float) $row['VR_LIM']);
            $this->emissor = $emissor;
        }

        return $this;
    }
}
