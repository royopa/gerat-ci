<?php
/**
* DpgeGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * DpgeGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class DpgeGrupo extends Grupo
{
    public function __construct
        (FundoDiario $fundoDia = null)
    {
        //var_dump($fundoDia);
        if ($fundoDia instanceof FundoDiario) {
            $this->nome     = 'Debêntures';
            $this->fundoDia = $fundoDia;
            $this->fetchAll($fundoDia);
        }
    }
    /**
     * DpgeGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return DpgeGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDia = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupo
            = $model->getAtivosCredito(
                $fundoDia->getFundo()->getCnpj(),
                $fundoDia->getDataAtualizacao()->format('Y-m-d'),
                'DPGE'
            );

        //var_dump($grupo);

        if (count($grupo) > 0) {

            foreach ($grupo as $row) {

                $ativoCredito = new AtivoCredito();

                //adiciona o ativo de crédito no objeto grupo
                $this->append($ativoCredito->create($fundoDia, $row));
            }
        }

        return $this;
    }
}
