<?php
/**
* CdbGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * CdbGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class CdbGrupo extends AtivoCreditoGrupo
{
    public function __construct
        (FundoDiario $fundoDia = null)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->fundoDia = $fundoDia;
            $this->setNome('CDB');
            parent::fetchAll($fundoDia);
        }
    }
}
