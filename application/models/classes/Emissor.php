<?php
/*
 * This file is part of the Gerat package.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */

//namespace Gerat\classes;

class Emissor
{

    /**
     * @var string
     * Nome do emissor
     */
    private $nome;

    /**
     * @var string
     * Rating do Emissor
     */
    private $rating;

    /**
     * @var float
     * O valor máximo que pode ser alocado do emissor no fundo
     */
    private $valorLimite;

    /**
     * @var float
     * O percentual máximo que pode ser alocado do emissor no fundo
     */
    private $percentualLimite;

    /**
     * Emissor::getNome()
     *
     * @param void
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Gets the O valor máximo que pode ser alocado do emissor no fundo.
     *
     * @return float
     */
    public function getValorLimite()
    {
        return $this->valorLimite;
    }

    /**
     * Sets the O valor máximo que pode ser alocado do emissor no fundo.
     *
     * @param float $valorLimite the valor limite
     *
     * @return self
     */
    public function setValorLimite($valorLimite)
    {
        $this->valorLimite = $valorLimite;

        return $this;
    }

    /**
     * Gets the O percentual máximo que pode ser alocado do emissor no fundo.
     *
     * @return float
     */
    public function getPercentualLimite()
    {
        return $this->percentualLimite;
    }

    /**
     * Sets the O percentual máximo que pode ser alocado do emissor no fundo.
     *
     * @param float $percentualLimite the percentual limite
     *
     * @return self
     */
    public function setPercentualLimite($percentualLimite)
    {
        $this->percentualLimite = $percentualLimite * 100;

        return $this;
    }

    /**
     * Emissor::getRating()
     *
     * @param void
     *
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Emissor::setNome()
     *
     * @param nome
     *
     * @return Emissor
     */
    public function setNome($nome)
    {
        $this->nome = utf8_encode($nome);

        return $this;
    }

    /**
     * Emissor::setRating()
     *
     * @param string
     *
     * @return Emissor
     */
    public function setRating($rating)
    {
        $this->rating = (string) $rating;

        return $this;
    }

    public function toArray()
    {
        $dados = array(
            'nome'   => $this->getNome(),
            'rating' => $this->getRating(),
        );

        return $dados;
    }
}
