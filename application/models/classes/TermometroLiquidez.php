<?php
/**
* TermometroLiquidez File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * TermometroLiquidez Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class TermometroLiquidez extends ArrayIterator
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var float
     * O volume projetado de resgate
     */
    private $volumeResgateProjetado;

    /**
     * @var float
     * O volume liquido
     */
    private $volumeLiquido;

    /**
     * @var float
     * RL1 - Conforme CR245 - 3.3.4
     */
    private $indicadorRl1;

    /**
     * @var float
     * RL2 - Conforme CR245 - 3.3.4
     */
    private $indicadorRl2;

    /**
     * @var float
     * RL3 - Conforme CR245 - 3.3.4
     */
    private $indicadorRl3;

    /**
     * @var float
     * RL4 - Conforme CR245 - 3.3.4
     */
    private $indicadorRl4;

    /**
     * @var float
     * O valor do maior resgate ocorrido nos últimos 3 anos
     */
    private $maiorResgateUltimos3Anos;

    public function __construct
        (FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario = $fundoDiario;
            
            $this->maiorResgateUltimos3Anos
                = $fundoDiario->getMaiorResgateUltimos3Anos();

            $this->fetchAll($fundoDiario);
        }
    }

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the O volume projetado de resgate.
     *
     * @return float
     */
    public function getVolumeResgateProjetado()
    {
        return $this->volumeResgateProjetado;
    }

    /**
     * Sets the O volume projetado de resgate.
     *
     * @param float $volumeResgateProjetado the volume resgate projetado
     *
     * @return self
     */
    public function setVolumeResgateProjetado($volumeResgateProjetado)
    {
        $this->volumeResgateProjetado = $volumeResgateProjetado;

        return $this;
    }

    /**
     * Gets the O volume liquido.
     *
     * @return float
     */
    public function getVolumeLiquido()
    {
        return $this->volumeLiquido;
    }

    /**
     * Sets the O volume liquido.
     *
     * @param float $volumeLiquido the volume liquido
     *
     * @return self
     */
    public function setVolumeLiquido($volumeLiquido)
    {
        $this->volumeLiquido = $volumeLiquido;

        return $this;
    }

    /**
     * Gets the RL1 - Conforme CR245 - 3.3.4
     *
     * @return float
     */
    public function getIndicadorRl1()
    {
        return round($this->indicadorRl1 * 100, 2);
    }

    /**
     * Sets the RL1 - Conforme CR245 - 3.3.4
     *
     * @param float $indicadorRl1 the indicador rl1
     *
     * @return self
     */
    public function setIndicadorRl1($indicadorRl1)
    {
        $this->indicadorRl1 = $indicadorRl1;

        return $this;
    }

    /**
     * Gets the RL2 - Conforme CR245 - 3.3.4
     *
     * @return float
     */
    public function getIndicadorRl2()
    {
        return $this->indicadorRl2;
    }

    /**
     * Sets the RL2 - Conforme CR245 - 3.3.4
     *
     * @param float $indicadorRl2 the indicador rl2
     *
     * @return self
     */
    public function setIndicadorRl2($indicadorRl2)
    {
        $this->indicadorRl2 = $indicadorRl2;

        return $this;
    }

    /**
     * Gets the RL3 - Conforme CR245 - 3.3.4
     *
     * @return float
     */
    public function getIndicadorRl3()
    {
        return $this->indicadorRl3;
    }

    /**
     * Sets the RL3 - Conforme CR245 - 3.3.4
     *
     * @param float $indicadorRl3 the indicador rl3
     *
     * @return self
     */
    public function setIndicadorRl3($indicadorRl3)
    {
        $this->indicadorRl3 = $indicadorRl3;

        return $this;
    }

    /**
     * Gets the RL4 - Conforme CR245 - 3.3.4
     *
     * @return float
     */
    public function getIndicadorRl4()
    {
        return $this->indicadorRl4;
    }

    /**
     * Sets the RL4 - Conforme CR245 - 3.3.4
     *
     * @param float $indicadorRl4 the indicador rl4
     *
     * @return self
     */
    public function setIndicadorRl4($indicadorRl4)
    {
        $this->indicadorRl4 = $indicadorRl4;

        return $this;
    }

    /**
     * Gets the O valor do maior resgate ocorrido nos últimos 3 anos.
     *
     * @return float
     */
    public function getMaiorResgateUltimos3Anos()
    {
        return $this->maiorResgateUltimos3Anos;
    }

    /**
     * Sets the O valor do maior resgate ocorrido nos últimos 3 anos.
     *
     * @param float $maiorResgateUltimos3Anos the maior resgate ultimos3 anos
     *
     * @return self
     */
    public function setMaiorResgateUltimos3Anos($maiorResgateUltimos3Anos)
    {
        $this->maiorResgateUltimos3Anos = $maiorResgateUltimos3Anos;

        return $this;
    }

    /**
     * TermometroLiquidez::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return TermometroLiquidez
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        //instancia o model para fazer acesso aos métodos de banco de dados
        $model = new Produto_Model();

        $grupoDb
            = $model->getRelatorioRiscoLiquidez(
                $fundoDiario->getFundo()->getCnpj(),
                $fundoDiario->getDataAtualizacao()->format('Y-m-d')
            );

        if (count($grupoDb) > 0) {

            foreach ($grupoDb as $row) {

                $this->volumeResgateProjetado = (float) $row['VR_RESG_PROJ'];
                $this->volumeLiquido          = (float) $row['VR_VOLUME_LIQ'];
                $this->indicadorRl1
                    = $this->volumeResgateProjetado / $this->volumeLiquido;
                //$this->indicadorRl2             = $row[''];
                //$this->indicadorRl3             = $row[''];
                //$this->indicadorRl4             = $row[''];
                $this->codigoAlerta             = $row['CO_ALERTA'];
                $this->mensagemAlerta           = utf8_encode($row['NO_MSG']);
            }
        }

        return $this;
    }
}
