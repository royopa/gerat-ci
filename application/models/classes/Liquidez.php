<?php
/**
* Liquidez File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * Liquidez Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class Liquidez extends ArrayIterator
{
    /**
     * @var string
     * O nome do quadro de liquidez (Caia, Derivativos, Titulos Privados, 
     * Titulos Públicos)
     */
    private $nome;

    /**
     * @var float
     * O volumeLiquido
     */
    private $volumeLiquido;

    /**
     * @var float
     * O valor de mercado
     */
    private $valorMercado;

    /**
     * @var float
     * O percentual de liquidez
     */
    private $percentualLiquidez;

    /**
     * Liquidez::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return Liquidez
     */
    public function create($row)
    {
        $this->setNome(utf8_encode($row['NO_MTP_ATI']));
        $this->setVolumeLiquido((float) $row['VR_VOLUME_1D']);
        $this->setValorMercado((float) $row['VR_MERC']);
        $this->setPercentualLiquidez($this->getVolumeLiquido() / $this->getValorMercado());
        
        return $this;
    }

    /**
     * Gets the nome do quadro de liquidez
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the nome do quadro de liquidez
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the volumeLiquido.
     *
     * @return float
     */
    public function getVolumeLiquido()
    {
        return $this->volumeLiquido;
    }

    /**
     * Sets the volumeLiquido.
     *
     * @param float $volumeLiquido the volume liquido
     *
     * @return self
     */
    public function setVolumeLiquido($volumeLiquido)
    {
        $this->volumeLiquido = $volumeLiquido;

        return $this;
    }

    /**
     * Gets the valor de mercado.
     *
     * @return float
     */
    public function getValorMercado()
    {
        return $this->valorMercado;
    }

    /**
     * Sets the valor de mercado.
     *
     * @param float $valorMercado the valor mercado
     *
     * @return self
     */
    public function setValorMercado($valorMercado)
    {
        $this->valorMercado = $valorMercado;

        return $this;
    }

    /**
     * Gets the percentual de liquidez.
     *
     * @return float
     */
    public function getPercentualLiquidez()
    {
        return $this->percentualLiquidez * 100;
    }

    /**
     * Sets the percentual de liquidez.
     *
     * @param float $percentualLiquidez the percentual liquidez
     *
     * @return self
     */
    public function setPercentualLiquidez($percentualLiquidez)
    {
        $this->percentualLiquidez = $percentualLiquidez;

        return $this;
    }    
}
