<?php
/**
* SerieDados File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * SerieDados Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class SerieDados
{
    /**
     * @var ListaComposicao
     * A lista de composição para gerar a série de dados
     */
    private $listaComposicao;

    /**
     * @var mixed[]
     * A lista de composição em formato array para ser usada no gráfico
     */
    private $serieArray;

    public function __construct
        (ListaComposicao $listaComposicao = null)
    {
        if ($listaComposicao instanceof ListaComposicao) {
            $this->listaComposicao = $listaComposicao;
            $this->montaArray();
        }
    }

    /**
     * Gets the A lista de composição para gerar a série de dados.
     *
     * @return ListaComposicao
     */
    public function getListaComposicao()
    {
        return $this->listaComposicao;
    }

    /**
     * Sets the A lista de composição para gerar a série de dados.
     *
     * @param ListaComposicao $listaComposicao the lista composicao
     *
     * @return self
     */
    public function setListaComposicao(ListaComposicao $listaComposicao)
    {
        $this->listaComposicao = $listaComposicao;

        return $this;
    }

    /**
     * Gets the A lista de composição em formato array para ser usada no gráfico.
     *
     * @return mixed[]
     */
    public function getSerieArray()
    {
        return $this->serieArray;
    }

    /**
     * Sets the A lista de composição em formato array para ser usada no gráfico.
     *
     * @param array $serieArray the serie array
     *
     * @return self
     */
    public function setSerieArray(array $serieArray)
    {
        $this->serieArray = $serieArray;

        return $this;
    }

    /**
     * Monta o array, get dados série que será usado no gráfico
     *
     * @return self
     */
    public function montaArray()
    {
        $dadosSerie     = array();
        $somaPercentual = 0;

        foreach ($this->listaComposicao as $composicao) {

            if ($this->listaComposicao instanceof ListaComposicaoCarteira) {
                //pega o primeiro elemento
                if ($this->listaComposicao->key() == 0) {

                }
            }

            $dados = array(
                0 => $composicao->getNome(),
                1 => round($composicao->getPercentual(), 2)
            );            

            $dadosSerie[]   = $dados;
        }

        $this->serieArray = $dadosSerie;

        return;
    }
}
