<?php
/**
* LimiteAlocacao File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* IF, IF_DPGE, NãoIF, FIDC
*
*/

/**
 * LimiteAlocacao Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteAlocacao
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var Emissor
     * O emissor do ativo
     */
    private $emissor;

    /**
     * @var float
     * O valor alocado
     */
    private $valorAlocado;

    /**
     * @var float
     * O valor de mercado
     */
    private $valorMercado;

    /**
     * @var float
     * O percentual alocado sobre o PL do fundo
     */
    private $percentualAlocado;

    /**
     * @var float
     * O percentual alocado sobre o percentual limite
     */
    private $percentualAlocadoLimite;

    /**
     * @var float
     * O percentual alocado sobre o limite de stress
     */
    private $percentualAlocadoLimiteStress;

    /**
     * @var string
     * O nome do limite (IF, DPGE, Não IF, FIDC)
     */
    private $nome;

    /**
     * @var int
     * O código de alerta
     */
    private $codigoAlerta;

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Gets the O valor de mercado.
     *
     * @return float
     */
    public function getValorMercado()
    {
        return $this->valorMercado;
    }

    /**
     * Sets the O valor de mercado.
     *
     * @param float $valorMercado the valor mercado
     *
     * @return self
     */
    public function setValorMercado($valorMercado)
    {
        $this->valorMercado = $valorMercado;

        return $this;
    }    

    /**
     * Gets the O código de alerta.
     *
     * @return int
     */
    public function getCodigoAlerta()
    {
        return $this->codigoAlerta;
    }

    /**
     * Sets the O código de alerta.
     *
     * @param int $codigoAlerta the codigo alerta
     *
     * @return self
     */
    public function setCodigoAlerta($codigoAlerta)
    {
        $this->codigoAlerta = $codigoAlerta;

        return $this;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the O emissor do ativo.
     *
     * @return Emissor
     */
    public function getEmissor()
    {
        return $this->emissor;
    }

    /**
     * Sets the O emissor do ativo.
     *
     * @param Emissor $emissor the emissor
     *
     * @return self
     */
    public function setEmissor(Emissor $emissor)
    {
        $this->emissor = $emissor;

        return $this;
    }

    /**
     * Gets the O valor alocado.
     *
     * @return float
     */
    public function getValorAlocado()
    {
        return $this->valorAlocado;
    }

    /**
     * Sets the O valor alocado.
     *
     * @param float $valorAlocado the valor alocado
     *
     * @return self
     */
    public function setValorAlocado($valorAlocado)
    {
        $this->valorAlocado = $valorAlocado;

        return $this;
    }

    /**
     * Gets the O percentual alocado sobre o PL do fundo.
     *
     * @return float
     */
    public function getPercentualAlocado()
    {
        return $this->percentualAlocado * 100;
    }

    /**
     * Sets the O percentual alocado sobre o PL do fundo.
     *
     * @param float $percentualAlocado the percentual alocado
     *
     * @return self
     */
    public function setPercentualAlocado($percentualAlocado)
    {
        $this->percentualAlocado = $percentualAlocado;

        return $this;
    }

    /**
     * Gets the O percentual alocado sobre o limite.
     *
     * @return float
     */
    public function getPercentualAlocadoLimite()
    {
        return $this->percentualAlocadoLimite * 100;
    }

    /**
     * Sets the O percentual alocado sobre o limite.
     *
     * @param float $percentualAlocadoLimite the percentual alocado limite
     *
     * @return self
     */
    public function setPercentualAlocadoLimite($percentualAlocadoLimite)
    {
        $this->percentualAlocadoLimite = $percentualAlocadoLimite;

        return $this;
    }

    /**
     * Gets the O percentual alocado sobre o limite de stress.
     *
     * @return float
     */
    public function getPercentualAlocadoLimiteStress()
    {
        return $this->percentualAlocadoLimiteStress * 100;
    }

    /**
     * Sets the O percentual alocado sobre o limite de stress.
     *
     * @param float $percentualAlocadoLimiteStress the percentual alocado limite stress
     *
     * @return self
     */
    public function setPercentualAlocadoLimiteStress($percentualAlocadoLimiteStress)
    {
        $this->percentualAlocadoLimiteStress = $percentualAlocadoLimiteStress;

        return $this;
    }

    /**
     * Gets the O nome do limite (IF, DPGE, Não IF, FIDC).
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the O nome do limite (IF, DPGE, Não IF, FIDC).
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * LimiteAlocacao::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return LimiteAlocacao
     */
    public function create(FundoDiario $fundoDia, $row)
    {
        if ($fundoDia instanceof FundoDiario) {
            $this->setFundoDiario($fundoDia);
        }

        if (count($row) > 0) {

            $this->setFundoDiario($fundoDia);

            $this->setValorAlocado((float) $row['VR_ALOC']);
            $this->setValorMercado((float) $row['VR_MERC']);
            $this->setPercentualAlocado((float) $row['PC_ALOC']);
            $this->setPercentualAlocadoLimite((float) $row['PC_ALOC_S_LIM']);
            $valorLimiteStress = (float) $row['PC_LIM'] * $fundoDia->getPatrimonioLiquidoStress();

            if ((float) $row['PC_LIM'] > 0) {
                $this->setPercentualAlocadoLimiteStress(
                    (float) $row['VR_ALOC'] / ($row['PC_LIM'] * $fundoDia->getPatrimonioLiquidoStress())
                    );
            }

            $this->setCodigoAlerta($row['CO_ALERTA']);
            $emissor = new Emissor();
            $emissor->setNome($row['NO_EMISS']);
            $emissor->setRating($row['NO_RATING']);
            $emissor->setValorLimite((float) $row['VR_LIM']);
            $emissor->setPercentualLimite((float) $row['PC_LIM']);
            $this->emissor = $emissor;
        }

        return $this;
    }
}
