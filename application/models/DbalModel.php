<?php
/**
* DbalModel File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DbalModel Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

//namespace Gerat\classes;

class DbalModel
{
    /**
     * @var Doctrine\DBAL\Connection
     * A conexão da base de dados via DBAL
     * libraries\Dbal.php
     */
    private $conn;

    /**
     * Gets the A conexão da base de dados via DBAL
     *
     * @return Doctrine\DBAL\Connection
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * Sets the A conexão da base de dados via DBAL
     *
     * @param Doctrine\DBAL\Connection $conn the conn
     *
     * @return self
     */
    public function setConn(Doctrine\DBAL\Connection $conn)
    {
        $this->conn = $conn;

        return $this;
    }

    public function __construct($conn = null)
    {
        $this->conn = $conn;
    }
}
