<?php
/**
* Produto_Model File Doc Comment
*
* @category CI_Model
* @package  Models
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Produto_Model_CI_Model
*
* @category Controllers
* @package  Controllers
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*/
class Produto_Model extends CI_Model
{
    /**
    * Consulta no banco de dados os fundos/carteiras
    *
    * Tabela Produto
    *
    * @param int $ic_desat Indicador se o fundo está ativo ou desativado
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getProdutos($ic_desat = 0)
    {
        $this->db->select('*');

        $this->db->from('Produto');

        if ($ic_desat == 0) {
            $this->db->where('IC_DESAT', 0);
        } else {
              $this->db->where('IC_DESAT', $ic_desat);
        }

        $query = $this->db->get();

        return $query->result();
    }

    /**
    * Consulta no banco de dados a composição carteira de crédito do fundo
    *
    * Tabela CR245002_RC_F_11
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getComposicaoCarteiraCredito($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
              SELECT
                    NO_TP_ATI,
                    SUM(VR_MERC) VR_MERC
                FROM
                    CR245002_RC_F_11
                WHERE
                    CO_PRD = '$co_prd' AND
                    DT_ATU = '$dt_atu'
                GROUP BY
                    NO_TP_ATI
                ORDER BY
                    VR_MERC DESC
              ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta no banco de dados a tabela 12
    *
    * Tabela CR245002_RL_F_12
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getLiquidezTabela12($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
              SELECT
                  DT_ATU,
                      DT_REF,
                      CO_PRD,
                      NO_MTP_ATI,
                      NO_TP_ATI,
                      CO_ATI,
                      NO_ATI,
                      NO_EMISS,
                      IC_RECOMPRA,
                      DT_VCTO,
                      IC_CENARIO,
                      VR_VOLUME_1D,
                      VR_VOLUME_1D_2,
                      VR_MERC,
                      NU_QTD_VOLUME_1D,
                      VR_PRECO,
                      VR_VOL_NEG_COTIZ
              FROM
                  CR245002_RL_F_12
                  WHERE
                  DT_ATU = '$dt_atu' AND
                  CO_PRD = '$co_prd' AND
                  IC_CENARIO = 'N'
              ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta no banco de dados o limite de alocaação de IF para o fundo
    *
    * Tabela V_CR245002_MONIT_RC_FUNDOS_EMISS
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getLimiteIF($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
            SELECT
                NO_EMISS,
                NO_RATING,
                VR_ALOC,
                PC_LIM,
                VR_LIM,
                VR_LIM_D0,
                PC_ALOC,
                PC_ALOC_D0,
                CASE WHEN VR_LIM IS NOT NULL THEN PC_ALOC_S_LIM END,
                CO_ALERTA,
                CASE WHEN VR_LIM_D0 IS NOT NULL THEN PC_ALOC_S_LIM_D0 END,
                CO_ALERTA_D0
            FROM
                V_CR245002_MONIT_RC_FUNDOS_EMISS
            WHERE
                DT_ATU='$dt_atu' AND
                CO_PRD='$co_prd' AND
                NO_TP_EMISS='IF'
            ORDER BY
                NO_RATING+'Z',
                NO_EMISS
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta no banco de dados o limite de liquidez para o fundo
    * Gera uma tabela temporária com as informações
    *
    * Procedure SP_CR245002_RL_F_12
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getLimiteLiquidezTabela($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
            EXEC
                SP_CR245002_RL_F_12
                '$co_prd',
                '$dt_atu'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta no banco de dados o limite de alocação para o fundo
    *
    * Tabela V_CR245002_MONIT_RC_FUNDOS_EMISS
    *
    * @param string $co_prd       O CNPJ do fundo
    * @param string $dt_atu       A data de atualização da base de dados
    * @param string $tipo_emissao Tipo de emissão
    *                                             IF Instituição Financeira
    *                                             INF Instituição não financeira
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getLimiteAlocacao
    ($co_prd = null, $dt_atu = null, $tipo_emissao = null)
    {
        $sql
            = "
            SELECT
                NO_EMISS,
                NO_RATING,
                VR_ALOC,
                PC_LIM,
                VR_LIM,
                VR_LIM_D0,
                PC_ALOC,
                PC_ALOC_D0,
                PC_ALOC_S_LIM,
                CO_ALERTA,
                PC_ALOC_S_LIM_D0,
                CO_ALERTA_D0,
                VR_MERC
            FROM
                V_CR245002_MONIT_RC_FUNDOS_EMISS
            WHERE
                DT_ATU='$dt_atu' AND
                CO_PRD='$co_prd' AND
                NO_TP_EMISS='$tipo_emissao'
            ORDER BY
                NO_RATING+'Z',
                PC_ALOC_S_LIM,
                NO_EMISS
            ";

        //echo $sql;
        return $this->_executaSql($sql);
    }

    /**
    * Função para ser usada como modelo
    *
    * Tabela Qualquer
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getModeloQuery($co_prd = null, $dt_atu = null)
    {
        $sql
            = "

            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta os valores de liquidez da tabela de backup
    *
    * Tabela CR245002_RL_F_12_BACKUP
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getLiquidezTabela12Backup($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
            SELECT
                DT_ATU,
                    DT_REF,
                    CO_PRD,
                    NO_MTP_ATI,
                    NO_TP_ATI,
                    CO_ATI,
                    NO_ATI,
                    NO_EMISS,
                    IC_RECOMPRA,
                    DT_VCTO,
                    IC_CENARIO,
                    VR_VOLUME_1D,
                    VR_VOLUME_1D_2,
                    VR_MERC,
                    NU_QTD_VOLUME_1D,
                    VR_PRECO,
                    VR_VOL_NEG_COTIZ
            FROM
                CR245002_RL_F_12_BACKUP
            WHERE
                DT_ATU = '$dt_atu' AND
                CO_PRD = '$co_prd' AND
                IC_CENARIO = 'N'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a composição da carteira do fundo
    *
    * Tabela CR245002_RL_11
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getComposicaoCarteira($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
            SELECT
                NO_MTP_ATI,
                SUM(VR_MERC) VR_MERC
            FROM
                CR245002_RL_11
            WHERE
                DT_ATU = '$dt_atu' AND
                CO_PRD = '$co_prd'
            GROUP BY
                NO_MTP_ATI
            ORDER BY
                VR_MERC
            DESC
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a data de atualização mais recente do banco de dados
    *
    * Tabela PRODUTO_DIA
    *
    * @return  datetime $row->DT_ATU A data de atualização
    */
    public function getMaxDtAtu()
    {
        $this->db->select_max('DT_ATU');
        $this->db->from('PRODUTO_DIA');
        $query = $this->db->get();

        foreach ($query->result() as $row) {
            return $row->DT_ATU;
        }

        return;
    }

    /**
    * Consulta os dados do fundo
    *
    * Tabela Produto
    *
    * @param string $co_prd O CNPJ do fundo
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getProduto($co_prd = null)
    {
        $query = $this->db->query(
            "
            SELECT
                *
            FROM
                Produto
            WHERE
                CO_PRD = '$co_prd'
            "
        );

        foreach ($query->result() as $row) {
            return $row;
        }

        return;
    }

    /**
    * Consulta o XML importado do fundo
    *
    * Tabela PRODUTO_DIA
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getProdutoDisa($co_prd = null, $dt_atu = null)
    {
        $this->db->select('*');

        $this->db->from('PRODUTO_DIA');

        $this->db->where('CO_PRD', $co_prd);
        $this->db->where('DT_ATU', $dt_atu);

        $query = $this->db->get();

        return $query->result();
    }

    /**
    * Consulta os dados do fundo usando sql comum
    *
    * Tabela Produto
    *
    * @param string $co_prd O CNPJ do fundo
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getProduto2($co_prd = null)
    {
        $sql
            = "
            SELECT
                CO_PRD,
                NO_PRD,
                NU_COTIZ
            FROM
                Produto
            WHERE CO_PRD = '$co_prd'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a posição completa do fundos - XML importado
    *
    * Tabela Produto_Posicao
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_ref A data de referência do arquivo XML (D-1)
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getPosicaoCompleta($co_prd = 0, $dt_ref = 0)
    {
        $sql
            = "
            SELECT
                DT_REF,
                CO_PRD,
                CO_ATI,
                DT_OP,
                CO_TP_OP,
                NU_QTD,
                VR_MERC_A,
                VR_MERC_P,
                CO_IDX_A,
                PC_IDX_A,
                CO_IDX_P,
                PC_IDX_P,
                DT_VCTO,
                VR_EM_GAR,
                PC_TX_ALUG,
                IC_COMPROM,
                DT_RET_COMPROM,
                CO_ATI_LAS,
                DT_VCTO_LAS,
                VR_PRINC_A,
                VR_PRINC_P,
                VR_PRINC_PROJ,
                PC_COUPOM,
                NU_PP,
                DT_VCTO_ALUG,
                PC_TX_A,
                PC_TX_P,
                CO_IDX_OP,
                PC_IDX_OP,
                PC_TX_OP,
                NO_SERIE,
                VR_CONT,
                VR_PU_COMPRA
            FROM
                Produto_Posicao
            WHERE
                DT_REF = '$dt_ref' AND
                CO_PRD = '$co_prd'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a posição do fundo agrupada por ativo
    *
    * Tabela Produto_Posicao
    *        ATIVO_DIA
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_ref A data de referência do arquivo XML (D-1)
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getPosicaoAgrupadaAtivo
    ($co_prd = 0, $dt_ref = 0, $dt_atu = 0)
    {
        $sql
            = "
            SELECT
                Produto_Posicao.CO_ATI,
                a.CO_TP_ATI,
                CO_TP_OP,
                NU_QTD,
                VR_MERC_A VR_MERC,
                VR_MERC_P,
                Produto_Posicao.VR_EM_GAR,
                Produto_Posicao.DT_VCTO,
                ad.DT_ATU, 
                ad.IC_LIQUIDO,
                a.CO_STP_ATI
            FROM
                Produto_Posicao
            LEFT JOIN ATIVO_DIA ad 
                ON ad.CO_ATI = Produto_Posicao.CO_ATI
            LEFT JOIN 
                ATIVO a ON a.CO_ATI = ad.CO_ATI
            WHERE
                DT_REF = '$dt_ref' AND
                CO_PRD = '$co_prd' AND
                Produto_Posicao.CO_ATI = 'OP_COMPROM00'
            UNION
            SELECT
                Produto_Posicao.CO_ATI,
                a.CO_TP_ATI,
                CO_TP_OP,
                SUM(Produto_Posicao.NU_QTD) NU_QTD,
                SUM(Produto_Posicao.VR_MERC_A) VR_MERC,
                SUM(Produto_Posicao.VR_MERC_P) VR_MERC_P,
                SUM(Produto_Posicao.VR_EM_GAR) AS VR_EM_GAR,
                Produto_Posicao.DT_VCTO,
                ad.DT_ATU,
                ad.IC_LIQUIDO,
                a.CO_STP_ATI
            FROM
                Produto_Posicao
            LEFT JOIN
                ATIVO_DIA ad
            ON
                ad.CO_ATI = Produto_Posicao.CO_ATI
            LEFT JOIN
                ATIVO a
            ON
                a.CO_ATI = ad.CO_ATI
            WHERE
                DT_REF = '$dt_ref' AND
                CO_PRD = '$co_prd' AND
                ad.DT_ATU = '$dt_atu'
            GROUP BY
                Produto_Posicao.CO_ATI,
                a.CO_TP_ATI,
                CO_TP_OP,
                Produto_Posicao.VR_MERC_P,
                Produto_Posicao.DT_VCTO,
                ad.DT_ATU,
                ad.IC_LIQUIDO,
                a.CO_STP_ATI
              ";

        //echo '</br></br></br></br></br></br>';
        //echo($sql);

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a posição do fundo agrupada por ativo
    *
    * Tabela Produto_Posicao
    *        ATIVO_TIPO
    *        ATIVO
    *
    * @param string $co_ati O ISIN do ativo
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getMtpAtivo($co_ati = 0)
    {
        $sql
            = "
            SELECT
                CO_MTP_ATI
            FROM
                ATIVO_TIPO at
            INNER JOIN
                ATIVO a
            ON at.CO_TP_ATI = a.CO_TP_ATI
            WHERE
                CO_ATI = '$co_ati'
              ";

        $mtp_ati = $this->_executaSql($sql);

        return $mtp_ati[0]["CO_MTP_ATI"];
    }

    /**
    * Consulta o no cod ativo
    *
    * Tabela ATIVO
    *
    * @param string $co_ati O ISIN do ativo
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getNoCodAtivo($co_ati = 0)
    {
        $sql
            = "
            SELECT
                NO_COD
            FROM
                ATIVO
            WHERE
                CO_ATI = '$co_ati'
              ";

        $mtp_ati = $this->_executaSql($sql);

        return $mtp_ati[0]["NO_COD"];
    }

    /**
    * Consulta a posição do caixa do fundo do dia solicitado
    *
    * Tabela Produto_Posicao
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_ref A data de referência do arquivo XML (D-1)
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getPosicaoCaixaDia($co_prd = 0, $dt_ref = 0)
    {
        $sql
            = "
            SELECT
                SUM(VR_MERC_A) VR_CAIXA
            FROM
                Produto_Posicao
            WHERE
                DT_REF = '$dt_ref' AND
                CO_PRD = '$co_prd' AND
                co_ati = 'CAIXA0000000'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a posição da liquidez do fundo do dia solicitado
    *
    * Tabela Produto_Posicao
    *        ATIVO_DIA
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_ref A data de referência do arquivo XML (D-1)
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getPosicaoLiquidez($co_prd = 0, $dt_ref = 0)
    {
        $dt_atu = proximoDiaUtil(add_date($dt_ref, 1), 'Y-m-d');

        $sql = "
                SELECT
                    Produto_Posicao.DT_REF,
                    Produto_Posicao.CO_PRD,
                    Produto_Posicao.CO_ATI,
                    Produto_Posicao.DT_OP,
                    Produto_Posicao.CO_TP_OP,
                    Produto_Posicao.NU_QTD,
                    Produto_Posicao.VR_MERC_A,
                    Produto_Posicao.VR_MERC_P,
                    Produto_Posicao.CO_IDX_A,
                    Produto_Posicao.PC_IDX_A,
                    Produto_Posicao.CO_IDX_P,
                    Produto_Posicao.PC_IDX_P,
                    Produto_Posicao.DT_VCTO,
                    Produto_Posicao.VR_EM_GAR,
                    Produto_Posicao.PC_TX_ALUG,
                    Produto_Posicao.IC_COMPROM,
                    Produto_Posicao.DT_RET_COMPROM,
                    Produto_Posicao.CO_ATI_LAS,
                    Produto_Posicao.DT_VCTO_LAS,
                    Produto_Posicao.VR_PRINC_A,
                    Produto_Posicao.VR_PRINC_P,
                    Produto_Posicao.VR_PRINC_PROJ,
                    Produto_Posicao.PC_COUPOM,
                    Produto_Posicao.NU_PP,
                    Produto_Posicao.DT_VCTO_ALUG,
                    Produto_Posicao.PC_TX_A,
                    Produto_Posicao.PC_TX_P,
                    Produto_Posicao.CO_IDX_OP,
                    Produto_Posicao.PC_IDX_OP,
                    Produto_Posicao.PC_TX_OP,
                    Produto_Posicao.NO_SERIE,
                    Produto_Posicao.VR_CONT,
                    Produto_Posicao.VR_PU_COMPRA,
                    ad.DT_ATU,
                    ad.IC_LIQUIDO
                FROM
                    Produto_Posicao
                INNER JOIN
                    ATIVO_DIA ad
                ON
                    ad.CO_ATI = Produto_Posicao.CO_ATI
                WHERE
                    DT_REF = '$dt_ref' AND
                    CO_PRD = '$co_prd' AND
                    ad.DT_ATU = '$dt_atu'
                ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a liquidez diária dos fundos (CMV)
    *
    * Tabela Perfil_CVM_Liq
    *
    * @param string $dt_ref A data de referência do arquivo XML (D-1)
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getPerfilCvmLiquidez($dt_ref = null)
    {
        if (is_null($dt_ref)) {
            return;
        }

        $sql
            = "
            SELECT
                p.ID,
                p.DT_REF,
                p.CO_PRD,
                p.NO_PRD,
                p.LIQ,
                p.EMISSOR,
                p.VR_PROV_C,
                p.VR_PROV_D,
                c.LIQ AS LIQ_CONF
            FROM
                Perfil_CVM_Liq p
            LEFT JOIN 
                Perfil_CVM_Liq_Conferencia c
            ON 
                p.DT_REF = c.DT_REF AND
                p.CO_PRD = c.CO_PRD
            WHERE
                p.DT_REF = '$dt_ref'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta as últimas negociações no mercado de Títulos Públicos
    *
    * Tabela MERCADO_TPUBLICO
    *        ATIVO
    *        ATIVO_TIPO
    *
    * @param string $dt_atu  A data de atualização da base de dados
    * @param string $co_ati  O ISIN do ativo
    * @param string $qtd_neg A quantidade de últimos negócios que serão exibidos
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function detalheNegocMercadoTitPub
    ($dt_atu = null, $co_ati = null, $qtd_neg = null)
    {
        if (is_null($dt_atu) || is_null($co_ati)) {
            return;
        }

        if ((int) $qtd_neg == 0) {
            $qtd_neg = '21';
        }

        $sql
            = "
            SELECT
                *
            FROM
                MERCADO_TPUBLICO X,
                ATIVO A
            WHERE
                X.NU_NEG_CONTR > 0 AND
                X.DT_VCTO = A.DT_VCTO AND
                X.NO_SIGL = A.NO_SIGL AND
                X.DT_REF BETWEEN (
                    SELECT
                        MIN(Y.DT_REF)
                    FROM (
                        SELECT DISTINCT TOP $qtd_neg
                            Z.DT_REF
                        FROM
                            MERCADO_TPUBLICO Z
                        WHERE
                            Z.DT_REF < '$dt_atu'
                        ORDER BY
                            Z.DT_REF DESC) Y) AND (
                    SELECT
                        MAX(Y.DT_REF)
                    FROM
                        MERCADO_TPUBLICO Y
                    WHERE
                        Y.DT_REF < '$dt_atu') AND
                A.CO_TP_ATI IN (
                    SELECT
                        X.CO_TP_ATI
                    FROM
                        ATIVO_TIPO X
                    WHERE
                        X.CO_MTP_ATI = 11) AND
                A.CO_ATI = '$co_ati'
            ORDER BY
                DT_REF DESC
            ";

        //var_dump($sql);
        return $this->_executaSql($sql);
    }

    /**
    * Consulta as últimas negociações no mercado de Títulos Ações
    *
    * Tabela MERCADO_ACAO
    *        ATIVO
    *        ATIVO_TIPO
    *
    * @param string $dt_atu  A data de atualização da base de dados
    * @param string $no_cod  O ISIN do ativo
    * @param string $qtd_neg A quantidade de últimos negócios que serão exibidos
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function detalheNegocMercadoAcao
    ($dt_atu = null, $no_cod = null, $qtd_neg = null)
    {
        if (is_null($dt_atu) || is_null($no_cod)) {
            return;
        }

        if ((int) $qtd_neg == 0) {
            $qtd_neg = '21';
        }

        $sql
            = "
            SELECT
                X.*,
                X.NU_NEG_TT AS NU_NEG_CONTR
            FROM
                MERCADO_ACAO X
            WHERE
                X.NO_COD = '$no_cod' AND
                X.DT_REF IN (
                    SELECT
                        DISTINCT TOP $qtd_neg
                        Z.DT_REF
                    FROM
                        MERCADO_ACAO Z
                    WHERE
                        Z.DT_REF < '$dt_atu'
                    ORDER BY
                        Z.DT_REF DESC)
            ORDER BY
                DT_REF DESC
            ";
       
        //var_dump($sql);

        return $this->_executaSql($sql);
    }

    /**
    * Consulta as últimas negociações no mercado de debêntures
    *
    * Tabela MERCADO_DEBENTURE
    *
    * @param string $dt_atu  A data de atualização da base de dados
    * @param string $no_ati  O nome do ativo
    * @param string $qtd_neg A quantidade de últimos negócios que serão exibidos
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function detalheNegocMercadoDebenture
    ($dt_atu = null, $no_ati = null, $qtd_neg = null)
    {
        if (is_null($dt_atu) || is_null($no_ati)) {
            return;
        }

        if ((int) $qtd_neg == 0) {
            $qtd_neg = '21';
        }

        $sql
            = "
            SELECT
                X.*,
                X.NU_NEG_CONTR AS NU_NEG_CONTR
            FROM
                MERCADO_DEBENTURE X
            WHERE
                X.NO_COD = '$no_ati' AND
                X.DT_REF IN (
                    SELECT
                        DISTINCT TOP $qtd_neg
                        Z.DT_REF
                    FROM
                        MERCADO_DEBENTURE Z
                    WHERE
                        Z.DT_REF < '$dt_atu'
                    ORDER BY
                        Z.DT_REF DESC)
            ORDER BY
                DT_REF DESC
            ";
       
        //var_dump($sql);
        //echo '</br></br>';

        return $this->_executaSql($sql);
    }

    /**
    * Consulta o valor de ajuste no mercado futuro
    *
    * Tabela MERCADO_FUTURO
    *
    * @param string $dt_ref  A data de referência
    * @param string $no_ati  O nome do ativo
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getValorAjusteMercadoFuturo
    (\DateTime $dt_ref = null, $no_ati = null)
    {
        if (is_null($dt_ref) || is_null($no_ati)) {
            return;
        }

        $no_cod  = substr($no_ati, 0, 3);
        $no_vcto = substr($no_ati, 4, 3);

        $dt_ref = $dt_ref->format('Y-m-d');

        $sql
            = "
            SELECT
                DISTINCT X.VR_AJUSTE
            FROM
                MERCADO_FUTURO X
            WHERE
                X.NO_COD = '$no_cod' AND 
                X.NO_VCTO = '$no_vcto' AND
                X.DT_REF = '$dt_ref'
            ";
        
        //echo '</br></br></br></br>';
        //echo $sql;

        $valorAjuste = $this->_executaSql($sql);

        return (float) $valorAjuste[0]['VR_AJUSTE'];
    }

    /**
    * Consulta as últimas negociações no mercado de debêntures
    *
    * Tabela MERCADO_FUTURO
    *
    * @param string $dt_atu  A data de atualização da base de dados
    * @param string $no_ati  O nome do ativo
    * @param string $qtd_neg A quantidade de últimos negócios que serão exibidos
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function detalheNegocMercadoFuturo
    ($dt_atu = null, $no_ati = null, $qtd_neg = null)
    {
        if (is_null($dt_atu) || is_null($no_ati)) {
            return;
        }

        if ((int) $qtd_neg == 0) {
            $qtd_neg = '21';
        }

        $no_cod  = substr($no_ati, 0, 3);
        $no_vcto = substr($no_ati, 4, 3);

        $sql
            = "
            SELECT
                X.NU_NEG_CONTR AS NU_NEG_CONTR,
                X.VR_AJUSTE
            FROM
                MERCADO_FUTURO X
            WHERE
                X.NO_COD = '$no_cod' AND 
                X.NO_VCTO = '$no_vcto' AND
                X.DT_REF IN (
                    SELECT
                        DISTINCT TOP $qtd_neg
                        Z.DT_REF
                    FROM
                        MERCADO_FUTURO Z
                    WHERE
                        Z.DT_REF < '$dt_atu'
                    ORDER BY
                        Z.DT_REF DESC)
            ORDER BY
                DT_REF DESC
            ";
       
        //var_dump($sql);
        //echo '</br></br>';

        return $this->_executaSql($sql);
    }

    /**
    * Consulta os XMLs dos fundos não processados na rotina diária do XIMPP
    *
    * Tabela PRODUTO_DIA
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getXmlNaoProcessados($dt_atu = null)
    {
        $dt_atu = formata_data($dt_atu);

        $sql = "
            SELECT
                P.NO_PRD,
                P.CO_PRD,
                DT_ATU,
                DT_REF
            FROM
                PRODUTO_DIA D
            INNER JOIN
                PRODUTO P
            ON
              D.CO_PRD = P.CO_PRD
            WHERE
                DT_ATU = '$dt_atu' AND
                DT_REF <> (
                      SELECT TOP 1
                          DT_REF
                      FROM
                          PRODUTO_DIA
                      WHERE
                          DT_ATU = '$dt_atu')
            ORDER BY DT_REF
        ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta as diferenças nos XMLs dos fundos
    *
    * Tabela PRODUTO_DIA
    *        PRODUTO_POSICAO
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getDiferencasXml($dt_atu)
    {
        $dt_atu = formata_data($dt_atu);

        $sql
            = "
            SELECT
                PP.DT_REF DATA,
                P.CO_PRD CNPJ,
                P.NO_PRD FUNDO,
                SUM(PP.VR_MERC_A) ATIVO,
                SUM(PP.VR_MERC_P) PASSIVO,
                SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) TOTAL,
                PD.VR_ATI VALOR_XML,
                SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) - PD.VR_ATI DIFERENCA
            FROM
                PRODUTO_POSICAO PP
            JOIN
                PRODUTO_DIA PD
            ON
                PD.DT_REF = PP.DT_REF AND
                PD.CO_PRD = PP.CO_PRD
            JOIN
                PRODUTO P
            ON
                P.CO_PRD = PP.CO_PRD
            WHERE
                PD.DT_ATU = ('$dt_atu') AND
                P.CO_PRD <> '00000000000007' AND
                P.CO_CUSTOD = '00360305000104' AND
                NOT (CO_TP_OP = 'T' AND DT_VCTO_ALUG IS NOT NULL) AND
                P.CO_CUSTOD = '00360305000104'
            GROUP BY
                PP.DT_REF,
                P.CO_PRD,
                P.NO_PRD,
                PD.VR_ATI
            HAVING
                ABS(SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) - PD.VR_ATI) > 1000
            AND
                ABS(SUM(PP.VR_MERC_A) - PD.VR_ATI) > 1000
            ORDER BY
                DIFERENCA";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta O patrimônio líquido do fundo
    *
    * Tabela PRODUTO_DIA
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return float $VR_PL O valor do PL do fundo
    */
    public function getPlFundo($co_prd = 0, $dt_atu = 0)
    {
        $query = $this->db->query(
            "
            SELECT
                DISTINCT
                VR_PL
            FROM
                PRODUTO_DIA
            WHERE
                CO_PRD = '$co_prd' AND
                DT_ATU = '$dt_atu'
            "
        );

        foreach ($query->result() as $row) {
            return $row->VR_PL;
        }

        return;
    }

    /**
    * Consulta a distribuição dos cotistas no fundo
    *
    * Tabela DISTR_FIC_P_FF
    *        PRODUTO P
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getDistribuicaoCotistas($co_prd = 0, $dt_atu = 0)
    {
        $sql
            = "
            SELECT
                P.NO_PRD NO_PRD,
                A.VR_COTA_FF VR_COTA_FF
            FROM
                DISTR_FIC_P_FF A
            INNER JOIN
                PRODUTO P
            ON
                A.CO_FIC = P.CO_PRD
            WHERE
                A.CO_FF = '$co_prd' AND
                A.DT_ATU = '$dt_atu'
            ORDER BY
                A.VR_COTA_FF DESC
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta dados de rentabilidade e valores aplicados e resgatados diarios
    * Se não for definido a quantidade, pega os últimos 42 dias úteis anteriores
    * a data de referência
    *
    * Tabela Produto_Dia_2
    *
    * @param string $co_prd   O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    * @param int    $qtd_dias A quantidade de dias para retorno
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getDadosCaptacaoRentabilidadeDiaria
        ($co_prd = 0, $dt_atu = 0, $qtd_dias = 0)
    {
        if ($qtd_dias == 0) {
            $qtd_dias = 42;
        }

        if (($co_prd == 0) || ($dt_atu == 0)) {
            return;
        }

        $sql
            = "
            SELECT
                TOP $qtd_dias
                DT_REF,
                CO_PRD,
                VR_PL,
                VR_APL,
                VR_RESG,
                VR_COTA,
                VR_RET_DIA,
                VR_RET_MES,
                VR_RET_ANO,
                NU_COTISTAS,
                VR_RESG_IR,
                DH_FONTE_1,
                DH_FONTE_2
            FROM
                Produto_Dia_2
            WHERE
                CO_PRD = '$co_prd' AND
                DT_REF < '$dt_atu'
            ORDER BY
                DT_REF DESC
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a carteira de crédito total da VITER para a página inicial
    * do SIRAT web
    *
    * Tabela CR245002_RC_11
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getCarteiraCreditoGeral($dt_atu = 0)
    {
        if ($dt_atu == 0) {
            return;
        }

        $sql
            = "
            SELECT
                NO_SEG,
                SUM(VR_MERC),
                SUM(VR_MERC_C_DUPL),
                SUM(VR_MERC)/SUM(VR_MERC_C_DUPL),
                SUM(VR_MERC_S_DUPL),
                SUM(VR_MERC)/SUM(VR_MERC_S_DUPL)
            FROM
                CR245002_RC_11
            WHERE
                DT_ATU = '$dt_atu' AND (
                'VITER' = 'VITER' OR
                NO_GRU_GESTAO = 'VITER' OR
                (NO_SEG = 'VITER' AND
                ('0' = 1 AND
                CO_POL_CRED = 1 OR
                '0' = 0 AND
                ISNULL(CO_POL_CRED, 0) = 0)) OR
                ('VITER' = 'CR 245' AND
                CO_POL_CRED = 1))
            GROUP BY
                NO_SEG
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta todos os limites instituição não financeira
    * VITER para a página inicial do SIRAT web
    *
    * Tabela CR245002_RC_17
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getLimitesNaoIfGeral($dt_atu = 0)
    {
        if ($dt_atu == 0) {
            return;
        }

        $sql
            = "
            SELECT
                NO_RATING,
                NO_EMISS,
                DT_EMISS,
                VR_EMISS,
                PC_LIM,
                VR_LIM,
                PC_ALOC,
                VR_ALOC,
                PC_ALOC_S_LIM,
                CO_ALERTA
            FROM
                CR245002_RC_17
            WHERE
                DT_ATU = '$dt_atu'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta todos os FIDCs
    * VITER para a página inicial do SIRAT web
    *
    * Tabela CR245002_RC_20
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getFidcsGeral($dt_atu = 0)
    {
        if ($dt_atu == 0) {
            return;
        }

        $sql
            = "
            SELECT
                NO_RATING,
                NO_EMISS,
                DT_EMISS,
                VR_EMISS,
                PC_LIM,
                VR_LIM,
                PC_ALOC,
                VR_ALOC,
                PC_ALOC_S_LIM,
                CO_ALERTA
            FROM
                CR245002_RC_20
            WHERE
                DT_ATU = '$dt_atu'
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a composição da carteira de todos fundos VITER
    * para a página inicial do SIRAT web
    *
    * Tabela CR245002_RL_11
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getComposicaoCarteiraGeral($dt_atu = 0)
    {
        if ($dt_atu == 0) {
            return;
        }

        $sql
            = "
            SELECT
                NO_MTP_ATI,
                SUM(VR_MERC) VR_MERC
            FROM
                CR245002_RL_11
            WHERE
                DT_ATU = '$dt_atu' AND (
                'VITER' = 'VITER' OR
                NO_GRU_GESTAO = 'VITER' OR
                (NO_SEG = 'VITER' AND
                ('0' = 1 AND
                CO_POL_LIQ = 1 OR
                '0' = 0 AND
                ISNULL(CO_POL_LIQ, 0) = 0)) OR
                ('VITER' = 'CR 245' AND
                CO_POL_LIQ = 1) OR
                CO_PRD = 'VITER')
            GROUP BY
                NO_MTP_ATI
            ORDER BY
                NO_MTP_ATI
            ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta a composição da carteira de todos fundos VITER
    * para a página inicial do SIRAT web
    *
    * Tabela PRODUTO_DIA
    *
    * @param string $dt_atu A data de atualização da base de dados
    * @param string $co_prd O CNPJ do fundo
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getProdutoDia($co_prd = 0, $dt_atu = 0)
    {
        $sql
            = "
            SELECT
                CO_PRD,
                DT_ATU,
                DT_REF,
                VR_ATI,
                VR_PL,
                VR_COTA,
                VR_APL_D0,
                VR_RES_D0,
                DH_CAPT_LIQ_D0,
                VR_CAPT_LIQ_D0,
                VR_PL_D0,
                VR_ALAVANC,
                VR_ALAVANC_2,
                IC_CHECADO,
                VR_RESG_PROJ,
                VR_RESG_MAX_3A,
                PC_RESG_MAX_3A,
                DT_RESG_MAX_3A,
                VR_ALUG,
                VR_EM_GAR,
                VR_ARB_V,
                VR_ARB_C,
                VR_TOP_COTISTA,
                NO_XML,
                DT_COTIZ,
                VR_SALDO_PERM_ALTA,
                VR_A_EMIT,
                VR_A_RESG,
                VR_DURATION,
                VR_RET_DIA,
                VR_ISG,
                NO_MAPS_METODO_VAR,
                VR_MAPS_NIV_CONF,
                NU_MAPS_HORIZ,
                VR_MAPS_LAMBDA,
                NO_MAPS_CURRENCY,
                NO_MAPS_VAR_TITLE,
                VR_MAPS_COTAS,
                VR_MAPS_PL_INFO,
                VR_MAPS_PL_CALC,
                VR_MAPS_MTM,
                VR_MAPS_VAR_POL,
                VR_MAPS_VAR_1D_95,
                VR_MAPS_VAR_1D_95_STRESS,
                VR_MAPS_VAR_21D_95,
                VR_MAPS_VAR_1D_99,
                VR_MAPS_VAR_15D_99,
                VR_MAPS_VAR_21D_99,
                NO_MAPS_BENCH,
                NU_MAPS_MULT,
                NO_MAPS_IS_NORMALIZED,
                VR_MAPS_DURATION,
                VR_MAPS_DURATION_MOD,
                CO_MAX_FATOR,
                VR_MAX_FATOR,
                CO_MAX_ATIVO,
                VR_MAX_ATIVO,
                VR_MAX_ATIVO2
            FROM
                Produto_Dia
            WHERE
                CO_PRD = '$co_prd' AND
                DT_ATU = '$dt_atu'
            ";

        $saida = $this->_executaSql($sql);

        return $saida[0];
    }

    /**
    * Consulta no banco de dados os dados de liquidez
    * agrupados por tipo de ativo
    *
    * Tabela CR245002_RL_F_12
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getDadosLiquidezAgrupadasPorTipo
    ($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
            SELECT
                NO_MTP_ATI,
                SUM(VR_VOLUME_1D) VR_VOLUME_1D,
                SUM(VR_MERC) VR_MERC
            FROM
                CR245002_RL_F_12
            WHERE
                IC_CENARIO = 'N' AND
                DT_ATU = '$dt_atu' AND
                CO_PRD = '$co_prd'
            GROUP BY
                NO_MTP_ATI
              ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta os ativos de crédito de um fundo
    *
    * View V_CR245001_MONIT_RC_FUNDOS_EMISS
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    * @param string $tipo_ativo O Tipo do ativo: LF
    *                                            Letra Hipotecária
    *                                            CCB
    *                                            Operação Compromissada
    *                                            LFSUB
    *                                            LH
    *                                            CDBSUB
    *                                            Debêntures
    *                                            CRI
    *                                            CDB
    *                                            CCI
    *                                            IF
    *                                            DPGE
    *                                            INF
    *                                            FIDC
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getAtivosCredito
    ($co_prd = null, $dt_atu = null, $tipo_ativo = null)
    {
        $sqlWhere
            = " O.DT_ATU      = '$dt_atu' AND
                O.CO_PRD      = '$co_prd' AND
                O.NO_TP_ATI   = '$tipo_ativo'
            ";

        if ($tipo_ativo == 'DPGE' || $tipo_ativo == 'FIDC' || 
            $tipo_ativo == 'IF' OR $tipo_ativo == 'INF') {
            $sqlWhere
                = " O.DT_ATU      = '$dt_atu' AND
                    O.CO_PRD      = '$co_prd' AND
                    V.NO_TP_EMISS = '$tipo_ativo'
                ";
        }

        $sql
            = "
            SELECT
                V.DT_ATU,
                V.DT_REF,
                V.CO_PRD,
                V.CO_EMISS,
                V.NO_EMISS,
                V.NO_TP_EMISS,
                V.NO_RATING,
                V.VR_MERC,
                V.PC_LIM,
                V.VR_ALOC,
                V.PC_ALOC,
                V.VR_LIM,
                V.PC_ALOC_S_LIM,
                V.CO_ALERTA,
                V.VR_LIM_D0,
                V.PC_ALOC_D0,
                V.PC_ALOC_S_LIM_D0,
                V.CO_ALERTA_D0,
                O.CO_ATI,
                O.NO_ATI,
                O.NO_EMISS,
                O.IC_RECOMPRA,
                O.DT_EMISS,
                O.DT_VCTO,
                O.PC_IDX_A,
                O.PC_IDX_P,
                O.PC_COUPOM,
                O.VR_MERC,
                O.PC_MERC,
                O.VR_DIAS_CDI
            FROM
                [CR245002_RC_F_14.A] O
            INNER JOIN
                ATIVO A
            ON
                O.CO_ATI = A.CO_ATI
            RIGHT JOIN
                V_CR245002_MONIT_RC_FUNDOS_EMISS V
            ON
                O.NO_EMISS = V.NO_EMISS AND
                O.DT_ATU   = V.DT_ATU AND
                O.CO_PRD   = V.CO_PRD
            WHERE
                $sqlWhere
            ORDER BY
                O.NO_ATI
            ";

        //echo ($sql);
        return $this->_executaSql($sql);
    }

    /**
    * Consulta no banco de dados o relatório de liquidez 
    *
    * Tabela CR245002_Monit_RL_Fundos
    *
    * @param string $co_prd O CNPJ do fundo
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getRelatorioRiscoLiquidez
    ($co_prd = null, $dt_atu = null)
    {
        $sql
            = "
            SELECT
                DT_ATU,
                CO_PRD,
                DT_REF,
                VR_LIM,
                VR_ALOC,
                PC_ALOC_S_LIM,
                VR_TOP_COTISTA,
                VR_VOLUME_LIQ,
                VR_VOLUME_1D,
                VR_RESG_PROJ,
                VR_RESG_PROG,
                CO_ALERTA,
                NO_MSG,
                CO_ALERTA_RESG_PROG,
                CO_ALERTA_RESG_PROJ,
                CO_ALERTA_TOP_COTISTA
            FROM
                CR245002_Monit_RL_Fundos
            WHERE
                DT_ATU = '$dt_atu' AND
                CO_PRD = '$co_prd'
              ";

        return $this->_executaSql($sql);
    }

    /**
    * Consulta no banco de dados a data de atualização de acordo com a dt_ref
    *
    * Tabela PRODUTO_DIA
    *
    * @param string $dt_ref A data de referência
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getDataReferencia($dt_atu = null)
    {
        $sql
            = "
            SELECT
                TOP 1 
                DT_REF 
            FROM 
                PRODUTO_DIA 
            WHERE 
                DT_ATU = '$dt_atu'
              ";
        
        foreach ($this->_executaSql($sql) as $row) {
            return $row['DT_REF'];
        }
        return;
    }
}
