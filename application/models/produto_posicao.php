<?php

class Produto_posicao_Model extends CI_Model
{

    private $id;

    /**
     * @var string $nome
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @var string $cpf
     *
     * @ORM\Column(name="cpf", type="string", length=11)
     */
    private $cpf;

    /**
     * @var smallint $caixa
     *
     * @ORM\Column(name="caixa", type="smallint")
     */
    private $caixa;

    /**
     * @var date $data_ficha_cadastro
     *
     * @ORM\Column(name="data_ficha_cadastro", type="date")
     */
    private $data_ficha_cadastro;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }	
	
    /**
     * Set nome
     *
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set cpf
     *
     * @param bigint $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * Get cpf
     *
     * @return bigint 
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set caixa
     *
     * @param smallint $caixa
     */
    public function setCaixa($caixa)
    {
        $this->caixa = $caixa;
    }

    /**
     * Get caixa
     *
     * @return smallint 
     */
    public function getCaixa()
    {
        return $this->caixa;
    }

    /**
     * Set data_ficha_cadastro
     *
     * @param date $dataFichaCadastro
     */
    public function setDataFichaCadastro($dataFichaCadastro)
    {
        $this->data_ficha_cadastro = $dataFichaCadastro;
    }

    /**
     * Get data_ficha_cadastro
     *
     * @return date 
     */
    public function getDataFichaCadastro()
    {
        return $this->data_ficha_cadastro;
    }

    /**
     * Set created_at
     *
     * @param datetime $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * Get created_at
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
	
    public function save()
    {
		$data = array(
            'caixa'               => $this->getCaixa(),
			'nome'                => $this::toUpper($this->getNome()),
			'cpf'				  => $this->getCpf(),
			'data_ficha_cadastro' => $this->getDataFichaCadastro(),
			'created_at'          => date('Y-m-d H:i:s')
		);
		
		if ($this->getId() > 0) {
			
			$this->db->where('id', $this->getId());
			$this->db->update('Dossie', $data);
		
		} else {
		
			$this->db->insert('Dossie', $data);
		
		}
	}
	
    public function getLasts($limit)
    {
		$query = $this->db->limit($limit);
		$query = $this->db->order_by("id", "desc"); 
		$query = $this->db->get('Dossie');		
		return $query->result();
	}
	
    public function getDossies()
    {
		$this->db->select('*');
		$this->db->from('Dossie');
		
		if ($this->getCaixa() > 0) {
			$this->db->where('caixa', $this->getCaixa());
		}
		
		if (strlen(trim($this->getNome()))  > 0) {
			$this->db->like('nome', $this::toUpper($this->getNome()));
		}
		
		if ($this->getCpf() > 0) {
			$this->db->where('cpf', $this->getCpf());
		}
		
		$this->db->order_by("id", "desc"); 
		
		$query = $this->db->get();
		return $query->result();
	}

    public function getDossie($id)
    {
		$this->db->select('*');
		$this->db->from('Dossie');
		
		if ($id > 0) {
			$this->db->where('id', $id);
		}
		
		$query = $this->db->get();
		
		$dossies = $query->result();
		
		foreach ($dossies as $row)
		{
			$dossie = new Dossie_Model();
			$dossie->setId($row->id);
			$dossie->setNome($row->nome);
			$dossie->setCpf($row->cpf);
			$dossie->setDataFichaCadastro($row->data_ficha_cadastro);
			$dossie->setCaixa($row->caixa);
			$dossie->setCreatedAt($row->created_at);
			return $dossie;
		}		
		
		return;
	}
	
    public function deleteDossie($id)
    {
		if ($id > 0) {
			$this->db->where('id', $id);
			$this->db->delete('Dossie'); 
		}
		
		return;
	}

    public static function toUpper($string) 
    {
        return (
            strtoupper(
                strtr(
                    $string, 
                    'àáâãäåæçèéêëìíîïðñòóôõöøùúûüý',
                    'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ' 
                    )
                    )); 
    }
}
