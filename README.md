GERAT
=====

GERAT é um projeto pessoal para o auxílio nas rotinas básicas e automatização de
algumas tarefas manuais e geração de relatórios referentes aos fundos de investimentos.


GERAT utliliza o framework Code Igniter e PHP 5.4.

Virtual Host Apache
===================

.. code-block:: apache

    <VirtualHost *:80>
        DocumentRoot "/home/rodrigo/Dropbox/projetos/caixa/gerat"
        ServerName gerat
        ServerAlias gerat
        DirectoryIndex index.php index.html index.htm index.shtml test.php
        <Directory “/home/rodrigo/Dropbox/projetos/caixa/gerat”>
            Options Indexes FollowSymLinks Includes ExecCGI
            AllowOverride All
            Order deny,allow
            Allow from all
        </Directory>
    </VirtualHost>

Clonando o repositório
======================

git clone git://github.com/royopa/gerat-ci.git

SQlite Admin
============

projeto/assets/php/phpliteadmin.php

DBAL Sql Server 2000
====================

https://github.com/royopa/dbal

PHP CPD
=======
phpcpd C:\xampp\htdocs\gerat\application\models\classes\